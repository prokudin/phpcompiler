/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phprtl;

import java.lang.String;
import java.util.HashMap;

/**
 *
 * @author Admin
 */
public class Variables {
    
    public Variables(){
        
    }
    
    public stdClass dollar(stdClass varName){
        String name = varName.convertToString();
        
        if (!_vars.containsKey(name)){
            _vars.put(name,stdClass.fromNull());
        }
        
        return _vars.get(name);
    }
    
    public stdClass dollar(String varName){  
        if (!_vars.containsKey(varName)){
            _vars.put(varName,stdClass.fromNull());
        }
        
        return _vars.get(varName);
    }
        
    public stdClass put(String name,stdClass value){
        _vars.put(name,value);        
        return _vars.get(name);
    }
    
    public stdClass put(stdClass name,stdClass value){
        _vars.put(name.convertToString(),value);        
        return _vars.get(name.convertToString());
    }    
    
    private HashMap<String,stdClass> _vars = new HashMap<String,stdClass>();
}
