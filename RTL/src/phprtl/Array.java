/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phprtl;

import java.util.LinkedHashMap;
import java.lang.ArrayIndexOutOfBoundsException;
import java.util.Iterator;
import java.util.Map;
/**
 *
 * @author Admin
 */
public class Array {        
    
    private int currentIntIndex;
    
    public Array(){
        currentIntIndex= -1;
    }
    
    public int size(){
        return _arr.size();
    }
    
    public boolean containsKey(String key){
        return _arr.containsKey(key);
    }
    
    public stdClass put(stdClass key,stdClass value){  
        switch (key._valueType){
            case stdClass.TYPE_OBJECT:
                throw new IllegalArgumentException("Array key musn't be object");
            case stdClass.TYPE_STR:
                if (key._stringValue != "0" && key.convertToInt()==0){
                    _arr.put((key._stringValue), value);
                    return _arr.get(key._stringValue);
                }else{
                    if (key.convertToInt()>currentIntIndex)
                        currentIntIndex = key.convertToInt();
                    _arr.put(key.convertToInt(), value);
                    return _arr.get(key.convertToInt());
                }
            default:       
                
                if (key.convertToInt()>currentIntIndex)
                    currentIntIndex = key.convertToInt();
                _arr.put(key.convertToInt(), value);
                return _arr.get(key.convertToInt());
        }
    } 
    
    public stdClass put(stdClass value){
        _arr.put(++currentIntIndex, value);
        return _arr.get(currentIntIndex);
    }
    
    public ArrayIterator iterator(){
        return new ArrayIterator(_arr);
    }
    
    public boolean equals(java.lang.Object other){
        
        if (!(other instanceof Array)){
            throw new IllegalArgumentException("Right object isn't array");
        }
        
        Array otherarr = (Array)other;
        
        boolean result = _arr.size() == otherarr.size();
        
        Iterator th = _arr.entrySet().iterator();
        Iterator oth = otherarr._arr.entrySet().iterator();
        while (th.hasNext() && result)
        {
            Map.Entry thk = (Map.Entry)th.next();
            Map.Entry othk = (Map.Entry)oth.next();
            result = result && thk.getKey().equals(othk.getKey()) && ((stdClass)thk.getValue()).opEqualByType(((stdClass)othk.getValue())).convertToBoolean();
        }
        
        return result;
    }
    
    public stdClass get(stdClass key){
        try{
            switch (key._valueType){
                case stdClass.TYPE_OBJECT:
                    throw new IllegalArgumentException("Array key musn't be object");
                case stdClass.TYPE_STR:
                    if (key._stringValue != "0" && key.convertToInt()==0){                        
                        return _arr.get(key._stringValue);
                    }else{                        
                        return _arr.get(key.convertToInt());
                    }
                default:                    
                    return _arr.get(key.convertToInt());
            }
            
        } catch (Exception ex){
            if (ex instanceof IllegalArgumentException)
                throw new IllegalArgumentException("Array key musn't be object");
            
            throw new ArrayIndexOutOfBoundsException("Array index is out of range. Index = " + key.convertToString());
        }
    }
    
    public java.lang.Object clone() throws CloneNotSupportedException
    {
        Array result = new Array();
        
        result.currentIntIndex = this.currentIntIndex;
        result._arr = (LinkedHashMap<java.lang.Object,stdClass>)this._arr.clone();
        
        return result;
    }
    
    private LinkedHashMap<java.lang.Object,stdClass> _arr = new LinkedHashMap<java.lang.Object,stdClass>();
}
