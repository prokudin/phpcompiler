/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phprtl;

import java.util.HashMap;

/**
 *
 * @author Admin
 */
public class Object extends stdClass{
    public Object(){
        super();
    }
    
        // для вызова изнутри класса
    public stdClass setField(String name,String callerClass,String currentClass, stdClass value){
        addMap(currentClass);
        
        if (!_fields.containsKey(currentClass))
            _fields.put(currentClass, new HashMap<>());
        
        //System.out.println(name);
        // System.out.println(currentClass);
        String cl = containsField(name,currentClass,this);        
        //System.out.println(cl);
        
        if (cl == null){
            throw new RuntimeException("No such field '" +name+ "' from class " + currentClass);
        }else if (canGetField(name,callerClass,cl,this)){
            if (_fields.containsKey(cl) && _fields.get(cl).containsKey(name)){
                _fields.get(cl).put(name, value);
                return _fields.get(cl).get(name);
            }else if (_staticFields.containsKey(cl) && _staticFields.get(cl).containsKey(name)){
                _staticFields.get(cl).put(name, value);
                return _staticFields.get(cl).get(name);
            }
        }
        
        throw new RuntimeException("Access level error. You cann't set field '" +name+ "' from class " + callerClass);
    }
    // для вызова извне класса
    public stdClass setField(String name,String callerClass,stdClass value){
        return setField(name,callerClass,className,value);
    }
}
