/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phprtl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static phprtl.Value.TYPE_INT;

/**
 *
 * @author Admin
 */
public class stdClass extends Value implements Serializable {    
    
    public stdClass(){
        this("phprtl/stdClass");
    }
    
    protected stdClass(String classname){
        super();
        super._valueType = Value.TYPE_OBJECT;
        this.className = classname;     
    }
    
    /* ******************Генераторы стандартных типов**************************/
    public static stdClass fromInt(int val){
        stdClass v = new stdClass();
        v._valueType = TYPE_INT;
        v._intValue = val;
        return v;        
    }
    
    public static stdClass fromFloat(float val){
        stdClass v = new stdClass();
         v._valueType = TYPE_FLOAT;
        v._floatValue = val;
        return v;        
    }
    
    public static stdClass fromBool(boolean val){
        stdClass v = new stdClass();
        v._valueType = TYPE_BOOLEAN;
        v._intValue = val? 1 : 0;
        return v;        
    }
    
    public static stdClass fromString(String val){
        stdClass v = new stdClass();
        v._valueType = TYPE_STR;
        v._stringValue = val;
        return v;        
    }
    
    public static stdClass fromArray(Array val){
        stdClass v = new stdClass();
        v._valueType = TYPE_ARRAY;
        v._array = val;
        return v;        
    }
    
    public static stdClass fromNull(){
        stdClass v = new stdClass();
        v._valueType = TYPE_NULL;       
        return v;        
    }    
    
    public String className;
    
    protected LinkedHashMap<String,HashMap<String,stdClass>> _fields= new LinkedHashMap<>();;
    protected static LinkedHashMap<String,HashMap<String,stdClass>> _staticFields= new LinkedHashMap<>();;
    protected static LinkedHashMap<String,HashMap<String,Integer>> _fieldsVisibility= new LinkedHashMap<>();;
    protected static LinkedHashMap<String,HashMap<String,Integer>> _methodsVisibility= new LinkedHashMap<>();;
    
    protected static void addMap(String classname){
        if (!_staticFields.containsKey(classname)){            
            _staticFields.put(classname, new HashMap<>());
            _fieldsVisibility.put(classname, new HashMap<>());
            _methodsVisibility.put(classname, new HashMap<>());
        }
    }
    /*****************  Методы для работу с полями      ********************//////////
    
    protected static boolean canGetField(String field,String callerClass,String currentClass,stdClass obj){        
        
        currentClass = containsField(field,currentClass,obj);
        
        HashMap<String,Integer> fieldvis = _fieldsVisibility.get(currentClass);
        int vis = fieldvis.get(field);
        
        if (vis==0){
            return true;
        }
        
        Class current,caller;
        try{
            current = Class.forName(currentClass.replace('/', '.'));
            caller = Class.forName(callerClass.replace('/', '.'));
        } catch (Exception e)
        {
            throw new RuntimeException("Incorrect caller class");
        }
        
        if (vis==1 && current.isAssignableFrom(caller)){
            return true;
        }
        
        if (vis==2 && current.equals(caller)){
            return true;
        }
        
        return false;
    }
    protected static boolean canCallMethod(String method,String callerClass,String currentClass){
        
        currentClass = containsMethod(method,currentClass);
        
        if (currentClass == null)
            return false;
        
        HashMap<String,Integer> fieldvis = _methodsVisibility.get(currentClass);
        
        int vis = fieldvis.get(method);
        
        if (vis==0){
            return true;
        }
        
        Class current,caller;
        try{
            current = Class.forName(currentClass.replace('/', '.'));
            caller = Class.forName(callerClass.replace('/', '.'));
        } catch (Exception e)
        {
            throw new RuntimeException("Incorrect caller class");
        }
        
        if (vis==1 && current.isAssignableFrom(caller)){
            return true;
        }
        
        if (vis==2 && current.equals(caller)){
            return true;
        }
        
        return false;
    }
    protected static String containsField(String field,String from,stdClass obj){
        /// поиск начиная с класса from включая
        from = from.replace('.', '/');
        
        // поиск среди нестатических свойств
        if (obj!=null){
            // найти текущий класс
            
            ListIterator<String> iter = new ArrayList<>(obj._fields.keySet()).listIterator(obj._fields.size());
        
            boolean classFound = false;
            String tmp;
            while(iter.hasPrevious() && !classFound){
                tmp = iter.previous();
                classFound = tmp.equals(from);            
            }
            
            try{
                if (classFound){
                    Class current = Class.forName(from.replace('/', '.'));

                    String s = from;
                    if (obj._fields.get(s).containsKey(field))
                        return s;
                    
                    while(iter.hasPrevious()){
                        s = iter.previous();                    
                        Class parent = Class.forName(s.replace('/', '.'));
                        if (parent.isAssignableFrom(current) && obj._fields.get(s).containsKey(field))
                        {
                            return s;
                        }
                    }
                }
            }catch (Exception ex){
                return null;
            }
        }
        
        ListIterator<String> iter = new ArrayList<>(_staticFields.keySet()).listIterator(_staticFields.size());
        
        boolean classFound = false;
        String tmp;
        while(iter.hasPrevious() && !classFound){
            tmp = iter.previous();
            classFound = tmp.equals(from);            
        }
                
        try {        
            if (classFound){
                Class current = Class.forName(from.replace('/', '.'));

                String s = from;
                if (_staticFields.get(s).containsKey(field))
                    return s;
                
                iter = new ArrayList<>(_staticFields.keySet()).listIterator(_staticFields.size());
                while(iter.hasPrevious()){
                    s = iter.previous();                    
                    Class parent = Class.forName(s.replace('/', '.'));
                    
                    if (parent.isAssignableFrom(current) && _staticFields.get(s).containsKey(field))
                    {
                        return s;
                    }
                }
            }
        }catch (Exception ex){
            return null;
        }
        
        return null;
    }    
    protected static String containsMethod(String method,String from){
        from = from.replace('.', '/');
        //System.out.println("Method name = " + method + ", Class name = "+ from);
        ListIterator<String> iter = new ArrayList<>(_methodsVisibility.keySet()).listIterator(_methodsVisibility.size());
        
        boolean classFound = false;
        while(iter.hasPrevious() && !classFound){
            String s = iter.previous();
            classFound = s.equals(from);            
        }
        
        try { 
        
            if (classFound){
                String s = from;
                    
                Class current = Class.forName(from.replace('/', '.'));
                
                if (_methodsVisibility.containsKey(s) && _methodsVisibility.get(s).containsKey(method))
                    return s;
                
                iter = new ArrayList<>(_methodsVisibility.keySet()).listIterator(_methodsVisibility.size());

                while(iter.hasPrevious()){
                    s = iter.previous();
                    
                    Class parent = Class.forName(s.replace('/', '.'));                    
                    
                    if (parent.isAssignableFrom(current) && _methodsVisibility.containsKey(s) &&  _methodsVisibility.get(s).containsKey(method))
                        return s;
                }
            }
        }catch (Exception ex){
            return null;
        }
        return null;
    }
    
    
    // 0 - public , 1 - protected , 2 - private
    // для вызова внутри класса 
    public void addField(String name, int visibility,String currentClass){
        addField(name,visibility,currentClass,fromNull());
    }  
    
    public void addField(String name, int visibility,stdClass value){
        addField(name,visibility,className,value);
    }
    
    public void addField(String name, int visibility,String currentClass, stdClass value){    
        
        addMap(currentClass);
        
        if (!_fields.containsKey(currentClass))
            _fields.put(currentClass, new HashMap<>());
        
        String s;        
        if ((s = containsField(name,currentClass,this))!=null){
            if (_fieldsVisibility.get(s).get(name)<visibility)
                throw new RuntimeException("Field was already defined in parent class with higher access level");            
        }
        
        _fields.get(currentClass).put(name, value);
       _fieldsVisibility.get(currentClass).put(name, visibility);
    }    
    // для вызова изнутри класса
    public stdClass setField(String name,String currentClass,String callerClass, stdClass value){
        //System.out.println(currentClass);
        addMap(currentClass);
        
        if (!_fields.containsKey(currentClass))
            _fields.put(currentClass, new HashMap<>());
        
        //System.out.println(name);
       // System.out.println(currentClass);
        String cl = containsField(name,currentClass,this);        
        //System.out.println(cl);
        
        if (cl == "phprtl/stdClass"){
            if (_fields.containsKey(cl) && !_fields.get(cl).containsKey(name)){
                _fields.get(cl).put(name, value);
                return _fields.get(cl).get(name);
            } 
        }
        
        if (cl == null){
            addField(name,0,currentClass,value);
            return _fields.get(currentClass).get(name);
        }else if (canGetField(name,callerClass,cl,this)){
            if (_fields.containsKey(cl) && _fields.get(cl).containsKey(name)){
                //System.out.println(cl);
                //System.out.println(value.convertToString());
                
                _fields.get(cl).put(name, value);
                return _fields.get(cl).get(name);
            }else if (_staticFields.containsKey(cl) && _staticFields.get(cl).containsKey(name)){
                _staticFields.get(cl).put(name, value);
                return _staticFields.get(cl).get(name);
            }
        }
        
        throw new RuntimeException("Access level error. You cann't set field '" +name+ "' from class " + callerClass);
    }
    // для вызова извне класса
    public stdClass setField(String name,String callerClass,stdClass value){
        return setField(name,className,callerClass,value);
    }
    // для вызова изнутри класса    
    public stdClass getField(String name,String currentClass,String callerClass){
                
        addMap(currentClass);
        if (!_fields.containsKey(currentClass))
            _fields.put(currentClass, new HashMap<>());
        
        
        
        String cl = containsField(name,currentClass,this);         
       
        if (cl == null)
            throw new NoSuchElementException("Class '" + currentClass + "' haven't field '" + name + "'");
        
        if (canGetField(name,callerClass,cl,this)){
            if (_fields.containsKey(cl) && _fields.get(cl).containsKey(name)){ 
                return _fields.get(cl).get(name);                 
            }
            else if (_staticFields.containsKey(cl) && _staticFields.get(cl).containsKey(name)) {
                return _staticFields.get(cl).get(name); 
            }
        }         
        
        throw new RuntimeException("Access level error. You cann't set field '" +name+ "' from class " + callerClass);
    }
    // для вызове извне класса    
    public stdClass getField(String name,String callerClass){
        return  getField(name,className,callerClass);
    }
    
    public static void addStaticField(String name, int visibility,String currentClass,stdClass value){          
        currentClass = toFullBytecodeName(currentClass);
        
        addMap(currentClass);
        
        String s;        
        if ((s = containsField(name,currentClass,null))!=null){
            if (_fieldsVisibility.get(s).get(name)<visibility)
                throw new RuntimeException("Field was already defined in parent class with higher access level");
        }
        
        _staticFields.get(currentClass).put(name,value);        
        _fieldsVisibility.get(currentClass).put(name, visibility);
    }
    
    public static stdClass setStaticField(String name,String currentClass, String callerClass,  stdClass value)
    {
        if (currentClass.equals("self"))
            currentClass = callerClass;
        else if (currentClass.equals("parent"))
            return setParentStaticField(name,callerClass,callerClass,value);
                      
        currentClass = toFullBytecodeName(currentClass);
        
        addMap(currentClass);
        
        String cl = containsField(name,currentClass,null);
        
        if (cl == null){
            throw new RuntimeException("No such property '" +name+ "' in class " + callerClass);
        }
        
        if (canGetField(name,callerClass,cl,null)){
            _staticFields.get(cl).put(name, value);
            return _staticFields.get(cl).get(name);
        }
        
        throw new RuntimeException("Access level error. You cann't set static field '" +name+ "' from class " + callerClass);
    }
    
    public static stdClass getStaticField(String name, String currentClass,String callerClass){          
        if (currentClass.equals("self"))
            currentClass = callerClass;
        else if (currentClass.equals("parent"))
            return getParentStaticField(name,callerClass,callerClass);
        
        currentClass = toFullBytecodeName(currentClass);
        
        addMap(currentClass);
        
        String cl = containsField(name,currentClass,null);
        
        if (cl == null){
            throw new RuntimeException("No such property '" +name+ "' in class " + callerClass);
        }
        
        if (canGetField(name,callerClass,cl,null)){
            return _staticFields.get(cl).get(name); 
        }
        
        throw new RuntimeException("Access level error. You cann't set field '" +name+ "' from class " + callerClass);
    }
    
    public static stdClass setParentStaticField(String name, String currentClass,String callerClass, stdClass value){    
        currentClass = toFullBytecodeName(currentClass);
        callerClass = toFullBytecodeName(callerClass);
        
        addMap(currentClass);
        
        /// получить имя родителя
        String parent="";
        try{
            parent = Class.forName(toFullClassName(currentClass)).getSuperclass().getName();
        } catch(Exception e){}
        
        String cl = containsField(name,parent,null);
        
        if (cl == null){
            throw new RuntimeException("No such parent property '" +name+ "' in class " + currentClass);
        }
        
        if (canGetField(name,callerClass,cl,null)){
            _staticFields.get(cl).put(name, value);
            return _staticFields.get(cl).get(name);
        }
        
        throw new RuntimeException("Parant field '" + name + "' from class = '" + currentClass + "' not found");
    }
    
    public static stdClass getParentStaticField(String name, String currentClass,String callerClass){  
        currentClass = toFullBytecodeName(currentClass);
        callerClass = toFullBytecodeName(callerClass);
        addMap(currentClass);
        
        /// получить имя родителя
        String parent="";
        try{
            parent = Class.forName(toFullClassName(currentClass)).getSuperclass().getName();
        } catch(Exception e){}
        
        String cl = containsField(name,parent,null);
        
        if (cl == null){
            throw new RuntimeException("No such property '" +name+ "' in class " + callerClass);
        }
        
        if (canGetField(name,callerClass,cl,null)){
            return _staticFields.get(cl).get(name); 
        }
        
        throw new RuntimeException("Access level error. You cann't set field '" +name+ "' from class " + callerClass);
    }
    
    public void setMethodVisibility(String name, int visibility){
        addMap(className);
        
        String s ;        
        if ((s = containsMethod(name,className))!=null){
            if (_methodsVisibility.get(s).get(name)<visibility)
                throw new RuntimeException("Field was already defined in parent class with higher access level");
        }
        
        _methodsVisibility.get(className).put(name, visibility);
    }
    
    public static void setStaticMethodVisibility(String name, int visibility,String classname){     
        
        classname = toFullBytecodeName(classname);
        addMap(classname);
        
        String s;        
        if ((s = containsMethod(name,classname))!=null){
            if (_methodsVisibility.get(s).get(name)<visibility)
                throw new RuntimeException("Field was already defined in parent class with higher access level");
        }
        
        _methodsVisibility.get(classname).put(name, visibility);
    }
    
    /***************************************Арифметические операции*****************************************************/
    public stdClass opSub(stdClass other){
        if (this._valueType == TYPE_FLOAT || other._valueType == TYPE_FLOAT){
            return stdClass.fromFloat(this.convertToFloat() - other.convertToFloat());
        }
        
        return stdClass.fromInt(this.convertToInt() - other.convertToInt());
    }
    public stdClass opAdd(stdClass other){
        if (this._valueType == TYPE_FLOAT || other._valueType == TYPE_FLOAT){
            return stdClass.fromFloat(this.convertToFloat() + other.convertToFloat());
        }
        
        return stdClass.fromInt(this.convertToInt() + other.convertToInt());
    }
    public stdClass opMul(stdClass other){
        if (this._valueType == TYPE_FLOAT || other._valueType == TYPE_FLOAT){
            return stdClass.fromFloat(this.convertToFloat() * other.convertToFloat());
        }
        
        return stdClass.fromInt(this.convertToInt() * other.convertToInt());
    }
    public stdClass opDiv(stdClass other){
        if (this._valueType == TYPE_FLOAT || other._valueType == TYPE_FLOAT){
            return stdClass.fromFloat(this.convertToFloat() / other.convertToFloat());
        }
        
        return stdClass.fromInt(this.convertToInt() / other.convertToInt());
    }     
    public stdClass opAnd(stdClass other){
        return stdClass.fromBool(convertToBoolean() && other.convertToBoolean());
    }    
    public stdClass opOr(stdClass other){
        return stdClass.fromBool(convertToBoolean() || other.convertToBoolean());
    }
    public stdClass opPow(stdClass other){
        if (this._valueType == TYPE_INT && other._valueType == TYPE_INT){
            return stdClass.fromInt((int)Math.pow(convertToInt(),other.convertToInt()));
        }
        
        return stdClass.fromFloat((float)Math.pow(this.convertToFloat(),other.convertToFloat()));
    }
    public stdClass opEqual(stdClass other){
        
        switch (this._valueType)
        {
            case TYPE_BOOLEAN:
                return stdClass.fromBool(this.convertToBoolean() == other.convertToBoolean());
            case TYPE_INT:
                return stdClass.fromBool(Math.abs(_intValue - other.convertToFloat())<0.000001);
            case TYPE_FLOAT:
                return stdClass.fromBool(Math.abs(_floatValue - other.convertToFloat())<0.000001);
            case TYPE_ARRAY:
            case TYPE_NULL:
                return stdClass.fromBool(this.convertToInt() == other.convertToInt());
            case TYPE_STR:   
                int i=convertToInt();
                float f=convertToFloat();
                
                if (i==0 && f==0)
                    return stdClass.fromBool(this.convertToString().equals(other.convertToString()));
                
                if (f!=0)
                    return stdClass.fromBool(Math.abs(f - other.convertToFloat())<0.000001);
                
                if (i!=0)
                    return stdClass.fromBool(Math.abs(i - other.convertToFloat())<0.000001);
                
                return stdClass.fromBool(convertToString().equals(other.convertToString())); 
            
            case TYPE_OBJECT:
                return stdClass.fromBool(true);
        }
        
        return null;
    }
    public stdClass opNoneEqual(stdClass other){
        stdClass tmp = opEqual(other);
        
        if (tmp == null)
            return null;
        
        tmp._intValue = Math.abs(1 - tmp._intValue);
        
        return tmp;        
    }
    public stdClass opEqualByType(stdClass other){
        if (_valueType!=other._valueType)
            return stdClass.fromBool(false);
        
        switch (this._valueType)
        {
            case TYPE_BOOLEAN:
                return stdClass.fromBool(this._intValue!=0 && other._intValue!=0);
            case TYPE_INT:
                return stdClass.fromBool(_intValue == other._intValue);
            case TYPE_FLOAT:
                return stdClass.fromBool(Math.abs(_floatValue - other._floatValue)<0.000001);
            case TYPE_ARRAY:
                return stdClass.fromBool(this._array.equals(other._array));
            case TYPE_NULL:
                return stdClass.fromBool(true);
            case TYPE_STR:   
                return stdClass.fromBool(_stringValue.equals(other.convertToString()));             
            case TYPE_OBJECT:
                return stdClass.fromBool(true);
        }
        
        return null;
    }
    public stdClass nonEqualByType(stdClass other){
        stdClass tmp = opEqualByType(other);
        
        if (tmp == null)
            return null;
        
        tmp._intValue = Math.abs(1 - tmp._intValue);
        
        return tmp;  
    }
    public stdClass opLess(stdClass other){
        return stdClass.fromBool(this.convertToFloat() < other.convertToFloat());
    }
    public stdClass opLessOrEqual(stdClass other){
        return opLess(other).opOr(opEqual(other));
    }
    public stdClass opLarger(stdClass other){
        return opLessOrEqual(other).opNot();
    }
    public stdClass opLargerOrEqual(stdClass other){
        return opLess(other).opNot();
    }
    public stdClass opConcat(stdClass other){
        return stdClass.fromString(this.convertToString() + other.convertToString());
    }  
    public stdClass opNot(){
        boolean tmp = this.convertToBoolean();        
        return stdClass.fromBool(!tmp);
    }    
    public stdClass opUminus(){
        switch(_valueType){
            case TYPE_ARRAY:
                throw new RuntimeException("Try to increment array");
            case TYPE_NULL:
                throw new RuntimeException("Try to increment null");
            case TYPE_OBJECT:
                throw new RuntimeException("Try to increment object");
            case TYPE_INT:
                return stdClass.fromInt(-this.convertToInt());
            case TYPE_FLOAT:
                return stdClass.fromFloat(-this.convertToFloat());
            case TYPE_BOOLEAN:
                if (this._intValue!=0)
                    return stdClass.fromInt(-1);
                else
                    return stdClass.fromInt(0);
            case TYPE_STR:
                int i=convertToInt();
                float f=convertToFloat();
                
                if (i==0 && f==0)
                    return stdClass.fromInt(-1);
                
                if (f!=0)
                    return stdClass.fromFloat(-f);
                
                if (i!=0)
                    return stdClass.fromInt(-i);
                
                return stdClass.fromInt(0);
        }
        
        return null;        
    }
    public stdClass opInc(){
        switch(_valueType){
            case TYPE_ARRAY:
                return this.myClone();
            case TYPE_NULL:
                return stdClass.fromInt(1);
            case TYPE_OBJECT:
                throw new RuntimeException("Try to increment object");
            case TYPE_INT:
                return stdClass.fromInt(this.convertToInt()+1);
            case TYPE_FLOAT:
                return stdClass.fromFloat(this.convertToFloat()+1);
            case TYPE_BOOLEAN:
                if (this._intValue!=0)
                    return stdClass.fromBool(true);
                else
                    return stdClass.fromBool(false);
            case TYPE_STR:
                int i=convertToInt();
                float f=convertToFloat();
                
                if (i==0 && f==0)
                    return stdClass.fromInt(1);
                
                if (f!=0)
                    return stdClass.fromFloat(f+1);
                
                if (i!=0)
                    return stdClass.fromInt(i+1);
                
                StringBuilder news = new StringBuilder(_stringValue);
                char last = _stringValue.charAt(_stringValue.length()-1);
                last++;                
                news.setCharAt(_stringValue.length()-1, last);
                return stdClass.fromString(news.toString());
        }
        
        return null;        
    }
    public stdClass opDec(){
        switch(_valueType){
            case TYPE_ARRAY:
                return this.myClone();
            case TYPE_NULL:
                return this.myClone();
            case TYPE_OBJECT:
                throw new RuntimeException("Try to increment object");
            case TYPE_INT:
                return stdClass.fromInt(this.convertToInt()-1);
            case TYPE_FLOAT:
                return stdClass.fromFloat(this.convertToFloat()-1);
            case TYPE_BOOLEAN:
                if (this._intValue!=0)
                    return stdClass.fromBool(true);
                else
                    return stdClass.fromBool(false);
            case TYPE_STR:
                int i=convertToInt();
                float f=convertToFloat();
                
                if (i==0 && f==0)
                    return stdClass.fromInt(-1);
                
                if (f!=0)
                    return stdClass.fromFloat(f-1);
                
                if (i!=0)
                    return stdClass.fromInt(i-1);
                
                return this.myClone();
        }
        
        return null;
    }
    
    /***************************************получение полного имени класса********************************************************/
    private static String toFullClassName(String classname){
        if (classname.indexOf('/')>=0)
            return classname.replace('/', '.');
        
        if (classname.equals("0bject") || classname.equals("stdClass"))
            return "phprtl." + classname;
                
        return "phpprogram." + classname;
    }
    private static String toFullBytecodeName(String classname){
        if (classname.indexOf('/')>=0)
            return classname;
        
        if (classname.indexOf('.')>=0)
            return classname.replace('.', '/');
        
        if (classname.equals("0bject") || classname.equals("stdClass"))
            return "phprtl/" + classname;
                
        return "phpprogram/" + classname;
    }
    
    /*********************Создание объектов и вызов методов с помощью рефлексии*********************/       
    public stdClass __construct(){
        return stdClass.fromNull();
    }
    
    public stdClass convertTo(String classname){
        classname = toFullBytecodeName(classname);
        
        
        try{
            stdClass result = (stdClass)Class.forName(classname.replace('/', '.')).cast(this);            
            result.className = classname;
            return result;
        }catch (Exception ex){
            throw new RuntimeException("Cann't chanage tyepr from '" + className + "' to '" + classname + "'");
        }
    }
    
    public static stdClass newObject(String className){
        className = toFullClassName(className);
        
        try{            
            Class c = Class.forName(className);
        
            Constructor[] construct =  c.getDeclaredConstructors();
            stdClass v=null;            
            
            for(int i=0;i<construct.length;i++){
                if (construct[i].getParameterCount() == 0){
                    v = (stdClass)construct[i].newInstance();
                }
            }
            
           return v;            
        }catch (Exception e){
            throw new IllegalArgumentException("Cann't create obj with name = '" + className + "'");
        }      
    }
    
    public stdClass methodCall(String methodname,stdClass [] params,String callerClass){        
        if (!canCallMethod(methodname,callerClass,this.className))
            throw new IllegalArgumentException("Acces eroor, cann't call method with name = '" + methodname + "'");
                
        // найти класс с данным методом
        String classname = containsMethod(methodname,this.getClass().getName().replace('.', '/'));
        
        try{            
            Class c = Class.forName(toFullClassName(classname));
            
            if (!c.isAssignableFrom(this.getClass()))
                throw new IllegalArgumentException("Cann't call method with name = '" + methodname + "'");
            
            Class [] stdClasses = new Class[params.length];
            for (int i=0;i<params.length;i++)
            {
                stdClasses[i] = stdClass.class;
            }
            
            Method classMethod = c.getDeclaredMethod(methodname, stdClasses);
            
            return (stdClass)classMethod.invoke(this, params);
            
        }catch (Exception e){  
            if (e instanceof InvocationTargetException){
                e.printStackTrace();
            }else{
                System.out.println(e.getMessage());
            }
            
            throw new IllegalArgumentException("Cann't call method with name = '" + methodname + "'");
        }                
    }
    
    public static stdClass staticMethodCall(String methodname,stdClass [] params,String currentClass,String callerClass,stdClass obj){
        
        if (currentClass.equals("self"))
            currentClass = callerClass;
        else if (currentClass.equals("parent"))
        {
            return obj.parentMethodCall(methodname,params,callerClass);            
        }
        
        currentClass = toFullBytecodeName(currentClass);

        // найти класс с указанным методом
        String classname = containsMethod(methodname,currentClass);
        
        try{            
            Class c = Class.forName(classname.replace('/', '.'));
            Class curclass = Class.forName(currentClass.replace('/', '.'));
            
            if (!c.isAssignableFrom(curclass))
                throw new IllegalArgumentException("Cann't call method with name = '" + methodname + "'");
            
            if (!canCallMethod(methodname,callerClass,currentClass))
                throw new IllegalArgumentException("Acces eroor, cann't call method with name = '" + methodname + "'");
            
            Class [] stdClasses = new Class[params.length];
            for (int i=0;i<params.length;i++)
            {
                stdClasses[i] = stdClass.class;
            }
            
            Method classMethod = c.getDeclaredMethod(methodname, stdClasses);
            
            return (stdClass)classMethod.invoke(null, params);
            
        }catch (Exception e){
            if (e instanceof InvocationTargetException)
                e.printStackTrace();
            
            System.out.println(e.getMessage());
            throw new IllegalArgumentException("Cann't call method with name = '" + methodname + "'");
        }
    }
    
    public static stdClass parentStaticMethodCall(String methodname,stdClass [] params, String callerClass){
        try{            
            String parentClass = containsMethod(methodname,Class.forName(callerClass.replace('/','.')).getSuperclass().getName());
            
            if (!canCallMethod(methodname,callerClass,parentClass))
                throw new IllegalArgumentException("Acces eroor, cann't call method with name = '" + methodname + "'");
            
            
            
            Class c = Class.forName(parentClass.replace('/','.'));
            Class thisClass = Class.forName(callerClass.replace('/','.'));
            
            if (!c.isAssignableFrom(thisClass)){
                 throw new IllegalArgumentException("Cann't call parant method with name = '" + methodname + "'");
            }
            
            
            Class [] stdClasses = new Class[params.length];
            for (int i=0;i<params.length;i++)
            {
                stdClasses[i] = stdClass.class;
            }
            
            //найти метод у родителя
            
            Method classMethod = c.getMethod(methodname, stdClasses);
            
            return (stdClass)classMethod.invoke(null, params);
            
        }catch (Exception e){
            if (e instanceof InvocationTargetException)
                e.printStackTrace();
            
            System.out.println(e.getMessage());
            throw new IllegalArgumentException("Cann't call method with name = '" + methodname + "'");
        }      
    }
        
    public stdClass parentMethodCall(String methodname,stdClass [] params, String childCl){
        try{             
            String parentCl = Class.forName(childCl.replace('/', '.')).getSuperclass().getName().replace('.', '/');            
           
            //System.out.println(methodname + " " + childCl);
            
            if (!canCallMethod(methodname,childCl,containsMethod(methodname,parentCl)))
                throw new IllegalArgumentException("Acces eroor, cann't call method with name = '" + methodname + "'");
            
            //System.out.println("can Call parent");
            
            Class c = Class.forName(childCl.replace('/', '.'));
            Class parent = Class.forName(parentCl.replace('/', '.'));
                        
            Class [] stdClasses = new Class[params.length];
            for (int i=0;i<params.length;i++)
            {
                stdClasses[i] = stdClass.class;
            }
            
            Field IMPL_LOOKUP = MethodHandles.Lookup.class.getDeclaredField("IMPL_LOOKUP");
            IMPL_LOOKUP.setAccessible(true);
            MethodHandles.Lookup lkp = (MethodHandles.Lookup) IMPL_LOOKUP.get(null);
            
            MethodHandle h1 = lkp.findSpecial(parent, methodname,MethodType.methodType(stdClass.class,stdClasses),c);       
            
            //System.out.println("Method found");
            
            List<java.lang.Object> args = new ArrayList<>();   
            args.add((java.lang.Object)this);
            for (int i=0;i<params.length;i++)
            {
                args.add((java.lang.Object)params[i]);
            }
            
            //System.out.println("args created");
            
            try {                
                return (stdClass)h1.invokeWithArguments(args);
            } catch (Throwable ex) {
               return parentStaticMethodCall(methodname,params,childCl);
               //throw new IllegalArgumentException("Cann't call method with name = '" + methodname + "'");
            }
            
        }catch (Exception e){
            if (e instanceof InvocationTargetException)
                e.printStackTrace();
            
            if (e instanceof IllegalArgumentException)
                throw new IllegalArgumentException("Acces eroor, cann't call method with name = '" + methodname + "'");
            
            return parentStaticMethodCall(methodname,params,childCl);
            //throw new IllegalArgumentException("Cann't call method with name = '" + methodname + "'");
        }        
    }
    
        
    /**********************************Клонирование объектов***************************************************/    
    public stdClass myClone(){
        try{
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            oos.writeObject(this);
            oos.flush();
            oos.close();
            bos.close();
            byte[] byteData = bos.toByteArray();
            ByteArrayInputStream bais = new ByteArrayInputStream(byteData);
            stdClass result = (stdClass) new ObjectInputStream(bais).readObject();
            
            
            result._fields =(LinkedHashMap<String,HashMap<String,stdClass>>) this._fields.clone();
            result._intValue = this._intValue;
            result._valueType = this._valueType;
            result._floatValue = this._floatValue;
            if (this._stringValue!=null)
                result._stringValue = new String(this._stringValue);
            if (this._array!=null)
                result._array = (Array)this._array.clone();
            
            return result;
        } catch (Exception ex){
            throw new IllegalArgumentException("Cann't clone object");
        }
    }   
    
    public java.lang.Object clone() throws CloneNotSupportedException
    {
        return myClone();
    }
    
    /************Конвертировать в тип без именения объекта*//////////////
    public int convertToInt(){        
        switch (_valueType)
        {
            case TYPE_INT:
            case TYPE_BOOLEAN:
                return this._intValue;
            case TYPE_FLOAT:
                return (int)this._floatValue;
            case TYPE_STR:
                try{
                    Pattern p = Pattern.compile("^\\s*(\\d+).*");  
                    Matcher m = p.matcher(_stringValue);
                    if (m.matches()){
                        return Integer.parseInt(m.group(1));
                    }
                }catch(Exception e){}
                return 0;
            case TYPE_ARRAY:
                if (_array.size()==0)
                    return 0;
                return 1;
            case TYPE_NULL:
                return 0;
            case TYPE_OBJECT:
                return 1;
        }
        
        return 0;
    }       
    public String convertToString(){
        switch (_valueType)
        {
            case TYPE_ARRAY:
                return "Array";
            case TYPE_INT:
                return String.valueOf(_intValue);
            case TYPE_BOOLEAN:
                return _intValue!=0? "true" : "false";
            case TYPE_FLOAT:
                return String.valueOf(_floatValue);
            case TYPE_STR:
                return new String(_stringValue);
            case TYPE_NULL:
                return "";
            case TYPE_OBJECT:
                throw new RuntimeException("Conn't convert object to string");            
        }
        
        return "";
    }    
    public float convertToFloat(){
        
        switch (_valueType)
        {
            case TYPE_ARRAY:
                if (_array.size()==0)
                    return 0;
                return 1;
            case TYPE_INT:
            case TYPE_BOOLEAN:
                return _intValue;
            case TYPE_FLOAT:
                return _floatValue;
            case TYPE_STR:
                try{
                    Pattern p = Pattern.compile("^\\s*([+-]?(([0-9]+|([0-9]*[\\.][0-9]+)|([0-9]+[\\.][0-9]*))[eE][+-]?[0-9]+)).*");  
                    Matcher m = p.matcher(_stringValue);
                    if (m.matches()){
                        return Float.parseFloat(m.group(1));
                    }
                    p = Pattern.compile("^\\s*([+-]?(([0-9]*[\\.][0-9]+)|([0-9]+[\\.][0-9]*)|[0-9]+)).*");  
                    m = p.matcher(_stringValue);
                    if (m.matches()){
                        return Float.parseFloat(m.group(1));
                    }
                }catch (Exception e){}
                try{
                    Pattern p = Pattern.compile("^\\s*(\\d+).+");  
                    Matcher m = p.matcher(_stringValue);
                    if (m.matches()){
                        return Integer.parseInt(m.group(1));
                    }
                }catch(Exception e){}                
                return 0;
            case TYPE_NULL:
                return 0;
            case TYPE_OBJECT:
                return 1;            
        }
        
        return 0;
    }        
    public Array convertToArray(){
        Array tmp = null;
        switch (_valueType)
        {
            case TYPE_ARRAY:
                return _array;
            case TYPE_INT:
                tmp = new Array();
                tmp.put(stdClass.fromInt(_intValue));
                return tmp;
            case TYPE_BOOLEAN:
                tmp = new Array();
                tmp.put(stdClass.fromBool(_intValue==1));
                return tmp;
            case TYPE_FLOAT:
                tmp = new Array();
                tmp.put(stdClass.fromFloat(_floatValue));
                return tmp;
            case TYPE_STR:
                tmp = new Array();
                tmp.put(stdClass.fromString(_stringValue));
                return tmp;
            case TYPE_NULL:
                return new Array();
            case TYPE_OBJECT:
                tmp = new Array();
                
                try{
                    Class current = Class.forName(className.replace('/', '.'));
                    
                    ListIterator<String> iter = new ArrayList<>(_fields.keySet()).listIterator(_fields.size());
                    boolean currentClassFound = false;    
                    while(iter.hasPrevious() && !currentClassFound){
                        String s = iter.previous();
                        currentClassFound = s.equals(className);
                    }
                    
                    if (currentClassFound){
                        // добавить поля текущего класса
                        Iterator it = _fields.get(className).entrySet().iterator();                        
                        while (it.hasNext())
                        {
                            Map.Entry entry = (Map.Entry) it.next();
                            tmp.put(stdClass.fromString((String)entry.getKey()),(stdClass)entry.getValue());
                        }                       
                        
                        // добавить поля родителей
                        while(iter.hasPrevious()){
                            String s = iter.previous();
                            Class parent = Class.forName(s.replace('/', '.'));
                            if (!parent.isAssignableFrom(current))
                                return tmp;
                            
                            it = _fields.get(s).entrySet().iterator();                        
                            while (it.hasNext())
                            {
                                Map.Entry entry = (Map.Entry) it.next();
                                String key = (String)entry.getKey();
                                if (!tmp.containsKey(key))
                                    tmp.put(stdClass.fromString((String)entry.getKey()),(stdClass)entry.getValue());
                            }                       
                        }
                    }
                
                }catch (Exception ex){
                    return tmp;
                }
                return tmp;
        }
        
        return tmp;
    
    }
    public boolean convertToBoolean(){        
        switch (_valueType)
        {
            case TYPE_ARRAY:
                return _array.size()>0;
            case TYPE_INT:
            case TYPE_BOOLEAN:
                return _intValue!=0;
            case TYPE_FLOAT:
                return Math.abs(_floatValue)>0.00001;
            case TYPE_STR:
                return !(_stringValue.equals("0") || _stringValue.equals(""));
            case TYPE_NULL:
                return false;
            case TYPE_OBJECT:
                return true;            
        }
        
        return false;
    }
    public void convertToNull(){
        if (_valueType!=TYPE_NULL)
            throw new RuntimeException("Conn't convert value to NULL"); 
    }
    
    
    /**///////////Конвертировать с изменением текущего объекта/////////////////////
    public stdClass toInt(){
        this._intValue = convertToInt();
        this._valueType = TYPE_INT;
        
        this._stringValue = null;
        this._array = null;
        this._floatValue = 0;
        
        return this;
    }
    public stdClass toStr(){
        this._stringValue = convertToString();
        
        this._valueType = TYPE_STR;
        
        this._intValue = 0;
        this._array = null;
        this._floatValue = 0;
        
        return this;
    }
    public stdClass toFloat(){
        this._floatValue = convertToFloat();        
        
        this._valueType = TYPE_FLOAT;
        
        this._intValue = 0;
        this._array = null;
        this._stringValue = null;
        
        return this;
    }
    public stdClass toArray(){
        this._array = convertToArray();       
        
        this._valueType = TYPE_ARRAY;
        
        this._intValue = 0;
        this._floatValue = 0; 
        this._stringValue = null;
        
        return this;
    }
    public stdClass toBool(){
        this._intValue = convertToBoolean() ? 1 : 0;       
        
        this._valueType = TYPE_BOOLEAN;
        
        this._array = null;
        this._floatValue = 0; 
        this._stringValue = null;
        
        return this;
    }
    public stdClass toNull(){
        this._valueType = TYPE_NULL;
        
        this._intValue = 0;    
        this._array = null;
        this._floatValue = 0; 
        this._stringValue = null;
        
        return this;
    }
}
