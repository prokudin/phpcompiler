/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phprtl;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Admin
 */
public class ArrayIterator {
    protected ArrayIterator(LinkedHashMap<java.lang.Object,stdClass> arr){
        _iter = arr.entrySet().iterator();
    }
    
    public boolean hasNext(){
        return _iter.hasNext();
    }
    
    public void next(){
        _current =(Map.Entry) _iter.next();
    }
    
    public stdClass key(){
        java.lang.Object res = _current.getKey();
        if (res instanceof String)
            return stdClass.fromString((String)res);
        else
            return stdClass.fromInt((Integer)res);
    }
    
    public stdClass value(){
        return (stdClass)_current.getValue();
    }
    
    private Map.Entry _current;
    private Iterator _iter;
}
