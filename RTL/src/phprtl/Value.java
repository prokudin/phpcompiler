/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phprtl;
import java.lang.String;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Admin
 */
class Value {      
    static final int TYPE_INT=45;
    static final int TYPE_STR=49;
    static final int TYPE_FLOAT=47;
    static final int TYPE_ARRAY=48;
    static final int TYPE_BOOLEAN=46;
    static final int TYPE_OBJECT=51;
    static final int TYPE_NULL=53;
    
    /*******              Конструкторы                  ******/
    public Value(){
        this(TYPE_NULL);
    }
    
    public Value(int type){
        _valueType = type;
        
        _intValue = 0;
        _floatValue = 0;
        _stringValue = null;
        _array = null;
    }    
    
    protected int _valueType;
    
    protected int _intValue;
    protected float _floatValue;
    protected String _stringValue;
    protected Array _array;
}
