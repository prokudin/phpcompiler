/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phpprogram;

import phprtl.stdClass;

/**
 *
 * @author Admin
 */
public class PhpClass3 extends PhpClass2{
    public PhpClass3(){
        super();  
        this.className = "phpprogram/PhpClass3";
        this.setMethodVisibility("__construct", 0);
        
        //stdClass.getStaticField("priSF", "phpprogram/PhpClass3", "phpprogram/PhpClass3");
        
//        this.addField("pubF", 0, "phpprogram/PhpClass3",stdClass.fromString("pubF PhpClass3"));
//        this.addField("proF", 1, "phpprogram/PhpClass3",stdClass.fromString("proF PhpClass3"));
//        this.addField("priF", 2, "phpprogram/PhpClass3",stdClass.fromString("priF PhpClass3"));
//        
//        this.addStaticField("pubSF", 0, "phpprogram/PhpClass3",stdClass.fromString("pubSF PhpClass3"));
//        this.addStaticField("proSF", 1, "phpprogram/PhpClass3",stdClass.fromString("proSF PhpClass3"));
//        this.addStaticField("priSF", 2, "phpprogram/PhpClass3",stdClass.fromString("priSF PhpClass3"));
        
        this.setMethodVisibility("pubStM", 0);
//        this.setMethodVisibility("proStM", 1);
//        this.setMethodVisibility("priStM", 2);
        this.setMethodVisibility("pubM", 0);
//        this.setMethodVisibility("proM", 1);
//        this.setMethodVisibility("priM", 2);
    }
    
    public static stdClass pubStM(){
        System.out.println("pubStM from PhpClass3");
        return null;
    }
//    
//    public static stdClass proStM(){
//        System.out.println("proStM from PhpClass3");
//        return null;
//    }
//    
//    public static stdClass priStM(){
//        System.out.println("priStM from PhpClass3");
//        return null;
//    }
//    
    public stdClass pubM(stdClass v1, stdClass v2){
        System.out.println("pubM from PhpClass3");
        //this.parentMethodCall("priM", new stdClass[2],"phpprogram/PhpClass3", "phpprogram/PhpClass2");
       return null;
    }
//    public stdClass proM(stdClass v1, stdClass v2){
//        System.out.println("proM from PhpClass3");
//        return null;
//    }
//    public stdClass priM(stdClass v1, stdClass v2){
//        System.out.println("priM from PhpClass3");
//        return null;
//    }
}
