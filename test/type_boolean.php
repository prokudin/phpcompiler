
<?php

$boolean = true;

$boolean = ((bool) "");        // bool(false)
$boolean =  ((bool) 1);         // bool(true)
$boolean = ((bool) -2);        // bool(true)
$boolean =  ((bool) "foo");     // bool(true)
$boolean = ((bool) 2.3e5);     // bool(true)
$boolean = ((bool) array(12)); // bool(true)
$boolean =  ((bool) array());   // bool(false)
$boolean =  ((bool) "false");   // bool(true)

$t = !0; // This will === true;
$f = !1; // This will === false;

$boolean = (0 == 1); // false
$boolean = (0 == (bool)'all'); // false
$boolean = (0 == 'all'); // TRUE, take care
$boolean = (0 === 'all'); // false

// To avoid this behavior, you need to cast your parameter as string like that :
$boolean = ((string)0 == 'all'); // false

$var1 = TRUE; 
$var2 = FALSE; 

echo $var1; // Will display the number 1 

echo $var2; //Will display nothing 

/* To get it to display the number 0 for 
a false value you have to typecast it: */ 

echo (int)$var2; //This will display the number 0 for false.

$a = !array();      // This will === true;
$a = !array('a');   // This will === false;
$s = !"";           // This will === true;
$s = !"hello";      // This will === false;

?>
