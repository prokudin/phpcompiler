<?php

class c1{	
	// методы

	public function pubM(){
		echo "pubM from class c1\n";
	}	
	protected function proM(){
		echo "proM from class c1\n";
	}	
	private function priM(){
		echo "priM from class c1\n";
	}
	
	// статические
	
	public static function stPubM(){
		echo "stPubM from class c1\n";
	}	
	protected static function stProM(){
		echo "stProM from class c1\n";
	}	
	private static function stPriM(){
		echo "stPriM from class c1\n";
	}
}

class c2 extends c1{
	// методы
	public $y;

	public function pubM(){
		echo "pubM from class c2\n";
	}	
	protected function proM(){
		echo "proM from class c2\n";
	}	
	private function priM(){
		echo "priM from class c2\n";
	}
	
	// статические
	
	public static function stPubM(){
		echo "stPubM from class c2\n";
	}	
	protected static function stProM(){
		echo "stProM from class c2\n";
	}	
	private static function stPriM(){
		echo "stPriM from class c2\n";
	}
}

class c3 extends c2{
	// методы
	public $x;
	
	public function pubM(){
		echo "pubM from class c3\n";
	}	
	protected function proM(){
		echo "proM from class c3\n";
	}	
	private function priM(){
		echo "priM from class c3\n";
	}
	
	// статические
	
	// public static function stPubM(){
		// echo "stPubM from class c3\n";
	// }	
	// protected static function stProM(){
		// echo "stProM from class c3\n";
	// }	
	// private static function stPriM(){
		// echo "stPriM from class c3\n";
	// }
}
$f = 0;

$x = new c3;
c3::stPubM();
$x->stPubM();

$y = new stdClass;
$y->x = 10;
$y->y = 20;

foreach($y as $key => $value){
	echo "$key => $value";
}

$z = (null)[1 => 3];

var_dump((null));

//echo $x->x;

//$x= new c3();
//$x->__construct();
//$x->__destruct();