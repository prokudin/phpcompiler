
%{
 #include <math.h>
 #include <stdlib.h>
 #include <stdio.h>
 #include <string.h>
 #include <conio.h>
 #include <locale.h>
 #include "MyStack.h"
%}

%option noyywrap
%option never-interactive

ID [a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*
TRUE [tT][rR][uU][eE]
FALSE [fF][Aa][lL][sS][eE]

INT_DIGIT_10 [+-]?[1-9][0-9]*|0
INT_DIGIT_16 [+-]?0[xX][0-9a-fA-F]+
INT_DIGIT_8 [+-]?0[0-7]+
INT_DIGIT_2 [+-]?0b[01]+

NUMBER          [0-9]+
DNUM          ([0-9]*[\.]{NUMBER})|({NUMBER}[\.][0-9]*)

FLOAT_NUM [+-]?({DNUM}|{NUMBER})
FLOAT_EXPONENT [+-]?(({NUMBER}|{DNUM})[eE][+-]?{NUMBER})

%x OUT_OF_PHP
%x COMMENT
%x STRING_SINGLE
%x STRING_DOUBLE
%x SIMPLE_SYNTAX
%x COMPLEX_SYNTAX
%x STRING_VARIABLE
%x HEREDOC
%x NOWDOC
%x HTML_COMMENT
%x HTML_STRING_SINGLE
%x HTML_STRING_DOUBLE
%x MUST_RETURN_TOKEN


%%

%{
	static int STATE=OUT_OF_PHP;
	static struct MyStack STATE_STACK = {{0,0,0,0,0,0,0,0,0,0} , 0};
	static int RETURNABLE_TOKEN = -1;
	static int branch = 0;
	static int isVar=0;

	int i=0;
	int int_n=0;
	double double_n=0;
	float float_n=0;
	static char buffer[10000]="";
	static char buffer2[100]="";
	static char single_char_str[2]={'x','\0'};	
	BEGIN(STATE);
%}


<OUT_OF_PHP>\<\!\-\- 			{ STATE=HTML_COMMENT; BEGIN(STATE); strcat(buffer,yytext); }
<HTML_COMMENT>[^\-]+ 			strcat(buffer,yytext);
<HTML_COMMENT>"-" 				strcat(buffer,yytext);
<HTML_COMMENT>\-{2,}\> 			{ STATE=OUT_OF_PHP; BEGIN(STATE); strcat(buffer,yytext); }

<OUT_OF_PHP>\"  				{ STATE=HTML_STRING_DOUBLE; BEGIN(STATE); strcat(buffer,yytext);}
<HTML_STRING_DOUBLE>[^\\\"]+	strcat(buffer,yytext);
<HTML_STRING_DOUBLE>\\\\		strcat(buffer,yytext);
<HTML_STRING_DOUBLE>\\\"		strcat(buffer,yytext);
<HTML_STRING_DOUBLE>\"			{ STATE=OUT_OF_PHP; BEGIN(STATE); strcat(buffer,yytext); }

<OUT_OF_PHP>\'  				{ STATE=HTML_STRING_SINGLE; BEGIN(STATE);}
<HTML_STRING_SINGLE>[^\\\']+	strcat(buffer,yytext);
<HTML_STRING_SINGLE>\\\\		strcat(buffer,yytext);
<HTML_STRING_SINGLE>\\\'		strcat(buffer,yytext);
<HTML_STRING_SINGLE>\'			{ STATE=OUT_OF_PHP; BEGIN(STATE); strcat(buffer,yytext); }


<OUT_OF_PHP>[^\<\'\"]+				strcat(buffer,yytext);
<OUT_OF_PHP>\<\?(php)?(\s|\n|" ") 	{ STATE=MUST_RETURN_TOKEN; BEGIN(STATE); RETURNABLE_TOKEN='p'; printf("\nStatic html found : %s",buffer); buffer[0]='\0'; }
<OUT_OF_PHP>\<						strcat(buffer,yytext);
\?\> 						 		{ STATE = OUT_OF_PHP; BEGIN(STATE); printf("\nEnd of php code found ");  buffer[0]='\0'; }
<OUT_OF_PHP><<EOF>>					{ printf("\nStatic html found : %s",buffer); buffer[0]='\0'; return -1; }
<<EOF>>								{ printf("\nEnd of php code found "); buffer[0]='\0'; return -1;}


"/*"  						{ BEGIN(COMMENT);   }
<COMMENT>[^*]* 				;
<COMMENT>"*"+[^*/]* 		;
<COMMENT>"*"+"/" 			{ BEGIN(INITIAL);  printf("\nMulti line comment was found") ; }

("//"|"#").*       			{ printf("\nSingle line comment was found") ; }


<MUST_RETURN_TOKEN>""/.		{ 
								printf("\nReturnable token found, token = %c",(char)RETURNABLE_TOKEN);
								STATE = pop(&STATE_STACK);  
								BEGIN(STATE); 
							}

"\""								{STATE = STRING_DOUBLE;  BEGIN(STATE);  buffer[0]='\0'; branch = 0;}
<STRING_DOUBLE>[^\\\"\$\{]+		{ strcat(buffer,yytext); 		}
<STRING_DOUBLE,HEREDOC>\\\$			{ strcat(buffer,"$"); }
<STRING_DOUBLE,HEREDOC>\\\{			{ strcat(buffer,"{"); }
<STRING_DOUBLE,HEREDOC>\{\$\}		{ strcat(buffer,"{$}"); }
<STRING_DOUBLE,HEREDOC>\$			{ 
										RETURNABLE_TOKEN=yytext[0];   
										printf ("\nString \"%s\" found",buffer);  
										buffer[0]='\0'; 
										push(&STATE_STACK,STATE); 
										push(&STATE_STACK,SIMPLE_SYNTAX); 
										STATE = MUST_RETURN_TOKEN; BEGIN(STATE); 
									}
<STRING_DOUBLE,HEREDOC>\{			{ /*!!!!! �������� �� \{\$ ��� bison !!!!!*/
										RETURNABLE_TOKEN=yytext[0];   
										printf ("\nString \"%s\" found",buffer);  
										buffer[0]='\0'; 
										push(&STATE_STACK,STATE); 
										push(&STATE_STACK,COMPLEX_SYNTAX); 
										STATE = MUST_RETURN_TOKEN; BEGIN(STATE); 
										branch++;
									}
<STRING_DOUBLE,HEREDOC>\\\\			 { strcat(buffer,"\\");  }
<STRING_DOUBLE,HEREDOC>\\n			 { strcat(buffer,"\n");  }
<STRING_DOUBLE,HEREDOC>\\r			 { strcat(buffer,"\r");}
<STRING_DOUBLE,HEREDOC>\\t			 { strcat(buffer,"\t");}
<STRING_DOUBLE,HEREDOC>\\v			 { strcat(buffer,"\v");}
<STRING_DOUBLE,HEREDOC>\\e			 { strcat(buffer,"\e");}
<STRING_DOUBLE,HEREDOC>\\f			 { strcat(buffer,"\f");}
<STRING_DOUBLE,HEREDOC>\\[0-7]{1,3}  { single_char_str[0]=(char)strtol(yytext+1,NULL,8);  strcat(buffer,single_char_str);}
<STRING_DOUBLE,HEREDOC>\\x[0-9A-Fa-f]{1,2}  {single_char_str[0]=(char)strtol(yytext+2,NULL,16); strcat(buffer,single_char_str);}
<STRING_DOUBLE,HEREDOC>\\u[0-9A-Fa-f]+  { single_char_str[0]=(char)strtol(yytext+2,NULL,16); strcat(buffer,single_char_str);}
<STRING_DOUBLE>\\\"			 { strcat(buffer,"\""); }
<STRING_DOUBLE>\"			 { printf("\nString found : \'%s\', length = %d",buffer,strlen(buffer));  BEGIN(INITIAL); }

<SIMPLE_SYNTAX,COMPLEX_SYNTAX>\{			{ printf ("\nOperator %s found",yytext);  branch++; }
<SIMPLE_SYNTAX,COMPLEX_SYNTAX>{ID}			{ printf ("\nID \"%s\" found",yytext);   }
<SIMPLE_SYNTAX,COMPLEX_SYNTAX>\[			{ printf ("\nOperator \"%s\" found",yytext);   }
<SIMPLE_SYNTAX,COMPLEX_SYNTAX>\]			{ printf ("\nOperator \"%s\" found",yytext);   }
<SIMPLE_SYNTAX,COMPLEX_SYNTAX>{NUMBER}		{ printf ("\nNumber \"%s\" found",yytext);   }
<SIMPLE_SYNTAX,COMPLEX_SYNTAX>"->"			{ printf ("\nOperator \"%s\" found",yytext);   }
<SIMPLE_SYNTAX,COMPLEX_SYNTAX>\n+			{ STATE = pop(&STATE_STACK);  BEGIN(STATE); }
<SIMPLE_SYNTAX>\}							{ branch--; if (branch==0) {printf ("\nOperator %s found",yytext);} else { strcat(buffer,"}"); }  STATE = pop(&STATE_STACK);  BEGIN(STATE);}
<SIMPLE_SYNTAX>""/.							{ STATE = pop(&STATE_STACK);  BEGIN(STATE); }

<COMPLEX_SYNTAX>\$			{ printf ("\nOperator %s found",yytext); }
<COMPLEX_SYNTAX>\}  		{ printf ("\nOoperator %s found",yytext);  branch--;  if (branch==0) { STATE = pop(&STATE_STACK);  BEGIN(STATE); }}
<COMPLEX_SYNTAX>\(			{ printf ("\nOperator %s found",yytext);}
<COMPLEX_SYNTAX>\)			{ printf ("\nOperator %s found",yytext);}
<COMPLEX_SYNTAX>\'			{ 
								STATE = STRING_SINGLE;
								push(&STATE_STACK,COMPLEX_SYNTAX); 
								BEGIN(STATE); buffer[0]='\0';
							}

\'							{ STATE =STRING_SINGLE;  BEGIN(STATE); buffer[0]='\0';}
<STRING_SINGLE>[^\\\']+	{ strcat(buffer,yytext);  }
<STRING_SINGLE>\\\\		    { strcat(buffer,"\\");  }
<STRING_SINGLE>\\n			{ strcat(buffer,"\n");  }
<STRING_SINGLE>\\\'			{ strcat(buffer,"'");  }
<STRING_SINGLE>\'			{ printf("\nString found : %s",buffer);  STATE = pop(&STATE_STACK);  BEGIN(STATE); }


"<<<"\'{ID}\'             { STATE = NOWDOC; BEGIN(STATE); buffer[0]='\0'; strcpy(buffer2,yytext+4); buffer2[strlen(buffer2)-1]='\0'; }
<NOWDOC>[^\s\n;]+			{
								if (strcmp(yytext,buffer2)==0)
								{
									STATE = INITIAL;
									BEGIN(STATE);
									printf("\nString found : %s",buffer);
								}
								else
								{
									strcat(buffer,yytext);
								}
							}
<NOWDOC>\s.*				{ strcat(buffer,yytext); }
<NOWDOC>\n+					{ strcat(buffer,yytext); }
<NOWDOC>.					{ strcat(buffer,yytext); }

"<<<"{ID}					{ STATE = HEREDOC;  BEGIN(HEREDOC); buffer[0]='\0'; strcpy(buffer2,yytext+3); buffer2[strlen(buffer2)]='\0'; }
<HEREDOC>[^\s\n;\$\{\\]+	{
								if (strcmp(yytext,buffer2)==0)
								{
									STATE = INITIAL;
									BEGIN(STATE);
									printf("\nString found : %s",buffer);
									buffer[0]='\0';
								}
								else
								{
									strcat(buffer,yytext);
								}
							}
<HEREDOC>\s.*				{ strcat(buffer,yytext); }
<HEREDOC,STRING_DOUBLE>\n+	{ strcat(buffer,yytext); }
<HEREDOC>;					{ strcat(buffer,yytext); }



array	 	 				{ printf("\nKeyword \"%s\" was found",yytext); }
class 	 					{ printf("\nKeyword \"%s\" was found",yytext); }
const						{ printf("\nKeyword \"%s\" was found",yytext); }
do							{ printf("\nKeyword \"%s\" was found",yytext); }
echo						{ printf("\nKeyword \"%s\" was found",yytext); }
else						{ printf("\nKeyword \"%s\" was found",yytext); }
elseif						{ printf("\nKeyword \"%s\" was found",yytext); }
for							{ printf("\nKeyword \"%s\" was found",yytext); }
foreach						{ printf("\nKeyword \"%s\" was found",yytext); }
as						    { printf("\nKeyword \"%s\" was found",yytext); }
function					{ printf("\nKeyword \"%s\" was found",yytext); }
if							{ printf("\nKeyword \"%s\" was found",yytext); }
new							{ printf("\nKeyword \"%s\" was found",yytext); }
private						{ printf("\nKeyword \"%s\" was found",yytext); }
protected					{ printf("\nKeyword \"%s\" was found",yytext); }
public						{ printf("\nKeyword \"%s\" was found",yytext); }
return						{ printf("\nKeyword \"%s\" was found",yytext); }
static						{ printf("\nKeyword \"%s\" was found",yytext); }
while						{ printf("\nKeyword \"%s\" was found",yytext); }
object 						{ printf("\nKeyword \"%s\" was found",yytext); }
count 						{ printf("\nKeyword \"%s\" was found",yytext); }
parent						{ printf("\nKeyword \"%s\" was found",yytext); }
extends 					{ printf("\nKeyword \"%s\" was found",yytext); }
bool						{ printf("\nKeyword \"%s\" was found",yytext); }
float						{ printf("\nKeyword \"%s\" was found",yytext); }
int							{ printf("\nKeyword \"%s\" was found",yytext); }
string						{ printf("\nKeyword \"%s\" was found",yytext); }
nil							{ printf("\nKeyword \"%s\" was found",yytext); }


";" 						{ printf ("\nOperator \"%s\" was found",yytext);}

"++"						{ printf ("\nOperator \"%s\" was found",yytext);}
"--"						{ printf ("\nOperator \"%s\" was found",yytext);}
"!" 						{ printf ("\nOperator \"%s\" was found",yytext);}
"*" 						{ printf ("\nOperator \"%s\" was found",yytext);}
"**" 						{ printf ("\nOperator \"%s\" was found",yytext);}
"/" 						{ printf ("\nOperator \"%s\" was found",yytext);}
"%" 						{ printf ("\nOperator \"%s\" was found",yytext);}
"+" 						{ printf ("\nOperator \"%s\" was found",yytext);}
"." 						{ printf ("\nOperator \"%s\" was found",yytext);}
"," 						{ printf ("\nOperator \"%s\" was found",yytext);}
"-" 						{ printf ("\nOperator \"%s\" was found",yytext);}
"<" 						{ printf ("\nOperator \"%s\" was found",yytext);}
"<=" 						{ printf ("\nOperator \"%s\" was found",yytext);}
">" 						{ printf ("\nOperator \"%s\" was found",yytext);}
">=" 						{ printf ("\nOperator \"%s\" was found",yytext);}
"==" 						{ printf ("\nOperator \"%s\" was found",yytext);}
"!=" 						{ printf ("\nOperator \"%s\" was found",yytext);}
"===" 						{ printf ("\nOperator \"%s\" was found",yytext);}
"!==" 						{ printf ("\nOperator \"%s\" was found",yytext);}
"&&" 						{ printf ("\nOperator \"%s\" was found",yytext);}
"||" 						{ printf ("\nOperator \"%s\" was found",yytext);}
"=" 						{ printf ("\nOperator \"%s\" was found",yytext);}

"[" 						{ printf ("\nOperator \"%s\" was found",yytext);}
"]"							{ printf ("\nOperator \"%s\" was found",yytext);}
"("							{ printf ("\nOperator \"%s\" was found",yytext);}
")"							{ printf ("\nOperator \"%s\" was found",yytext);}
"{"							{ printf ("\nOperator \"%s\" was found",yytext);}
"}"							{ printf ("\nOperator \"%s\" was found",yytext);}

"::" 						{ printf ("\nOperator \"%s\" was found",yytext);}
"->"						{ printf ("\nOperator \"%s\" was found",yytext);}
"=>" 						{ printf ("\nOperator \"%s\" was found",yytext);}

"$"							{ printf ("\nOperator \"%s\" was found",yytext); isVar = 1;}

{TRUE}                      {printf ("\nConstant TRUE",yytext);}
{FALSE}						{printf ("\nConstant FALSE",yytext);}

{ID}   						{printf ("\nID \"%s\" was found",yytext); isVar =0;}

{INT_DIGIT_10}				{ int_n =(int)strtol(yytext,NULL,10);  printf ("\nNumber \"%d\" was found",int_n);}
{INT_DIGIT_16}				{ int_n = (int)strtol(yytext,NULL,16);   printf ("\nNumber \"%d\" was found",int_n);}
{INT_DIGIT_8}				{ int_n = (int)strtol(yytext,NULL,8);  if (int_n>255) int_n=0;  printf("\nNumber \"%d\" was found",int_n);}
{INT_DIGIT_2}				{  if (yytext[1]=='b'){ int_n =(int)strtol(yytext+2,NULL,2);} else { int_n =(-1)*(int)strtol(yytext+3,NULL,2); }  printf("\nNumber \"%d\" was found",int_n);}

{FLOAT_NUM} 				{ char * ptr; double_n=strtod(yytext,&ptr); printf ("\nNFloat num \"%f\" was found, Str = %s",double_n,yytext); }
{FLOAT_EXPONENT}			{ double_n=atof(yytext); printf ("\nEFloat num \"%f\" was found, Str = %s",double_n,yytext); }

							
" "{1,3}|"\n"|"\n\r"|"\r\n"|"\r"|"\t"|\s	{  /*������� ������ ���� isVar*/}
	
.	printf( "\nUnrecognized character: %s\n", yytext );

%%
/*
main( argc, argv )
int argc;
char **argv;
    {
	setlocale(LC_ALL,"Russian");
	

    ++argv, --argc; 
    if ( argc > 0 )
            yyin = fopen( argv[0], "r" );
    else
            yyin = stdin;

    yylex();
    }

	*/