#pragma once
#include "ClassDescription.h"
#include "ConstantMethodref.h"
#include "tree_structs.h"
#include "VarDescription.h"
#include "GlobalDescriprion.h"
#include <string>
#include <map>

void generateMainClass();

void createClassDescription(ClassStatement * stmt );

void createClassMethodDescription(FunctionStatement * func, char * classname);

void createClassPropertyDescription(ClassProperty * prt, char * classname);

string createDescriptorString(FunctionStatement * func);

void addConstruct(char * classname);

void generateObjectClass();