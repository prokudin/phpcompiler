%{
#include "tree_functions.h"
#include "tree_print.h"
#define YYDEBUG 1
#include <stdio.h>
extern int yylex();
int isOk =1;
void yyerror(char const *s);
struct Program * _yyprg = NULL;
%}

%union {
	int int_const;
	double double_const;
	char * string_const;	
	char * statichtml;	
	char* id;	
	int valuetype;
	int visibilitytype;
	
	struct Program * prgm;
	struct HtmlText * htmltext;
	struct StatementsList * statementsList;
	struct Statement * statement;
	struct IfStatement * ifStatement;
	struct ForStatement * forStatement;
	struct ExpressionList * expressionList;
	struct ForEachStatement * forEachStatement;
	struct WhileDoStatement * whileDoStatement;
	struct WhileDoStatement * doWhileStatement;
	struct FunctionList * functionList;
	struct FunctionStatement * functionStatement;
	struct FormalFunctionArglist * formalFunctionArglist;
	struct ClassStatement * classStatement;
	struct ClassMemberList * classMemberList;
	struct ClassMemberStatement * classMemberStatement;
	struct Array * arr;
	struct Expression * expression;
}



%type <prgm> program
%type <htmltext> doc
%type <statementsList> stmt_list
%type <statement> stmt_incycle
%type <statement> stmt
%type <statementsList> stmt_incycle_listE
%type <statementsList> stmt_incycle_list
%type <valuetype> type
%type <formalFunctionArglist> func_arglistE
%type <formalFunctionArglist> func_arglist
%type <functionStatement> function
%type <classStatement> class_stmt
%type <classMemberList> class_memberlistE
%type <classMemberList> class_memberlist
%type <classMemberStatement> class_member
%type <visibilitytype> classfield_param
%type <visibilitytype> visibility

%type <ifStatement> if_stmt
%type <forStatement> for_stmt
%type <forEachStatement> foreach_stmt
%type <expression> foreach_arr
%type <whileDoStatement> whiledo_stmt
%type <doWhileStatement> dowhile_stmt
%type <expression> expr
%type <expression> expr_var
%type <expressionList> expr_listE
%type <expressionList> expr_list
%type <arr> array
%type <arr> keyvalue_listE
%type <arr> keyvalue_list
%type <expression> echo_stmt
%type <expression> return_stmt
%type <expression> string
%type <expression> value


%token <int_const>INT
%token <double_const>DBL
%token <int_const>BOOL
%token <string_const>STR


%token HTML
%token ARRAY
%token CLASS
%token CONST
%token DO
%token ECHO
%token ELSE
%token ELSEIF
%token FOR
%token FOREACH
%token AS    
%token FUNCTION
%token IF
%token NAMESPACE
%token NEW
%token PRIVATE
%token PROTECTED
%token PUBLIC
%token RETURN
%token STATIC
%token WHILE
%token OBJECT 
%token COUNT 
%token EXTENDS 
%token INC
%token DEC
%token POW
%token INTEGER
%token BOOLEAN
%token STRING
%token DOUBLE
%token <id> ID
%token <statichtml> HTML
%token NIL
%token DARROW

%right '='
%left OR
%left AND
%left EQUAL NONEQUAL EQUAL_BY_TYPE NONEQUAL_BY_TYPE
%left LESS LESS_OR_EQUAL LARGER LARGER_OR_EQUAL
%left '-' '+' '.'
%left '*' '/' '%'
%left UMINUS
%right '!'
%right INC DEC
%right POW
%left '[' ']'
%right NEW CLONE
%left DCOLON ARROW
%nonassoc '(' ')' '{' '}' BGN_COMPL END_COMPL
%right '$'
%%


program: doc			        	{$$ = _yyprg = createProgram($1); }
	;

doc: stmt_list						{$$ = createHtmlText(NULL,$1);}
	| HTML							{$$ = createHtmlText($1,NULL);}
	| doc stmt_list					{$$ = addToHtmlText($1,NULL,$2);}
	| doc HTML						{$$ = addToHtmlText($1,$2,NULL);}
	;

stmt_list: stmt						{$$ = createStatementsList($1);}		
    | stmt_list stmt				{$$ = addStatementToList($1,$2);}	
	;
	
stmt_incycle: 
	';'								{$$ = NULL;}
	| expr ';'						{$$ = createStatement(_EXPR,(void*)$1);   }	
    | if_stmt 						{$$ = createStatement(_IF,(void*)$1);   }
	| for_stmt						{$$ = createStatement(_FOR,(void*)$1);   }
	| whiledo_stmt					{$$ = createStatement(_WHILEDO,(void*)$1);   }
	| dowhile_stmt					{$$ = createStatement(_DOWHILE,(void*)$1);   }
	| foreach_stmt					{$$ = createStatement(_FOREACH,(void*)$1);   }
	| echo_stmt						{$$ = createStatement(_ECHO,(void*)$1);   }
	| return_stmt					{$$ = createStatement(_RETURN,(void*)$1);   }
	| '{' stmt_incycle_listE '}'	{$$ = createStatement(_STMTLIST,(void*)$2);   }
	;	
	
stmt: stmt_incycle					{$$ = $1}
	| function						{$$ = createStatement(_FUNCTION,(void*)$1);   }
	| class_stmt					{$$ = createStatement(_CLASS,(void*)$1);   }
    ;
	
stmt_incycle_listE:					{$$ = NULL;}
	| stmt_incycle_list				{$$ = $1;}
	;
	
stmt_incycle_list: stmt_incycle			{$$ = createStatementsList($1);}
	| stmt_incycle_list stmt_incycle	{$$ = addStatementToList($1,$2);}
	;	
	
type: INTEGER  						{$$ = _INTEGER;}
	| DOUBLE						{$$ = _DOUBLE;}
	| STRING 						{$$ = _STRING;}
	| ARRAY							{$$ = _ARRAY;}
	| BOOLEAN						{$$ = _BOOLEAN;}
	;
	
func_arglistE:							{$$ = NULL;}
	| func_arglist						{$$ = $1;}
	;
	
func_arglist: '$' ID					{$$ = createFormalFunctionArglist(createFunctionArg($2,_NONE,NULL));}
	| ID '$' ID							{$$ = createFormalFunctionArglist(createFunctionArg($3,_USEROBJECT,$1));}
	| type '$' ID						{$$ = createFormalFunctionArglist(createFunctionArg($3,$1,NULL));}
	| func_arglist ',' ID '$' ID		{$$ = addToFormalFunctionArglist($1,createFunctionArg($5,_USEROBJECT,$3));}
	| func_arglist ',' type '$' ID		{$$ = addToFormalFunctionArglist($1,createFunctionArg($5,$3,NULL));}
	| func_arglist ',' '$' ID			{$$ = addToFormalFunctionArglist($1,createFunctionArg($4,_NONE,NULL));}
	
function: FUNCTION ID '(' func_arglistE ')'  '{' stmt_incycle_listE '}'			{$$ = createFunctionStatement($2,0,1,_PUBLIC,$4,_NONE,NULL,$7);}
	| FUNCTION ID '(' func_arglistE ')' ':' type '{' stmt_incycle_listE '}'		{$$ = createFunctionStatement($2,0,1,_PUBLIC,$4,$7,NULL,$9);}
	| FUNCTION ID '(' func_arglistE ')' ':' ID '{' stmt_incycle_listE '}'		{$$ = createFunctionStatement($2,0,1,_PUBLIC,$4,_USEROBJECT,$7,$9);}
	;

class_stmt: CLASS ID '{' class_memberlistE '}'			{ $$ = createClassStatement($2,NULL,$4); }
	| CLASS ID EXTENDS ID '{' class_memberlistE '}'		{ $$ = createClassStatement($2,$4,$6); }
	;

class_memberlistE:								{$$ = NULL;}
	| class_memberlist							{$$ = $1;}
	;
	
class_memberlist: class_member					{$$ = createClassMemberList($1);}
	| class_memberlist class_member				{$$ = addToClassMemberList($1,$2);}
	
class_member: function							{ $$ = createClassFunctionStatement($1,0,_PUBLIC);}
	| visibility function						{ $$ = createClassFunctionStatement($2,0,$1);}
	| STATIC function							{ $$ = createClassFunctionStatement($2,1,_PUBLIC);}
	| classfield_param function					{ $$ = createClassFunctionStatement($2,1,$1);}
	| visibility '$' ID ';'						{ $$ = createClassPropertyStatement($3,0,$1,createExpression(_NIL,NULL,NULL));}
	| visibility '$' ID '=' expr ';'			{ $$ = createClassPropertyStatement($3,0,$1,$5);}
	| classfield_param '$' ID ';'				{ $$ = createClassPropertyStatement($3,1,$1,createExpression(_NIL,NULL,NULL));}
	| classfield_param '$' ID '=' expr ';'		{ $$ = createClassPropertyStatement($3,1,$1,$5);}
	;
	
classfield_param: STATIC visibility				{$$ = $2;}
	| visibility STATIC							{$$ = $1;}
	;
		
visibility: PUBLIC 								{$$ = _PUBLIC;}
	| PRIVATE									{$$ = _PRIVATE;}
	| PROTECTED									{$$ = _PROTECTED;}
	;

if_stmt: IF '(' expr ')' stmt_incycle					{$$ = createIfStatement($3,$5,NULL);}
	| IF '(' expr ')' stmt_incycle ELSE stmt_incycle			{$$ = createIfStatement($3,$5,$7);}
	;
	
for_stmt: FOR '(' expr_listE ';' expr ';' expr_listE ')' stmt_incycle		{$$ = createForStatement($3,$5,$7,$9);}
	| FOR '(' expr_listE ';' ';' expr_listE ')' stmt_incycle		{$$ = createForStatement($3,NULL,$6,$8);}
	;
	
foreach_stmt: FOREACH '(' foreach_arr AS '$' ID ')' stmt_incycle					{$$ = createForEachStatement($3,NULL,createExpression(_DOLLAR,NULL,createIdExpression($6)),$8);}
	| FOREACH '(' foreach_arr AS '$' ID DARROW '$' ID ')' stmt_incycle				{$$ = createForEachStatement($3,createExpression(_DOLLAR,NULL,createIdExpression($6)),createExpression(_DOLLAR,NULL,createIdExpression($9)),$11);}
	;	
	
foreach_arr: array														{$$ = createArrayExpression($1); }	
	| expr_var															{ $$ = $1; }	
	;

whiledo_stmt: WHILE '(' expr ')' stmt_incycle										{$$ = createWhileDoStatement($3,$5);}
	;
	
dowhile_stmt: DO '{' stmt_incycle_listE '}' WHILE '(' expr ')' ';'			{$$ = createDoWhileStatement($7,$3);}
	;
	
expr: expr '+' expr											{$$ = createExpression(_ADD,$1,$3);}
	| expr '-' expr											{$$ = createExpression(_SUB,$1,$3);}
	| expr '*' expr											{$$ = createExpression(_MUL,$1,$3);}
	| expr '/' expr											{$$ = createExpression(_DIVISION,$1,$3);}
	| expr POW expr											{$$ = createExpression(_POW,$1,$3);}
	| '(' expr ')'											{$$ = $2;}
	| '-' expr %prec UMINUS									{$$ = createExpression(_UMINUS,NULL,$2);}
	| expr '=' expr											{$$ = createExpression(_ASSIGN,$1,$3);}	
	| expr OR expr											{$$ = createExpression(_OR,$1,$3);}
	| expr AND expr											{$$ = createExpression(_AND,$1,$3);}
	| expr EQUAL expr										{$$ = createExpression(_EQUAL,$1,$3);}
	| expr NONEQUAL expr									{$$ = createExpression(_NONEEQUAL,$1,$3);}
	| expr EQUAL_BY_TYPE expr								{$$ = createExpression(_EQUAL_BY_TYPE,$1,$3);}
	| expr NONEQUAL_BY_TYPE expr							{$$ = createExpression(_NONEQUAL_BY_TYPE,$1,$3);}
	| expr LESS expr										{$$ = createExpression(_LESS,$1,$3);}
	| expr LESS_OR_EQUAL expr								{$$ = createExpression(_LESS_OR_EQUAL,$1,$3);}
	| expr LARGER expr										{$$ = createExpression(_LARGER,$1,$3);}
	| expr LARGER_OR_EQUAL expr								{$$ = createExpression(_LARGER_OR_EQUAL,$1,$3);}
	| expr '.' expr											{$$ = createExpression(_CONCAT,$1,$3);}
	| '!' expr 												{$$ = createExpression(_NOT,NULL,$2);}	
	| INC expr												{$$ = createExpression(_INC,NULL,$2);}	
	| expr INC												{$$ = createExpression(_INC,$1,NULL);}	
	| DEC expr												{$$ = createExpression(_DEC,NULL,$2);}
	| expr DEC												{$$ = createExpression(_DEC,$1,NULL);}	
	| expr_var									{ if (($1)->type==_ID)  { yyerror("parce error"); YYABORT;}  $$ = $1;}
	| value										{ $$ = $1;}
	| array										{ $$ = createArrayExpression($1);}
	| NIL										{ $$ = createExpression(_NIL,NULL,NULL);}
	;

expr_var: ID							{$$ = createIdExpression($1);}
	| '$' expr_var						{$$ = createExpression(_DOLLAR,NULL,$2);}
	| '$' '{' expr '}'					{$$ = createExpression(_DOLLAR,NULL,$3);}		
	| expr_var '[' expr ']'				{if (($1)->type==_ID) { yyerror("parce error"); YYABORT;} $$ = createExpression(_SQ_BRACKET,$1,$3);}
	| expr_var '[' ']' '=' expr			{$$ = createExpression(_ARRAY_PUSHBACK,$1,$5);}
	| expr_var '(' expr_listE ')'		{$$ = createFuncCallExpression($1,$3);}	
	| expr_var ARROW expr_var			{if (($1)->type==_ID)  { yyerror("parce error"); YYABORT;} $$ = createExpression(_ARROW,$1,$3);}
	| expr_var ARROW '{' expr '}'		{if (($1)->type==_ID)  { yyerror("parce error"); YYABORT;} $$ = createExpression(_ARROW,$1,$4);}
	| expr_var DCOLON expr_var			{ if (($1)->type == _NEWOBJ)  { yyerror("parce error"); YYABORT;}  $$ = createExpression(_DCOLON,$1,$3); }
	| expr_var DCOLON '{' expr '}'		{ if (($1)->type == _NEWOBJ)  { yyerror("parce error"); YYABORT;}  $$ = createExpression(_DCOLON,$1,$4);}	
	| NEW expr_var						{ $$ = createNewObjExpressionFromFuncCall($2); }
	| CLONE expr_var					{ if (($2)->type == _SQ_BRACKET || ($2)->type == _FUNCTION || ($2)->type == _ID)  { yyerror("parce error"); YYABORT;}  $$ = createExpression(_CLONE,NULL,$2); }
	| '(' expr_var ')'					{$$ = $2;}
	;
	


expr_listE:										{$$ = NULL;}
	| expr_list									{$$ = $1;}
	;
	
expr_list: expr									{$$ = createExpressionList($1);}
	| expr_list ',' expr						{$$ = addToExpressionList($1,$3);}
	;	
	
array: ARRAY '(' keyvalue_listE ')'		 		{$$ = $3;}
	| '[' keyvalue_listE ']'					{$$ = $2;}
	;

keyvalue_listE:									{$$ = NULL;}
	| keyvalue_list								{$$ = $1;}
	;
	
keyvalue_list: expr DARROW expr					{$$ = createArray($1,$3);}
	| expr										{$$ = createArray(NULL,$1);}
	| keyvalue_list ',' expr 				    {$$ = addToArray($1,NULL,$3);}
	| keyvalue_list ',' expr DARROW expr		{$$ = addToArray($1,$3,$5);}
	;
	
echo_stmt: ECHO expr ';'						{$$ = $2;}
	;
	
return_stmt: RETURN expr ';'					{$$ = $2; }
	;
	
string: STR									{$$ = createStringExpression($1);} 
	| string expr_var STR						{$$ = createExpression(_CONCAT,createExpression(_CONCAT,$1,$2),createStringExpression($3));}
	| string BGN_COMPL expr END_COMPL STR	{$$ = createExpression(_CONCAT,createExpression(_CONCAT,$1,$3),createStringExpression($5));}
	;	
	
value: INT							{$$ = createIntExpression($1);}
	| DBL							{$$ = createDoubleExpression($1);}
	| string						{$$ = $1;}
	| BOOL							{$$ = createBooleanExpression($1);}
	;

%%
void yyerror(char const *s)
{
	printf("%s",s);
}

/*
yywrap()
{
    return(1);
}
*/
