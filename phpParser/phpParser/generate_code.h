#pragma once
#pragma comment(lib, "Ws2_32.lib")
//#include "swap.h"
#include <fstream>
#include <assert.h>
#include <stdio.h>
#include "IConstant.h"
#include <vector>
#include "GlobalDescriprion.h"
#include "ConstantFieldref.h"
#include <io.h>
#include <fcntl.h>
#include <stdint.h>
#include <winsock2.h>
#include <cstdio>  
#include <map>
#include <list>


enum ARG_TYPE
{
	_U1,
	_U2,
	_U4,
	_S1,
	_S2,
	_S4
};



struct BytecodeInfo{
	map<string, unsigned char> comand;
	map<string, unsigned char> comandSize;
	map<int,string> const_i;
};

extern "C" struct Program * _yyprg;


using namespace std;

void fillBytecodeInfo();

void generateBytecode();

void generateMainClassBytecode();

void generateClassBytecode(string classname);

void createMainMethod(int descr);
void createMainMethodReadline(int descr);
list<pair<string,list<pair<int,int>>>> createMainMethodReadlineCode();

void createMainConstruct(int descr);

list<pair<string,list<pair<int,int>>>> createConstructCode(string & classname);

void createMethod(int descr, string & classname,string & methodname);

void createConstruct(int descr, string & classname);

list<pair<string,list<pair<int,int>>>> createMainConstructCode();

list<pair<string,list<pair<int,int>>>> createMainMethodCode();

list<pair<string,list<pair<int,int>>>> generateMethodBytecode(ClassMethodDescription * method, string & classname);

list<pair<string,list<pair<int,int>>>> generateStatementsListBytecode(StatementsList * stmts,string & classname,int varIndex);

void generateIfStmtBytecode(IfStatement * stmt,list<pair<string,list<pair<int,int>>>> & bytecode, string & classname,int varIndex);

void generateWhileDoStmtBytecode(WhileDoStatement * cycle, list<pair<string,list<pair<int,int>>>> & bytecode, string & classname,int varIndex);

void generateDoWhileStmtBytecode(WhileDoStatement * cycle,list<pair<string,list<pair<int,int>>>> & bytecode, string & classname,int varIndex);

void generateForeachStmtBytecode(ForEachStatement * cycle,list<pair<string,list<pair<int,int>>>> & bytecode, string & classname,int varIndex);

list<pair<string,list<pair<int,int>>>> generateExpressionBytecode(Expression * expr, string & classname,int varIndex);

int bytecodeLength(list<pair<string,list<pair<int,int>>>> & bytecode);

void writeBytecode(int descr,list<pair<string,list<pair<int,int>>>> & bytecode);