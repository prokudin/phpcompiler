#pragma once
#include "tree_structs.h"
#include "tables_generator.h"
#include "ClassMethodDescription.h"
#include "ClassPropertyDescription.h"
#include "ConstantInteger.h"
#include "ConstantFloat.h"
#include "ConstantUtf8.h"
#include "ConstantString.h"
#include "stdlib.h"
#include <list>

using namespace std;

extern "C" struct Program * createProgram(struct HtmlText * htmltext);
extern "C" struct HtmlText * createHtmlText(char * html, struct StatementsList * list);
extern "C" struct HtmlText * addToHtmlText(struct HtmlText * htmltext,char * html, struct StatementsList * list);
extern "C" struct StatementsList *  createStatementsList(struct Statement * stmt);
extern "C" struct StatementsList * addStatementToList(struct StatementsList * list,struct Statement * stmt);
extern "C" struct Statement * createStatement(int type, void * statement);
extern "C" struct IfStatement * createIfStatement(struct Expression * expr, struct Statement * thenStmts, struct Statement * elseStmts);
extern "C" struct ForStatement * createForStatement(struct ExpressionList * expr1,struct Expression * expr2,struct ExpressionList * expr3,struct Statement * statement);
extern "C" struct ForEachStatement * createForEachStatement(struct Expression * arr,struct Expression * key,struct Expression * value,struct Statement * statement);
extern "C" struct WhileDoStatement * createWhileDoStatement(struct Expression * expr,struct Statement * statement);
extern "C" struct WhileDoStatement * createDoWhileStatement(struct Expression * expr,struct StatementsList * statement);
extern "C" struct Expression * createNewObjExpressionFromFuncCall(struct Expression * funccall);
extern "C" struct ExpressionList * createExpressionList(struct Expression * expr);
extern "C" struct ExpressionList * addToExpressionList(struct ExpressionList * list,struct Expression * expr);
extern "C" struct FunctionStatement * createFunctionStatement(char * id, int isMethod, int isStatic, int visibility, struct FormalFunctionArglist * arglist, int returncode, char * returnObjectId,struct StatementsList * stmts);
extern "C" struct ClassStatement * createClassStatement(char * id, char * parentID,struct ClassMemberList * members);
extern "C" struct ClassMemberList * createClassMemberList(struct ClassMemberStatement * member);
extern "C" struct ClassMemberList * addToClassMemberList(struct ClassMemberList * list,struct ClassMemberStatement * member);
extern "C" struct ClassMemberStatement * createClassFunctionStatement(struct FunctionStatement * function,int isStatic, int visibility);
extern "C" struct ClassMemberStatement * createClassPropertyStatement(char * id,int isStatic, int visibility, struct Expression * value);
extern "C" struct Expression * createExpression(int type,struct Expression * left, struct Expression * right);
extern "C" struct Array * createArray(struct Expression * key, struct Expression * value);
extern "C" struct Array * createArrayL(struct ExpressionList * list);
extern "C" struct Array * addToArray(struct Array *arr,struct Expression * key, struct Expression * value);
extern "C" struct Array * addToArrayL(struct Array *arr,struct ExpressionList * list);
extern "C" struct FunctionArg * createFunctionArg(char * id,int vartype,char * userobjectname);
extern "C" struct FormalFunctionArglist * createFormalFunctionArglist(struct FunctionArg * arg);
extern "C" struct FormalFunctionArglist * addToFormalFunctionArglist(struct FormalFunctionArglist *list,struct FunctionArg * arg);
extern "C" struct Expression * createIntExpression(int value);
extern "C" struct Expression * createDoubleExpression(double value);
extern "C" struct Expression * createStringExpression(char * value);
extern "C" struct Expression * createFuncCallExpression(struct Expression * callexpr, struct ExpressionList * params);
extern "C" struct Expression * createNewObjExpression(struct Expression * callexpr, struct ExpressionList * params);
extern "C" struct Expression * createArrayExpression(struct Array * value);
extern "C" struct Expression * createBooleanExpression(int value);
extern "C" struct Expression * createIdExpression(char * id);

#include "tree_mod.h"

void treeModification(struct Program * prg);

void createClassTables(struct StatementsList * stmts);

void addParentCallFromonstructors(struct ClassStatement * stmt);

void movePropertyDefinitionsToConstruct(struct ClassStatement * stmt);

StatementsList * statementsListModification(struct StatementsList * stmts,char * classname);
bool isParent(string parent , string clazz);
void changeClassNameAndAddParent(struct ClassStatement * stmt);

void classModification(struct ClassStatement * stmt);

char * createConstructFunctionName(struct ClassStatement * stmt, struct FunctionStatement * constr);

int containsParantConstuctCall(struct FunctionStatement * constr);

bool addConstructDestruct(struct ClassStatement * stmt);

void exprListModification(struct ExpressionList * list,char * classname);

void exprModification(struct Expression * expr,char * classname);

struct Expression * mergeAssignAndSqBracket(struct Expression * expr);

void addReturnStatement(FunctionStatement * func);

void addFunctionArgsToConstantTabe(FormalFunctionArglist * args, char * classname);

StatementsList * forReplacement(struct StatementsList * stmts);

StatementsList * forEachReplacement(struct StatementsList * stmts);

ClassMethodDescription * containsMethodGlobal (string methodname);

ClassPropertyDescription * containsPropertyGlobal (string propname);

ClassMethodDescription * containsMethod (string classname,string methodname);

ClassPropertyDescription * containsProperty (string classname,string propname);

bool isChildClass(string child,string parent);

void checkMethods();
