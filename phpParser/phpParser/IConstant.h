#pragma once
#include <stdio.h>
#include <string>
#include "swap.h"
#include <fstream>
#include <assert.h>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <stdint.h>
#include <winsock2.h>
#include <cstdio>  

enum EConstType
{
	_C_UTF8=1,
	_C_INTEGER=3,
	_C_FLOAT=4,
	_C_STRING=8,
	_C_NAME_AND_TYPE=12,
	_C_CLASS=7,
	_C_FIELDREF=9,
	_C_METHODREF=10
};

using namespace std;

class IConstant
{
public:
	int getType() {return type;}
	void setType(int type) { this->type = type; } 
	void setNumber(int number) { this->number = number; }
	int getNumber() { return number; }
	virtual bool equal(IConstant * other)=0;
	virtual void print()=0;
	virtual std::string toCSV()=0;
	virtual void toBytecode(int descr)=0;
	virtual ~IConstant(){}
protected:
	IConstant() : type(-1) {}
	IConstant(int type) { this->type = type; }	
	int type;
	int number;
};