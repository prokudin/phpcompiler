#pragma once

struct MyStack
{
	int arr[10];
	int index;
};


void push(struct MyStack * stack, int val)
{
	stack->arr[++stack->index] = val;
}

int pop(struct MyStack * stack)
{
	return stack->arr[(stack->index>0)?stack->index--:stack->index];
}