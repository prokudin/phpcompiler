#pragma once
#include "IConstant.h"
#include <stdio.h>
#include <math.h>

class ConstantFloat : public IConstant
{
public:
	ConstantFloat(float value) : IConstant(_C_FLOAT) { this->value = value; }
	~ConstantFloat(){}
	float value;
	virtual bool equal(IConstant * other){ return this->getType()==other->getType() && abs(value - ((ConstantFloat*)other)->value)<=0.000001; }
	virtual void print(){ printf("\t\t%d\tFloat\t%f\n",number,value);}
	virtual string toCSV(){ return std::to_string((long long)number) + ";Float;" + std::to_string((long double)value) + "\n"; }
	virtual void toBytecode(int descr)
	{
		unsigned char u1 = 4;
		write(descr,(void*)&u1,1);

		s4F S4;
		S4.number = value;
		swap(S4.bytes,4);		
		write(descr,(void*)&(S4.number),4);
	}
};