#pragma once
#include "tree_structs.h"

#pragma once
#include <string.h>
#include <stdlib.h>

FILE * dotfile;
int nodeid;
char nodename[70];

#include "tree_print.h"

void printNode(int id, char * label);
void printTransition(int prev, int next);
int print(int prev, char * label);
void printFromTo(int prev,int next, char * label);

void printProgram(struct Program * prg);
void printHtmlText(struct HtmlText * html, int previd);
void printStatementsList(struct StatementsList *stmts,int previd);
void printStatement(struct Statement *stmt,int previd);
void printIfStatement(struct IfStatement * stmt, int previd);
void printForStatement(struct ForStatement * stmt, int previd);
void printForEachStatement(struct ForEachStatement * stmt, int previd);
void printWhileDoStatement(struct WhileDoStatement* stmt, int previd, char * cycle);
void printFunctionStatement(struct FunctionStatement* stmt, int previd);
void printFormalFunctionArglist(struct FormalFunctionArglist * arglist,int previd);
void printFunctionArg(struct FunctionArg * arg, int previd);
void printClassStatement(struct ClassStatement * stmt,int previd);
void printClassMemberList(struct ClassMemberList * list, int previd);
void printClassMemberStatement(struct ClassMemberStatement * stmt, int previd);
void printVisibility(int visibility,int previd);
void printType(int type,int previd,char * objtype);
void printExpression(struct Expression * expr, int previd);
void printExprOperation(struct Expression * expr, int previd);
void printArray(struct Array * arr, int previd);
void printFuncCall(struct FuncCall * call, int previd);
void printNewObj(struct FuncCall * call, int previd);
void printExpressionList(struct ExpressionList *stmts,int previd);
