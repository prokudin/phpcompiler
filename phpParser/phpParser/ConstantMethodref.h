#pragma once
#include "IConstant.h"
#include "ConstantClass.h"
#include "ConstantNameAndType.h"
#include <stdio.h>

class ConstantMethodref : public IConstant
{
public:
	ConstantMethodref(ConstantClass * _class,ConstantNameAndType * name_and_type) : IConstant(_C_METHODREF) { this->_class = _class; this->name_and_type = name_and_type;}
	~ConstantMethodref(){ if (_class) delete _class;   if (name_and_type) delete name_and_type;}
	ConstantClass * _class;
	ConstantNameAndType * name_and_type;
	virtual bool equal(IConstant * other){ return this->getType() == other->getType() && this->_class->equal(((ConstantMethodref*)other)->_class) && this->name_and_type->equal(((ConstantMethodref*)other)->name_and_type); }
	virtual void print(){ printf("\t\t%d\tMetRef\t%d,%d\n",number,_class->getNumber(),name_and_type->getNumber());}
	virtual string toCSV(){ return std::to_string((long long)number) + ";Methodref;" + std::to_string((long long)_class->getNumber()) + " " + std::to_string((long long)name_and_type->getNumber()) + "\n";  }
	virtual void toBytecode(int descr)
	{
		unsigned char u1 = 10;
		write(descr,(void*)&u1,1);

		unsigned short u2 = htons(_class->getNumber());
		write(descr,(void*)&u2,2);

		u2 = htons(name_and_type->getNumber());
		write(descr,(void*)&u2,2);
	}
};