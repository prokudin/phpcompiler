#include "GlobalDescriprion.h"

map<string,ClassDescription*> GlobalDescription::classes;

void GlobalDescription::print()
{
	for (auto i=classes.cbegin();i!=classes.cend();++i)
	{
		printf("\n\n");
		i->second->print();
		printf("\t\t____________________________________________________");
		printf("\n\n");
	}
}


void GlobalDescription::toCSV()
{
	for (auto i=classes.cbegin();i!=classes.cend();++i)
	{
		ofstream myfile;

		string filename = i->first;

		for (int j=0;j<filename.size();j++)
		{
			if (filename[j] == '<' || filename[j] == '>' || filename[j] == '/')
				filename = filename.erase(j,1);
		}

		filename = "Constants\\" + filename + ".csv";
		
		myfile.open(filename,std::ofstream::out);
		

		for (auto j=0;j!=i->second->constants.size();++j)
		{
			myfile<<i->second->constants[j]->toCSV();
		}

		myfile.close();
	}
}