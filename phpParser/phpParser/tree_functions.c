#include "tree_functions.h"

struct Program * createProgram(struct HtmlText * htmltext)
{
	struct Program * prg = (struct Program *)malloc(sizeof(struct Program));

	prg->stmts = htmltext;

	return prg;
}

struct HtmlText * createHtmlText(char * html, struct StatementsList * list)
{
	struct HtmlText * htmltext = (struct HtmlText *)malloc(sizeof(struct HtmlText));

	htmltext->html = html;
	htmltext->stmts = list;
	htmltext->next=NULL;

	return htmltext;
}


struct HtmlText * addToHtmlText(struct HtmlText * htmltext,char * html, struct StatementsList * list)
{
	struct HtmlText * next = (struct HtmlText *)malloc(sizeof(struct HtmlText));
	struct HtmlText * root = htmltext;

	while (htmltext->next!= NULL)
	{
		htmltext = htmltext->next;
	}

	htmltext->next = next;

	next->html = html;
	next->stmts = list;
	next->next=NULL;

	return root;
}

struct StatementsList * createStatementsList(struct Statement * stmt)
{	
	struct StatementsList * list;
	
	if (!stmt)
		return NULL;

	
	list = (struct StatementsList *)malloc(sizeof(struct StatementsList));
	


	list->stmt = stmt;
	list->next = NULL;

	return list;
}

struct StatementsList * addStatementToList(struct StatementsList * list,struct Statement * stmt)
{	
	struct StatementsList *current = list;
	struct StatementsList *next;

	if (!stmt)
		return list;
	
	next = (struct StatementsList *)malloc(sizeof(struct StatementsList));
	
	while (current -> next!= NULL)
	{
		current = current->next; 
	}
	
	next -> stmt = stmt;
	next ->next = NULL;
	current -> next = next;
	
	return list;
}

struct Statement * createStatement(enum EStatementType type, void * statement)
{
	struct Statement * stmt = (struct Statement *)malloc(sizeof(struct Statement));
	
	stmt->code = type;
	
	switch (type)
	{
		case _EXPR:
			stmt->stmt.exprStmt = (struct Expression *)statement;
			break;
		case _IF:
			stmt->stmt.ifStmt = (struct IfStatement *)statement;
			break;
		case _FOR:
			stmt->stmt.forStmt = (struct ForStatement *)statement;
			break;
		case _FOREACH:
			stmt->stmt.foreachStmt = (struct ForEachStatement *)statement;
			break;
		case _WHILEDO:
			stmt->stmt.whiledoStmt = (struct WhileDoStatement *)statement;
			break;
		case _DOWHILE:
			stmt->stmt.dowhileStmt = (struct WhileDoStatement *)statement;
			break;
		case _ECHO:
			stmt->stmt.echoStmt = (struct Expression *)statement;
			break;
		case _FUNCTION:
			stmt->stmt.funcStmt = (struct FunctionStatement *)statement;
			break;
		case _CLASS:
			stmt->stmt.classStmt = (struct ClassStatement *)statement;
			break;
		case _RETURN:
			stmt->stmt.returnStmt = (struct Expression *)statement;
			break;
		case _STMTLIST:
			stmt->stmt.stmtlist = (struct StatementsList *)statement;
			break;
	}	
	
	return stmt;
}

struct IfStatement * createIfStatement(struct Expression * expr, struct Statement * thenStmts, struct Statement * elseStmts)
{	
	struct IfStatement * stmt = (struct IfStatement *)malloc(sizeof(struct IfStatement));
	
	stmt -> expr = expr;

	if (thenStmts)
	{
		if (thenStmts->code == _STMTLIST)
		{
			stmt -> thenlist = thenStmts->stmt.stmtlist;
		}
		else
		{
			stmt -> thenlist  = createStatementsList(thenStmts);
		}
	}
	else
	{
		stmt -> thenlist  = NULL;
	}

	if (elseStmts)
	{
		if (elseStmts->code == _STMTLIST)
		{
			stmt -> elselist = elseStmts->stmt.stmtlist;
		}
		else
		{
			stmt -> elselist = createStatementsList(elseStmts);
		}
	}
	else
	{
		stmt -> elselist  = NULL;
	}
	
	return stmt;
}

struct ForStatement * createForStatement(struct ExpressionList * expr1,struct Expression * expr2,struct ExpressionList * expr3,struct Statement * statement)
{
	
	struct ForStatement * stmt = (struct ForStatement *)malloc(sizeof(struct ForStatement));
	
	stmt -> first = expr1;
	stmt -> second = expr2;
	stmt -> third = expr3;

	if (statement->code == _STMTLIST)
	{
		stmt -> stmts = statement->stmt.stmtlist;
	}
	else
	{
		stmt -> stmts = createStatementsList(statement);
	}
	
	return stmt;
}

struct ForEachStatement * createForEachStatement(struct Expression * arr,struct Expression * key,struct Expression * value,struct Statement * statement)
{
	
	struct ForEachStatement * stmt = (struct ForEachStatement *)malloc(sizeof(struct ForEachStatement));
	
	stmt -> arr = arr;
	stmt -> key = key;
	stmt -> value = value;

	if (statement->code == _STMTLIST)
	{
		stmt -> stmts = statement->stmt.stmtlist;
	}
	else
	{
		stmt -> stmts = createStatementsList(statement);
	}
	
	return stmt;
	
}

struct WhileDoStatement * createWhileDoStatement(struct Expression * expr,struct Statement * statement)
{	
	struct WhileDoStatement * stmt = (struct WhileDoStatement *)malloc(sizeof(struct WhileDoStatement));
	
	stmt -> expr = expr;

	if (statement->code == _STMTLIST)
	{
		stmt -> stmts = statement->stmt.stmtlist;
	}
	else
	{
		stmt -> stmts = createStatementsList(statement);
	}
	
	return stmt;
}

struct WhileDoStatement * createDoWhileStatement(struct Expression * expr,struct StatementsList * statement)
{	
	struct WhileDoStatement * stmt = (struct WhileDoStatement *)malloc(sizeof(struct WhileDoStatement));
	
	stmt -> expr = expr;
	
	stmt -> stmts = statement;	
	
	return stmt;
}

struct ExpressionList * createExpressionList(struct Expression * expr)
{	
	struct ExpressionList * stmt = (struct ExpressionList *)malloc(sizeof(struct ExpressionList));
	
	stmt -> expr = expr;
	stmt -> next = NULL;
	
	return stmt;
}

struct ExpressionList * addToExpressionList(struct ExpressionList * list,struct Expression * expr)
{	
	struct ExpressionList * result = list;
	struct ExpressionList * stmt = (struct ExpressionList *)malloc(sizeof(struct ExpressionList));
	
	while (list ->next !=NULL)	
	{
		list = list-> next;	
	}
	
	list-> next = stmt;
	
	stmt -> expr = expr;
	stmt -> next = NULL;
	
	return result;
	
}

struct FunctionStatement * createFunctionStatement(char * id, int isMethod, int isStatic, int visibility, struct FormalFunctionArglist * arglist, int returncode, char * returnObjectId,struct StatementsList * stmts)
{
	
	struct FunctionStatement * stmt = (struct FunctionStatement *)malloc(sizeof(struct FunctionStatement));
	
	stmt -> id = id;
	stmt -> isMethod = isMethod;
	stmt -> isStatic = isStatic;
	stmt -> visibility = visibility;
	stmt -> arglist = arglist;
	stmt -> returncode = returncode;
	stmt -> returnObjectId = returnObjectId;
	stmt -> stmts = stmts;
	
	return stmt;	
}

struct ClassStatement * createClassStatement(char * id, char * parentID,struct ClassMemberList * members)
{
	
	struct ClassStatement * stmt = (struct ClassStatement *)malloc(sizeof(struct ClassStatement));
		
	stmt -> id = id;
	stmt -> parentID = parentID;
	stmt -> members = members;
	
	return stmt;	
}

struct ClassMemberList * createClassMemberList(struct ClassMemberStatement * member)
{	
	struct ClassMemberList * stmt = (struct ClassMemberList *)malloc(sizeof(struct ClassMemberList));	

	stmt -> stmt = member;
	stmt ->  next = NULL;
	
	return stmt;	
}

struct ClassMemberList * addToClassMemberList(struct ClassMemberList * list,struct ClassMemberStatement * member)
{	
	struct ClassMemberList * result = list;
	struct ClassMemberList * stmt = (struct ClassMemberList *)malloc(sizeof(struct ClassMemberList));
	
	while (list -> next != NULL)	
	{
		list=list->next;
	}
		
	list -> next = stmt;

	stmt -> stmt = member;
	stmt -> next = NULL;
	
	return result;		
}


struct ClassMemberStatement * createClassFunctionStatement(struct FunctionStatement * function,int isStatic, int visibility)
{
	struct ClassMemberStatement * stmt = (struct ClassMemberStatement *)malloc(sizeof(struct ClassMemberStatement));
	
	function -> isMethod = 1;
	function -> isStatic = isStatic;
	function -> visibility = visibility;
	
	stmt -> function = function;
	stmt -> prt = NULL;
	
	return stmt;	
}

struct ClassMemberStatement * createClassPropertyStatement(char * id,int isStatic, int visibility, struct Expression * value)
{
	struct ClassMemberStatement * stmt = (struct ClassMemberStatement *)malloc(sizeof(struct ClassMemberStatement));
	
	struct ClassProperty * prt = (struct ClassProperty *)malloc(sizeof(struct ClassProperty));
	
	prt -> id = id;
	prt -> isStatic = isStatic;
	prt -> visibility = visibility;
	prt -> value = value;
	
	stmt -> function = NULL;
	stmt -> prt = prt;
	
	return stmt;
}


struct Expression * createExpression(int type,struct Expression * left, struct Expression * right)
{
	struct Expression * stmt = (struct Expression *)malloc(sizeof(struct Expression));

	stmt->type = type;
	stmt ->value.d = 0;
	stmt->left = left;
	stmt->right = right;
	stmt->center = NULL;
	stmt->constIndex = -1;
	
	return stmt;
}

struct Array * createArray(struct Expression * key, struct Expression * value)
{
	
	struct Array * arr = (struct Array *)malloc(sizeof(struct Array));
	struct ArrayMember * member = (struct ArrayMember *)malloc(sizeof(struct ArrayMember));

	member -> key = key;
	member -> value = value;
	
	arr -> value = member;
	arr -> next = NULL;
	
	return arr;
}

struct Array * createArrayL(struct ExpressionList * list)
{
	struct Array * root = NULL;
	struct Array * next=NULL,* prev=NULL;
	struct ArrayMember * nextmemb = NULL;

	while (list != NULL)
	{
		next = (struct Array *)malloc(sizeof(struct Array));

		nextmemb = (struct ArrayMember *)malloc(sizeof(struct ArrayMember));

		if (root == NULL)
			root = next;
		else
			prev->next = next;

		nextmemb -> key = NULL;
		nextmemb -> value = list -> expr;
		
		next -> value = nextmemb;
		next -> next = NULL;
		
		list = list -> next;
		prev=next;
	}

	return root;
}

struct Array * addToArray(struct Array *arr,struct Expression * key, struct Expression * value)
{	
	struct Array * result = arr;
	struct Array * next = (struct Array *)malloc(sizeof(struct Array));	
	struct ArrayMember * member = (struct ArrayMember *)malloc(sizeof(struct ArrayMember));
	
	while (arr-> next !=NULL)
	{
		arr = arr -> next;	
	}
	
	arr -> next = next;
	
	member -> key = key;
	member -> value = value;
	
	next -> value = member;
	next -> next = NULL;
	
	return result;
}

struct Array * addToArrayL(struct Array *arr,struct ExpressionList * list)
{
	struct Array * result = arr;
	struct Array * next = NULL;
	struct ArrayMember * member = NULL;
	
	while (arr-> next !=NULL)	
	{
		arr = arr->next;
	}
	
	while (list != NULL)
	{
		next = (struct Array *)malloc(sizeof(struct Array));		
		member = (struct ArrayMember *)malloc(sizeof(struct ArrayMember));

		arr -> next = next;
		
		member -> key = NULL;
		member -> value = list -> expr;
		
		next -> next = NULL;
		next -> value = member;

		list = list -> next;
		arr = next;
	}
	
	
	return result;
}

struct FunctionArg * createFunctionArg(char * id,int vartype,char * userobjectname)
{
	struct FunctionArg * stmt = (struct FunctionArg *)malloc(sizeof(struct FunctionArg));
	
	stmt -> id = id;
	stmt -> vartype = vartype;
	stmt -> userobjectname = userobjectname;	
	if (stmt -> userobjectname == NULL)
	{
		stmt -> userobjectname = (char *)malloc(sizeof(char));
		strcpy(stmt -> userobjectname,"");
	}


	return stmt;
}

struct FormalFunctionArglist * createFormalFunctionArglist(struct FunctionArg * arg)
{
	struct FormalFunctionArglist * stmt = (struct FormalFunctionArglist *)malloc(sizeof(struct FormalFunctionArglist));
	
	stmt -> arg = arg;
	stmt -> next = NULL;
	
	return stmt;
}

struct FormalFunctionArglist * addToFormalFunctionArglist(struct FormalFunctionArglist *list,struct FunctionArg * arg)
{	
	struct FormalFunctionArglist * result = list;
	struct FormalFunctionArglist * stmt = (struct FormalFunctionArglist *)malloc(sizeof(struct FormalFunctionArglist));
	
	while (list->next!=NULL)
	{
		list = list->next;
	}

	list -> next = stmt;

	stmt -> arg = arg;
	stmt -> next = NULL;
	
	return result;
}

struct Expression * createIntExpression(int value)
{
	struct Expression * stmt = (struct Expression *)malloc(sizeof(struct Expression));
	
	stmt -> type = _INTEGER;
	stmt -> value.i = value;
	stmt -> left = NULL;
	stmt -> right = NULL;
	stmt ->center = NULL;
	stmt->constIndex = -1;
	
	return stmt;
}

struct Expression * createDoubleExpression(double value)
{	
	struct Expression * stmt = (struct Expression *)malloc(sizeof(struct Expression));
	
	stmt -> type = _DOUBLE;
	stmt -> value.d = value;
	stmt -> left = NULL;
	stmt -> right = NULL;
	stmt ->center = NULL;
	stmt->constIndex = -1;

	return stmt;
}

struct Expression * createStringExpression(char * value)
{	
	struct Expression * stmt = (struct Expression *)malloc(sizeof(struct Expression));
	
	stmt -> type = _STRING;
	stmt -> value.s = value;
	stmt -> left = NULL;
	stmt -> right = NULL;
	stmt ->center = NULL;
	stmt->constIndex = -1;
	
	return stmt;
}

struct Expression * createFuncCallExpression(struct Expression * callexpr, struct ExpressionList * params)
{
	
	struct Expression * stmt = (struct Expression *)malloc(sizeof(struct Expression));
	
	struct FuncCall * funccall = (struct FuncCall *)malloc(sizeof(struct FuncCall));
	
	funccall -> callexpr = callexpr;
	funccall -> params = params;
	funccall ->paramsC = 0;
	
	stmt -> type = _FUNCTION;
	stmt -> value.funccall = funccall;
	stmt -> left = NULL;
	stmt -> right = NULL;
	stmt ->center = NULL;
	stmt->constIndex=-1;
	
	while (params)
	{
		funccall ->paramsC++;
		params = params->next;
	}
	

	return stmt;
}

struct Expression * createNewObjExpression(struct Expression * callexpr, struct ExpressionList * params)
{	
	struct Expression * stmt = (struct Expression *)malloc(sizeof(struct Expression));
	
	struct FuncCall * newobj = (struct FuncCall *)malloc(sizeof(struct FuncCall));
	
	newobj -> callexpr = callexpr;
	newobj -> params = params;
	newobj ->paramsC = 0;
	
	stmt -> type = _NEWOBJ;
	stmt -> value.newobj = newobj;
	stmt -> left = NULL;
	stmt -> right = NULL;
	stmt ->center = NULL;
	stmt->constIndex=-1;

	while (params)
	{
		newobj ->paramsC++;
		params = params->next;
	}
	
	return stmt;
}

struct Expression * createNewObjExpressionFromFuncCall(struct Expression * funccall)
{	
	if (funccall->type!=_FUNCTION)
	{
		return createNewObjExpression(funccall,NULL);
	}

	funccall -> type = _NEWOBJ;

	return funccall;
}

struct Expression * createArrayExpression(struct Array * value)
{
	struct Expression * stmt = (struct Expression *)malloc(sizeof(struct Expression));
	
	stmt->constIndex = -1;
	stmt -> type = _ARRAY;
	stmt -> value.arr = value;
	stmt -> left = NULL;
	stmt -> right = NULL;
	stmt ->center = NULL;
	
	return stmt;
}

struct Expression * createBooleanExpression(int value)
{
	struct Expression * stmt = (struct Expression *)malloc(sizeof(struct Expression));
	
	stmt -> type = _BOOLEAN;
	stmt -> value.i = value;
	stmt -> left = NULL;
	stmt -> right = NULL;
	stmt ->center = NULL;
	stmt->constIndex = -1;
	
	return stmt;
}

struct Expression * createIdExpression(char * id)
{
	struct Expression * expr=createStringExpression(id);

	expr->type = _ID;

	return expr;
}
