<?php
class TestVar
{
	public function text(string $text) : string
	{
		return $text;
	}
	
	public static function textStatic(string $text) : string
	{
		return $text;
	}
}

function printText(string $text)
{
	echo $text."\n";
}

$foo = 'foo';
$bar = 'bar';
$foobar = 'foobar';

echo (new TestVar())->text('foo');

echo $"bar";

$var = printText(TestVar::textStatic("text"));
$var = printText(${TestVar::textStatic("text")});
$var = printText($textobj->text("text"));
$var = printText(${$textobj->text("text")});
