#include "tables_generator.h"

void generateMainClass()
{
	ClassDescription * classDescr = new ClassDescription();	
	GlobalDescription::classes.insert(pair<string,ClassDescription*>("0Main",classDescr));

	// �������� ��������� code
	classDescr->addConstantUtf8("Code");

	// �������� ��� ������
	classDescr->name = classDescr->addConstantClass("phpprogram/0Main");

	// �������� ����� �������� 
	classDescr->parentname = classDescr->addConstantClass("java/lang/Object");

	// �������� �������� �� ����������� ��������
	classDescr->addConstantMethodref("java/lang/Object","<init>","()V");
		
	//������� �������� ������ main
	ClassMethodDescription * main = new ClassMethodDescription();
	classDescr->methods.insert(pair<string,ClassMethodDescription*>("main",main));
	main->name = classDescr->addConstantUtf8("main");
	main->descriptor = classDescr->addConstantUtf8("([Ljava/lang/String;)V");
	main->isStatic = true;
	main->returnType = _VOID;
	main->visibility = _PUBLIC;

	//�������� �����������
	// �������� �����������
	ClassMethodDescription * md = new ClassMethodDescription();
	classDescr->methods.insert(pair<string,ClassMethodDescription*>("<init>",md));
	md->name = classDescr->addConstantUtf8("<init>");
	md->descriptor = classDescr->addConstantUtf8("()V");
	md->isStatic = false;
	md->returnType = _VOID;
	md->visibility = _PUBLIC;

	md = new ClassMethodDescription();
	classDescr->methods.insert(pair<string,ClassMethodDescription*>("readLine",md));
	md->name = classDescr->addConstantUtf8("readLine");
	md->descriptor = classDescr->addConstantUtf8("()Lphprtl/stdClass;");
	md->isStatic = false;
	md->returnType = _VOID;
	md->visibility = _PUBLIC;


	// �������� ��� ��� readLine
	GlobalDescription::classes["0Main"]->addConstantUtf8("readLine");
	GlobalDescription::classes["0Main"]->addConstantUtf8("()Lphprtl/stdClass;");
	GlobalDescription::classes["0Main"]->addConstantClass("java/util/Scanner");
	GlobalDescription::classes["0Main"]->addConstantFieldref("java/lang/System","in","Ljava/io/InputStream;");
	GlobalDescription::classes["0Main"]->addConstantMethodref("java/util/Scanner","<init>","(Ljava/io/InputStream;)V");
	GlobalDescription::classes["0Main"]->addConstantMethodref("java/util/Scanner","nextLine","()Ljava/lang/String;");
	GlobalDescription::classes["0Main"]->addConstantMethodref("phprtl/stdClass","fromString","(Ljava/lang/String;)Lphprtl/stdClass;");
	
	// �������� ����� �������� � ������������� �������
	classDescr->names=classDescr->addConstantString("phpprogram/0Main");
	classDescr->parentnames=classDescr->addConstantString("java/lang/Object");

	// �������� �������� �� addStaticField
	classDescr->addConstantMethodref("phprtl/stdClass","addStaticField","(Ljava/lang/String;ILjava/lang/String;Lphprtl/stdClass;)V");
	
	// �������� �������� �� setStaticMethodVisibility
	classDescr->addConstantMethodref("phprtl/stdClass","setStaticMethodVisibility","(Ljava/lang/String;ILjava/lang/String;)V");

	// �������� �������� �� ������ �����
	classDescr->addConstantMethodref("java/io/PrintStream","print","(Ljava/lang/String;)V");

	// �������� ��� �������������� ������ �������� �����
	classDescr->addConstantMethodref("phprtl/stdClass","fromInt","(I)Lphprtl/stdClass;");
	classDescr->addConstantMethodref("phprtl/stdClass","fromFloat","(F)Lphprtl/stdClass;");
	classDescr->addConstantMethodref("phprtl/stdClass","fromBool","(Z)Lphprtl/stdClass;");
	classDescr->addConstantMethodref("phprtl/stdClass","fromString","(Ljava/lang/String;)Lphprtl/stdClass;");
	classDescr->addConstantMethodref("phprtl/stdClass","fromArray","(Lphprtl/Array;)Lphprtl/stdClass;");
	classDescr->addConstantMethodref("phprtl/stdClass","fromNull","()Lphprtl/stdClass;");

	classDescr->addConstantMethodref("phprtl/Variables","<init>","()V");

	classDescr->addConstantString("__construct");
	classDescr->addConstantString("phprtl/stdClass");


	classDescr->addConstantString("this");
}



void createClassDescription(ClassStatement * stmt)
{
	if (!strcmp(stmt->id,"parent") || !strcmp(stmt->id,"self") || !strcmp(stmt->id,"stdClass"))
		throw string("Cann't create class with name " + string(stmt->id));


	if (GlobalDescription::classes.count(stmt->id)>0)
		throw string("Class with name = '") + string(stmt->id) + string("' is already exist");

	ClassDescription * classDescr = new ClassDescription();	
	GlobalDescription::classes.insert(pair<string,ClassDescription*>(stmt->id,classDescr));

	// �������� ��������� code
	classDescr->addConstantUtf8("Code");

	string newClassname = "phpprogram/" + string(stmt->id);
	string newParentName = stmt->parentID;

	if (newParentName == "stdClass" || newParentName == "0bject")
		newParentName = "phprtl/" + newParentName;
	else
		newParentName = "phpprogram/" + newParentName;

	// �������� ��� ������
	classDescr->name = classDescr->addConstantClass(newClassname);

	// �������� ����� �������� 
	classDescr->parentname = classDescr->addConstantClass(newParentName);

	// �������� �����������
	ClassMethodDescription * md = new ClassMethodDescription();
	classDescr->methods.insert(pair<string,ClassMethodDescription*>("<init>",md));
	md->name = classDescr->addConstantUtf8("<init>");
	md->descriptor = classDescr->addConstantUtf8("()V");
	md->isStatic = false;
	md->returnType = _VOID;
	md->visibility = _PUBLIC;

	// �������� �������� �� ����������� ��������
	classDescr->addConstantMethodref(newParentName,"<init>","()V");

	// �������� �������� �� setMethodVisibility
	classDescr->addConstantMethodref("phprtl/stdClass","setMethodVisibility","(Ljava/lang/String;I)V");

	// �������� �������� �� addField
	classDescr->addConstantMethodref("phprtl/stdClass","addField","(Ljava/lang/String;ILjava/lang/String;Lphprtl/stdClass;)V");
	
	// �������� ����� �������� � ������������� �������
	classDescr->names = classDescr->addConstantString(newClassname);
	classDescr->parentnames =classDescr->addConstantString(newParentName);

	// �������� fieldRef �� className
	classDescr->addConstantFieldref(newClassname,"className","Ljava/lang/String;");

	// �������� ��� �������������� ������ �������� �����
	classDescr->addConstantMethodref("phprtl/stdClass","fromInt","(I)Lphprtl/stdClass;");
	classDescr->addConstantMethodref("phprtl/stdClass","fromFloat","(F)Lphprtl/stdClass;");
	classDescr->addConstantMethodref("phprtl/stdClass","fromBool","(Z)Lphprtl/stdClass;");
	classDescr->addConstantMethodref("phprtl/stdClass","fromString","(Ljava/lang/String;)Lphprtl/stdClass;");
	classDescr->addConstantMethodref("phprtl/stdClass","fromArray","(Lphprtl/Array;)Lphprtl/stdClass;");
	classDescr->addConstantMethodref("phprtl/stdClass","fromNull","()Lphprtl/stdClass;");

	classDescr->addConstantString("this");

	// �������� ������ � ��������
	ClassMemberList * members = stmt->members;
	while(members)
	{
		if (members->stmt->function!=NULL)
		{
			createClassMethodDescription(members->stmt->function,stmt->id);
		}
		else
		{
			createClassPropertyDescription(members->stmt->prt,stmt->id);
		}

		members = members->next;
	}
}

void addConstruct(char * classname)
{
	ClassMethodDescription * md = new ClassMethodDescription();
	md->visibility=_PUBLIC;
	md->returnType=_NONE;
	int index;
	if ((index = GlobalDescription::classes[classname]->indexOfUtf8Const("<init>"))>-1)
	{
		md->name = (ConstantUtf8*) GlobalDescription::classes[classname]->constants[index];
	}
	else
	{
		md->name = new ConstantUtf8("<init>");
		GlobalDescription::classes[classname]->addConstant(md->name);
	}
	md->isStatic=false;

	if ((index = GlobalDescription::classes[classname]->indexOfUtf8Const("()V"))>-1)
	{
		md->descriptor =  (ConstantUtf8*) GlobalDescription::classes[classname]->constants[index];
	}
	else
	{
		md->descriptor= new ConstantUtf8("()V");
		GlobalDescription::classes[classname]->addConstant(md->descriptor);
	}
	
	// �������� � �������
	GlobalDescription::classes[classname]->methods.insert(pair<string,ClassMethodDescription *>(md->name->value,md));
	
	//md->local_vars.push_back("<variables>");
}

void createClassMethodDescription(FunctionStatement * func, char * classname)
{
	if (GlobalDescription::classes[classname]->methods.count(func->id)>0)
		throw string("Method with name = '") + string(func->id) + string("' is already exist in class") + string(classname);
	
	ClassMethodDescription * md = new ClassMethodDescription();
	GlobalDescription::classes[classname]->methods.insert(pair<string,ClassMethodDescription*>(func->id,md));

	md->visibility=func->visibility;
	md->returnType=func->returncode;
	md->name = GlobalDescription::classes[classname]->addConstantUtf8(func->id);
	md->descriptor = GlobalDescription::classes[classname]->addConstantUtf8(createDescriptorString(func));
	md->isStatic = func->isStatic;
	md->code = func;
	
	GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","put","(Lphprtl/stdClass;Lphprtl/stdClass;)Lphprtl/stdClass;");
	GlobalDescription::classes[classname]->addConstantString(md->name->value);

	// �������� ����������
	FormalFunctionArglist * list = func->arglist;
	while (list)
	{
		md->local_vars.push_back(VarDescription(list->arg->id,list->arg->vartype,list->arg->userobjectname));
		list = list->next;
	}
}

void createClassPropertyDescription(ClassProperty * prt, char * classname)
{
	if (GlobalDescription::classes[classname]->properties.count(prt->id)>0)
		throw string("Property with name = '") + string(prt->id) + string("' is already exist in class") + string(classname);

	ClassPropertyDescription * prop = new ClassPropertyDescription();
	GlobalDescription::classes[classname]->properties.insert(pair<string,ClassPropertyDescription*>(prt->id,prop));
	GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","addField","(Ljava/lang/String;ILphprtl/stdClass;)V");
	prop->isStatic = prt->isStatic;
	prop->visibility = prt->visibility;
	prop->name = GlobalDescription::classes[classname]->addConstantString(prt->id);
	prop->value = prt->value;
}

string createDescriptorString(FunctionStatement * func)
{
	string result = "(";
	FormalFunctionArglist * list = func->arglist;
	
	while (list)
	{
		result+="Lphprtl/stdClass;";
		list = list->next;
	}

	result+=")Lphprtl/stdClass;";

	return result;
}


void generateObjectClass()
{
	//for (auto i = GlobalDescription::classes.cbegin(); i!= GlobalDescription::classes.cend();++i)
	//{// ��� ������� ������
	//	for (auto j = i->second->methods.cbegin(); j!=i->second->methods.cend(); ++j)
	//	{// ��� ������� ������
	//		if ()
	//	}
	//}
}
