#pragma once
#include "IConstant.h"
#include "ConstantUtf8.h"
#include <stdio.h>
#include <winsock2.h>
#include <stdint.h>

class ConstantString : public IConstant
{
public:
	ConstantString(ConstantUtf8 * value) : IConstant(_C_STRING) { this->value = value; }
	~ConstantString(){ if (value) delete value;  }
	ConstantUtf8 * value;
	virtual bool equal(IConstant * other){ return this->getType() == other->getType() && this->value->equal(((ConstantString*)other)->value);}
	virtual void print(){ printf("\t\t%d\tString\t%d\n",number,value->getNumber());}
	virtual string toCSV(){ return std::to_string((long long)number) + ";String;" + std::to_string((long long)value->getNumber()) + "\n";  }
	virtual void toBytecode(int descr)
	{
		unsigned char u1 = 8;
		write(descr,(void*)&u1,1);

		unsigned short u2 = htons(value->getNumber());
		write(descr,(void*)&u2,2);
	}
};