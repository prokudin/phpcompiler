#pragma once
#include "IConstant.h"
#include "ConstantUtf8.h"
#include <stdio.h>

class ConstantClass : public IConstant
{
public:
	ConstantClass(ConstantUtf8 * value) : IConstant(_C_CLASS) { this->value = value; }
	~ConstantClass(){ if (value) delete value; }
	ConstantUtf8 * value;
	virtual bool equal(IConstant * other){ return this->getType() == other->getType() && this->value->equal(((ConstantClass*)other)->value); }
	virtual void print(){ printf("\t\t%d\tClass\t%d\n",number,value->getNumber());}
	virtual string toCSV(){ return std::to_string((long long)number) + ";Class;" + std::to_string((long long)value->getNumber()) + "\n";  }
	virtual void toBytecode(int descr)
	{
		unsigned char u1 = 7;
		write(descr,(void*)&u1,1);

		unsigned short u2 = htons(value->getNumber());
		write(descr,(void*)&u2,2);
	}
};