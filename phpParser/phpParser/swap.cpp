
#include "swap.h"

char * swap(char * arr, int size)
{
	char tmp;
	for (int i=0;i<size/2;++i)
	{
		tmp = arr[i];
		arr[i] = arr[size-i-1];
		arr[size-i-1] = tmp;
	}

	return arr;
}


int swapS4(int val)
{
	s4 un;
	un.number = val;
	swap(un.bytes,4);
	return un.number;
}

unsigned int swapU4(unsigned int val)
{
	u4 un;
	un.number = val;
	swap(un.bytes,4);
	return un.number;
}

short swapS2(short val)
{
	s2 un;
	un.number = val;
	swap(un.bytes,2);
	return un.number;
}

unsigned short swapU2(unsigned short val)
{
	u2 un;
	un.number = val;
	swap(un.bytes,2);
	return un.number;
}