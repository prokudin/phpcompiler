#pragma once
#include "IConstant.h"
#include "ConstantUtf8.h"
#include <stdio.h>

class ConstantNameAndType : public IConstant
{
public:
	ConstantNameAndType(ConstantUtf8 * name,ConstantUtf8 * type) : IConstant(_C_NAME_AND_TYPE) { this->name = name; this->ctype = type;}
	~ConstantNameAndType(){  if (name) delete name;  if (ctype) delete ctype; }
	ConstantUtf8 * name;
	ConstantUtf8 * ctype;
	virtual bool equal(IConstant * other){ return this->getType() == other->getType() && this->name->equal(((ConstantNameAndType*)other)->name) && this->ctype->equal(((ConstantNameAndType*)other)->ctype); }
	virtual void print(){ printf("\t\t%d\tNaT\t%d,%d\n",number,name->getNumber(),ctype->getNumber());}
	virtual string toCSV(){ return std::to_string((long long)number) + ";NaT;" + std::to_string((long long)name->getNumber()) + " " + std::to_string((long long)ctype->getNumber()) + "\n";  }
	virtual void toBytecode(int descr)
	{
		unsigned char u1 = 12;
		write(descr,(void*)&u1,1);

		unsigned short u2 = htons(name->getNumber());
		write(descr,(void*)&u2,2);

		u2 = htons(ctype->getNumber());
		write(descr,(void*)&u2,2);
	}
};