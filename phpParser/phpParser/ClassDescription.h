#pragma once
#include "IConstant.h"
#include "ConstantClass.h"
#include "ConstantMethodref.h"
#include "ConstantFieldref.h"
#include "ConstantString.h"
#include "ConstantFloat.h"
#include "ConstantInteger.h"
#include "ClassPropertyDescription.h"
#include "ClassMethodDescription.h"
#include <iostream> 
#include <vector>
#include <map>

using namespace std;

class ClassDescription
{
	int constID;
public:
	ClassDescription(){constID=0; }

	ConstantClass * name;
	ConstantClass * parentname;

	ConstantString * names;
	ConstantString * parentnames;

	map<string,ClassPropertyDescription*> properties;
	map<string,ClassMethodDescription*> methods;
	vector<IConstant*> constants;

	int addConstant(IConstant * constant){ constant->setNumber(++constID); constants.push_back(constant); return constID;}
	int addConstantWithoutReps(IConstant * constant);
	
	ConstantUtf8 * addConstantUtf8(string value);
	ConstantClass * addConstantClass(string value);
	ConstantString * addConstantString(string value);
	ConstantFloat * addConstantFloat(float value);
	ConstantInteger * addConstantInteger(int value);
	ConstantNameAndType * addConstantNameAndType(string name,string type);
	ConstantFieldref * addConstantFieldref(string classname,string name,string type);
	ConstantMethodref * addConstantMethodref(string classname,string name,string type);
	
	int indexOfConst(IConstant * constant);
	int indexOfUtf8Const(string value);
	void print();
	void generateConstTable();
};

