#pragma once
#include "stdafx.h"
#include <string.h>
#include <stdlib.h>
#include <string>
#include "generate_code.h"
#include "GlobalDescriprion.h"
//#include "tables_generator.h"
#include "tree_mod.h"

extern "C" FILE * yyin;
//extern void treeModification(struct Program * prg);
extern "C" int yyparse(void);
extern "C" void printProgram(struct Program * prg);
extern "C" struct Program * _yyprg;
extern void generateTables(struct Program * prg);

int main(int argc, char * argv[])
{
	++argv, --argc; 
	yyin = fopen( argv[0], "r" );
    yyparse();
	try
	{
		treeModification(_yyprg);
		//printProgram(_yyprg);
		//GlobalDescription::toCSV();
		//GlobalDescription::print();
		generateBytecode();
		//system("draw.bat");
	} 
	catch (std::string & ex)
	{	
		printf ("\nError : %s",ex.c_str());
	}
	
	return 0;
}

