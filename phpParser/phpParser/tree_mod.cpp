#include "tree_mod.h"

// class - parent
map<string,string> classes;


void treeModification(struct Program * prg)
{
	struct StatementsList * stmts = NULL,* current = NULL; 
	struct HtmlText * htmltext = prg->stmts, * tmp = NULL;
		
	classes.clear();
	//classes.insert(pair<string,string>("0bject","stdClass"));

	// ������� ������� stmt_list
	while(htmltext)
	{
		
		if (htmltext->html!=NULL)
		{			
			// ������� echo stmt
			if (stmts)
			{
				addStatementToList(stmts,createStatement(_ECHO,(void*)createStringExpression(htmltext->html)));			
				current = current->next;
			}
			else
			{
				current = stmts = createStatementsList(createStatement(_ECHO,(void*)createStringExpression(htmltext->html)));
			}
		}
		else
		{
			current ->next = htmltext->stmts ;
			while (current->next)
				current = current->next;						
		}

		tmp = htmltext;
		htmltext = htmltext->next;
		free(tmp);
	} 

	tmp = new HtmlText();
	tmp->html = NULL;
	tmp->next = NULL;
	tmp->stmts = stmts;
	prg->stmts = tmp;

	// ������������� ����� Main
	generateMainClass();

	createClassTables(stmts);

	htmltext = prg->stmts;	

	// ���������� ����� � ����������� ������(� �������)
	tmp->stmts = stmts = statementsListModification(stmts,"0Main");

	// �������� ����������� ������ � ����������� �������� � main
	for (auto i = GlobalDescription::classes.cbegin(); i != GlobalDescription::classes.cend();++i)
	{
		for (auto j = i->second->methods.cbegin(); j!=i->second->methods.cend();++j )		
		{
			GlobalDescription::classes["0Main"]->addConstantString(j->first);
		}

		for (auto j = i->second->properties.cbegin(); j!=i->second->properties.cend();++j )		
		{
			GlobalDescription::classes["0Main"]->addConstantString(j->first);
			exprModification(j->second->value,"0Main");
		}

		if (i->first!="0Main")
			GlobalDescription::classes["0Main"]->addConstantString(i->second->name->value->value);
	}


	// �� ��� ������ �������� ��� ������ __construct ������ �������
	for (auto i = GlobalDescription::classes.cbegin(); i != GlobalDescription::classes.cend();++i)
	{
		if (i->second->methods.count("__construct") == 0)
			i->second->addConstantString("__construct");
	}

	checkMethods();
}

void createClassTables(struct StatementsList * stmts)
{
	while(stmts)
	{
		switch(stmts->stmt->code)
		{
			case _CLASS:
				changeClassNameAndAddParent(stmts->stmt->stmt.classStmt);
				if (!addConstructDestruct(stmts->stmt->stmt.classStmt))
					addParentCallFromonstructors(stmts->stmt->stmt.classStmt);
				
				// ������� �������
				classes.insert(pair<string,string>(stmts->stmt->stmt.classStmt->id,stmts->stmt->stmt.classStmt->parentID));
				createClassDescription(stmts->stmt->stmt.classStmt);				
				break;
			case _FUNCTION:
				createClassMethodDescription(stmts->stmt->stmt.funcStmt,"0Main");
				break;
		}

		stmts=stmts->next;
	}
	
	// ��������� �� ������� ���� �������
	for (auto i = GlobalDescription::classes.cbegin(); i != GlobalDescription::classes.cend();++i)
	{
		if (i->first == "0Main" || classes[i->first]=="0bject" || classes[i->first]=="stdClass")
			continue;

		if (classes[classes[i->first]]=="")
			throw string("Class " + classes[i->first] + " not found");
	}
}

void addParentCallFromonstructors(struct ClassStatement * stmt)
{
	struct ClassMemberList * iter = stmt->members;
	FunctionStatement * func;
	Statement * s;
	Expression * expr;
	while(iter)
	{
		if (iter->stmt->function!=NULL && !strcmp("__construct",iter->stmt->function->id) && strcmp("0bject",stmt->parentID) && strcmp("stdClass",stmt->parentID))
		{// ���� ������ �����������
			func = iter->stmt->function;

			s = new Statement();
			s->code = _EXPR;
			s->stmt.exprStmt = mergeAssignAndSqBracket(createExpression(_DCOLON,createIdExpression("parent"),createFuncCallExpression(createIdExpression("__construct"),NULL)));

			if (func->stmts == NULL)
				func->stmts = createStatementsList(s);
			else
				func->stmts = addStatementToList(func->stmts,s);
		}

		iter = iter->next;
	}
}

void movePropertyDefinitionsToConstruct(struct ClassStatement * stmt)
{
	struct ClassMemberList * iter = stmt->members;

	map<char*,Expression*> initialized;
	list<int> isStatic;

	while(iter)
	{
		if (iter->stmt->prt!=NULL && !iter->stmt->prt->isStatic)
		{			
			initialized.insert(pair<char*,Expression*>(iter->stmt->prt->id,iter->stmt->prt->value));
			isStatic.push_back(iter->stmt->prt->isStatic);
		}

		iter = iter->next;
	}

	iter = stmt->members;


	while(iter)
	{
		if (iter->stmt->function!=NULL && !strncmp("__construct",iter->stmt->function->id,11))
		{	
			auto isStaticIter = isStatic.cbegin();
			for (auto i = initialized.begin(); i!= initialized.cend() ; ++i,++isStaticIter)
			{
				StatementsList * list = new StatementsList();
				Expression * expr;
				expr = createExpression(_ASSIGN,createExpression(_ARROW,createExpression(_DOLLAR,NULL,createIdExpression("this")),createIdExpression(i->first)),i->second);
				
				list->stmt = createStatement(_EXPR,(void*)expr);
				list->next = iter->stmt->function->stmts;
				iter->stmt->function->stmts = list;
			}
		}

		iter = iter->next;
	}

}

StatementsList * statementsListModification(struct StatementsList * stmts,char * classname)
{
	stmts=forReplacement(stmts);
	StatementsList * mod=stmts;
	
	while(stmts)
	{
		switch(stmts->stmt->code)
		{
		case _CLASS:
			classModification(stmts->stmt->stmt.classStmt);
			break;
		case _IF:
			exprModification(stmts->stmt->stmt.ifStmt->expr,classname);
			stmts->stmt->stmt.ifStmt->thenlist=statementsListModification(stmts->stmt->stmt.ifStmt->thenlist,classname);
			stmts->stmt->stmt.ifStmt->elselist=statementsListModification(stmts->stmt->stmt.ifStmt->elselist,classname);
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToBoolean","()Z");
			break;
		case _WHILEDO:
		case _DOWHILE:
			exprModification(stmts->stmt->stmt.whiledoStmt->expr,classname);
			stmts->stmt->stmt.whiledoStmt->stmts=statementsListModification(stmts->stmt->stmt.whiledoStmt->stmts,classname);
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToBoolean","()Z");
			break;
		case _FOREACH:
			exprModification(stmts->stmt->stmt.foreachStmt->arr,classname);
			exprModification(stmts->stmt->stmt.foreachStmt->key,classname);
			exprModification(stmts->stmt->stmt.foreachStmt->value,classname);
			stmts->stmt->stmt.foreachStmt->stmts=statementsListModification(stmts->stmt->stmt.foreachStmt->stmts,classname);
			// ������ � ��������� � �����������
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToArray","()Lphprtl/Array;");
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Array","iterator","()Lphprtl/ArrayIterator;");
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/ArrayIterator","hasNext","()Z");
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/ArrayIterator","next","()V");
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/ArrayIterator","key","()Lphprtl/stdClass;");			
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/ArrayIterator","value","()Lphprtl/stdClass;");	
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","put","(Lphprtl/stdClass;Lphprtl/stdClass;)Lphprtl/stdClass;");
			GlobalDescription::classes[classname]->addConstantString("<iter>");
			break;
		case _ECHO:
			exprModification(stmts->stmt->stmt.exprStmt,classname);
			GlobalDescription::classes[classname]->addConstantMethodref("java/io/PrintStream","print","(Ljava/lang/String;)V");
			GlobalDescription::classes[classname]->addConstantFieldref("java/lang/System","out","Ljava/io/PrintStream;");
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToString","()Ljava/lang/String;");
			break;
		case _EXPR:
		case _RETURN:
			exprModification(stmts->stmt->stmt.exprStmt,classname);
			break;
		case _FUNCTION:
			addReturnStatement(stmts->stmt->stmt.funcStmt);
			stmts->stmt->stmt.funcStmt->stmts=statementsListModification(stmts->stmt->stmt.funcStmt->stmts,classname);
			// �������� ���������� ����� � ������������
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","myClone","()Lphprtl/stdClass;");
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","toInt","()Lphprtl/stdClass;");
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","toStr","()Lphprtl/stdClass;");
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","toFloat","()Lphprtl/stdClass;");
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","toArray","()Lphprtl/stdClass;");
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","toBool","()Lphprtl/stdClass;");
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertTo","(Ljava/lang/String;)Lphprtl/stdClass;");
			// �������� ����� ���������� � �� ���� � ������� ��������
			addFunctionArgsToConstantTabe(stmts->stmt->stmt.funcStmt->arglist,classname);

			// �������� ����� ��� �� ����������� Variables
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","<init>","()V");
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","put","(Ljava/lang/String;Lphprtl/stdClass;)Lphprtl/stdClass;");
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","fromString","(Ljava/lang/String;)Lphprtl/stdClass;");

			break;
		case _STMTLIST:
			stmts->stmt->stmt.stmtlist=statementsListModification(stmts->stmt->stmt.stmtlist,classname);
			break;
		}
		
		stmts=stmts->next;	
	}

	return mod;
}



void addFunctionArgsToConstantTabe(FormalFunctionArglist * args, char * classname)
{
	while (args)
	{
		GlobalDescription::classes[classname]->addConstantString(args->arg->id);

		if (args->arg->vartype == _USEROBJECT)
			GlobalDescription::classes[classname]->addConstantString(args->arg->userobjectname);
			
		args = args->next;
	}
}

void addReturnStatement(FunctionStatement * func)
{
	StatementsList * list = func->stmts;

	while (list && list->next)
	{
		list = list->next;
	}

	if (list && list->stmt->code != _RETURN)
	{	
		StatementsList * newlist = new StatementsList();
		newlist->next = NULL;	
		newlist->stmt=createStatement(_RETURN,(void*)createExpression(_NIL,NULL,NULL));		
		list->next = newlist;
	}
	else if (!list)
	{
		StatementsList * newlist = new StatementsList();
		newlist->next = NULL;	
		newlist->stmt=createStatement(_RETURN,(void*)createExpression(_NIL,NULL,NULL));		
		func->stmts = newlist;
	}
}

void changeClassNameAndAddParent(struct ClassStatement * stmt)
{
	// �������� ��� �������� ������
	char * buffer;
	/*buffer = new char[strlen(stmt->id) + 12];

	strcpy(buffer,"phpprogram/");
	strcat(buffer,stmt->id);

	delete[] stmt->id;
	stmt->id = buffer;*/

	// �������� ��� ������
	if (stmt->parentID == NULL)
	{
		buffer= new char[14];
		strcpy(buffer,"0bject");		

		delete[] stmt->parentID;
		stmt->parentID = buffer;
	}
	else
	{
		/*buffer= new char[strlen(stmt->id) + 12];
		strcpy(buffer,"phpprogram/");
		strcat(buffer,stmt->parentID);

		delete[] stmt->parentID;
		stmt->parentID = buffer;*/
	}

}

void classModification(struct ClassStatement * stmt)
{
	struct ClassMemberList * members = stmt->members;
	
	while(members)
	{
		if (members->stmt->function!=NULL)
		{
			addReturnStatement(members->stmt->function);
			members->stmt->function->stmts=statementsListModification(members->stmt->function->stmts,stmt->id);

			// �������� ���������� ����� � ������������
			GlobalDescription::classes[stmt->id]->addConstantMethodref("phprtl/stdClass","myClone","()Lphprtl/stdClass;");
			GlobalDescription::classes[stmt->id]->addConstantMethodref("phprtl/stdClass","toInt","()Lphprtl/stdClass;");
			GlobalDescription::classes[stmt->id]->addConstantMethodref("phprtl/stdClass","toStr","()Lphprtl/stdClass;");
			GlobalDescription::classes[stmt->id]->addConstantMethodref("phprtl/stdClass","toFloat","()Lphprtl/stdClass;");
			GlobalDescription::classes[stmt->id]->addConstantMethodref("phprtl/stdClass","toArray","()Lphprtl/stdClass;");
			GlobalDescription::classes[stmt->id]->addConstantMethodref("phprtl/stdClass","toBool","()Lphprtl/stdClass;");
			GlobalDescription::classes[stmt->id]->addConstantMethodref("phprtl/stdClass","convertTo","(Ljava/lang/String;)Lphprtl/stdClass;");
			// �������� ����� ���������� � �� ���� � ������� ��������
			addFunctionArgsToConstantTabe(members->stmt->function->arglist,stmt->id);

			// �������� ����� ��� �� ����������� Variables
			GlobalDescription::classes[stmt->id]->addConstantMethodref("phprtl/Variables","<init>","()V");
			GlobalDescription::classes[stmt->id]->addConstantMethodref("phprtl/Variables","put","(Ljava/lang/String;Lphprtl/stdClass;)Lphprtl/stdClass;");
			GlobalDescription::classes[stmt->id]->addConstantMethodref("phprtl/stdClass","fromString","(Ljava/lang/String;)Lphprtl/stdClass;");
		}
		else
		{
			exprModification(members->stmt->prt->value,stmt->id);
		}

		members = members->next;
	}
}

char * createConstructFunctionName(struct ClassStatement * stmt, struct FunctionStatement * constr)
{
	int size = 0;
	struct FormalFunctionArglist * arglist = constr->arglist;
	char number[5];
	char * result;

	while(arglist)
	{
		size++;
		arglist = arglist->next;
	}

	result = (char *)malloc(sizeof(char)*(11 +6));
	strcpy(result,constr->id);
	strcat(result,itoa(size,number,10));

	return result;
}

int containsParantConstuctCall(struct FunctionStatement * constr)
{
	return 0;
}

bool addConstructDestruct(struct ClassStatement * stmt)
{
	int constructFound = 0;
	int destructFound = 0;
	struct ClassMemberList * iter = stmt->members;
	struct FunctionStatement * func = NULL;

	while(iter)
	{
		if (iter->stmt->function!=NULL)
		{
			if ((!strcmp("__construct",iter->stmt->function->id)))
			{
				if (constructFound)
					throw std::string("Multiple definition of constructors");

				//iter->stmt->function->id = createConstructFunctionName(stmt,iter->stmt->function);

				constructFound = 1;											
			}
			else if (!strcmp("__destruct",iter->stmt->function->id))
			{
				if (destructFound)
					throw "Multiple definition of destructors";
				else if (iter->stmt->function->arglist!=NULL)
					throw "Destruct mustn't have input args";

				destructFound = 1;
			}
		}

		iter = iter->next;
	}

	if (!constructFound)
	{
		func = createFunctionStatement("__construct",1,0,_PUBLIC,NULL,_NONE,NULL,NULL);	

		//func->id = createConstructFunctionName(stmt,func);

		if (stmt->members)
		{
			stmt->members = addToClassMemberList(stmt->members,createClassFunctionStatement(func,0,_PUBLIC));
		}
		else
		{
			stmt->members = createClassMemberList(createClassFunctionStatement(func,0,_PUBLIC));
		}
	}


	/*if (!destructFound)
	{
		func = createFunctionStatement("__destruct",1,0,_PUBLIC,NULL,_NONE,NULL,NULL);
		if (stmt->members)
		{
			stmt->members = addToClassMemberList(stmt->members,createClassFunctionStatement(func,0,_PUBLIC));
		}
		else
		{
			stmt->members = createClassMemberList(createClassFunctionStatement(func,0,_PUBLIC));
		}
	}*/

	return constructFound;
}

void exprListModification(struct ExpressionList * list,char * classname)
{
	while(list)
	{
		exprModification(list->expr,classname);
		list = list->next;
	}
}

bool isParent(string parent , string clazz)
{
	list<string> classes;
	classes.push_back(clazz);

	while (classes.back()!="0bject" && classes.back()!="stdClass")
	{
		string par = GlobalDescription::classes[clazz]->parentname->value->value;
		if (par == parent)
			return true;
		classes.push_back(par);
	}
	return false;
}


void exprModification(struct Expression * expr,char * classname)
{
	if (!expr)
		return;

	static bool checkFCall=true;

	exprModification(expr->left,classname);

	if (expr->type == _ARROW_ASSIGN || expr->type == _DCOLON_ASSIGN)
		checkFCall = false;
	exprModification(expr->center,classname);
	if (expr->type == _ARROW_ASSIGN || expr->type == _DCOLON_ASSIGN)
		checkFCall = true;
	
	if (expr->type == _ARROW || expr->type == _DCOLON)
		checkFCall = false;	
	exprModification(expr->right,classname);
	if (expr->type == _ARROW || expr->type == _DCOLON)
		checkFCall = true;

	mergeAssignAndSqBracket(expr);

	int index;
	static char number[10];
	int index2;
	IConstant * iconst;
	IConstant * iconst2;
	ExpressionList * elist;
	Array * arr;
	switch (expr->type)
	{
	case _ID:
	case _STRING:	
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","fromString","(Ljava/lang/String;)Lphprtl/stdClass;")->getNumber();
		expr->constIndex = GlobalDescription::classes[classname]->addConstantString(expr->value.id)->getNumber();
		break;
	case _DOLLAR:		
		// �������� ���������� � ������ Vatiable
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","dollar","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();	
		break;
	case _DOLLAR_ASSIGN:		
		// �������� ���������� � ������ Vatiable
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","put","(Lphprtl/stdClass;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();	
		break;	
	case _NIL:		
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","fromNull","()Lphprtl/stdClass;")->getNumber();
		break;
	case _ARRAY_PUSHBACK:
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToArray","()Lphprtl/Array;");
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Array","put","(Lphprtl/stdClass;)Lphprtl/stdClass;");
		break;
	case _ARRAY:
		 GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","fromArray","(Lphprtl/Array;)Lphprtl/stdClass;")->getNumber();
		 GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Array","<init>","()V")->getNumber();
		 GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Array","put","(Lphprtl/stdClass;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		 GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Array","put","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		 arr = expr->value.arr;
		 while (arr)
		 {
			 exprModification(arr->value->key,classname);
			 exprModification(arr->value->value,classname);
			 arr = arr->next;
		 }
		break;
	case _BOOLEAN:
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","fromBool","(Z)Lphprtl/stdClass;")->getNumber();
		break;
	case _INTEGER:
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","fromInt","(I)Lphprtl/stdClass;")->getNumber();
		if (expr->value.i>=-1 && expr->value.i<=5)
			break;
		expr->constIndex = GlobalDescription::classes[classname]->addConstantInteger(expr->value.i)->getNumber();		
		break;
	case _DOUBLE:	
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","fromFloat","(F)Lphprtl/stdClass;")->getNumber();
		expr->constIndex = GlobalDescription::classes[classname]->addConstantFloat((float)(expr->value.d))->getNumber();		
		break;
	case _DCOLON:
		if ((expr->left->type== _ID || expr->left->type==_STRING))
		{// ���� ����� id
			if (!strcmp(expr->left->value.id,"self"))
			{// ���� ����� self
				if (!strcmp(classname,"0Main"))
					throw string("Using 'self' inside global code section");


				if (expr->right->type==_FUNCTION 
					&& (expr->right->value.funccall->callexpr->type == _ID || expr->right->value.funccall->callexpr->type == _STRING))
				{// ���� ������ �����
					auto method = containsMethod(classname,expr->right->value.funccall->callexpr->value.id);
					if (method == NULL)
					{
						throw string("Method " + string(expr->right->value.funccall->callexpr->value.id) + " wasn't found in class " + classname);
					}
					else
					{
						if (!method->isStatic)
							throw string("Try to call nonstatic method " + string(expr->right->value.funccall->callexpr->value.id));

						if (method->visibility == _PRIVATE && GlobalDescription::classes[classname]->methods.count(expr->right->value.funccall->callexpr->value.id)==0)
							throw string("Cann't call private method " + string(expr->right->value.funccall->callexpr->value.id));
					}
				}
				else if (expr->right->type==_ID || expr->right->type==_STRING)
				{
					auto prop = containsProperty(classname,expr->right->value.id);
					if (prop == NULL)
					{
						throw string("Property " + string(expr->right->value.id) + " doesn't found in class " + classname);
					}
					else
					{
						if (!prop->isStatic)
							throw string("Try to get nonstatic field " + string(expr->right->value.id));

						if (prop->visibility == _PRIVATE && GlobalDescription::classes[classname]->properties.count(expr->right->value.id)==0)
							throw string("Cann't get private property " + string(expr->right->value.id) + " in class" + classname);
					}
				}
			}
			else if (!strcmp(expr->left->value.id,"parent"))
			{// ���� ����� parent
				if (!strcmp(classname,"0Main"))
					throw string("Using 'parent' inside global code section");

				if (expr->right->type==_FUNCTION 
					&& (expr->right->value.funccall->callexpr->type == _ID || expr->right->value.funccall->callexpr->type == _STRING))
				{// ���� ������ �����
					auto method = containsMethod(classes[classname],expr->right->value.funccall->callexpr->value.id);
					if (method == NULL)
					{
						throw string("Parent method " + string(expr->right->value.funccall->callexpr->value.id) + " doesn't found in class " + classes[classname]);
					}
					else
					{
						if (method->visibility == _PRIVATE)
							throw string("Cann't call private method " + string(expr->right->value.funccall->callexpr->value.id) + " in class" + classes[classname]);
					}
				}
				else if (expr->right->type==_ID || expr->right->type==_STRING)
				{
					auto prop = containsProperty(classes[classname],expr->right->value.id);
					if (prop == NULL)
					{
						throw string("Parent property " + string(expr->right->value.id) + " doesn't found in class " + classes[classname]);
					}
					else
					{
						if (!prop->isStatic)
							throw string("Try get nonstatic field " + string(expr->right->value.id));

						if (prop->visibility == _PRIVATE)
							throw string("Cann't get private property " + string(expr->right->value.id) + " in class" + classes[classname]);
					}
				}
			}			
			else			
			{// ���� ����� id
				string clname = expr->left->value.id;

				if (expr->right->type==_FUNCTION 
					&& (expr->right->value.funccall->callexpr->type == _ID || expr->right->value.funccall->callexpr->type == _STRING))
				{// ���� ������ �����
					auto method = containsMethod(clname,expr->right->value.funccall->callexpr->value.id);
					if (method == NULL)
					{
						throw string("Method " + string(expr->right->value.funccall->callexpr->value.id) + " doesn't found in class " + clname);
					}
					else
					{
						if (!method->isStatic)
							throw string("Try to call nonstatic method " + string(expr->right->value.funccall->callexpr->value.id));

						if (method->visibility == _PRIVATE && GlobalDescription::classes[classname]->methods.count(expr->right->value.funccall->callexpr->value.id)==0
							|| method->visibility == _PROTECTED && !isChildClass(clname,classname))
							throw string("Cann't call private method " + string(expr->right->value.funccall->callexpr->value.id)) + " of class " + clname;
					}
				}
				else if (expr->right->type==_ID || expr->right->type==_STRING)
				{
					auto prop = containsProperty(clname,expr->right->value.id);
					if (prop == NULL)
					{
						throw string("Property " + string(expr->right->value.id) + " haven't been found in class " + clname);
					}
					else
					{
						if (!prop->isStatic)
							throw string("Try get nonstatic field " + string(expr->right->value.id));

						if (prop->visibility == _PRIVATE && GlobalDescription::classes[classname]->properties.count(expr->right->value.id)==0
							|| prop->visibility == _PROTECTED && !isChildClass(clname,classname))
							throw string("Cann't get private property " + string(expr->right->value.id) + " in class" + clname);
					}
				}
			}
		}
		else
		{
			if (expr->right->type==_FUNCTION 
				&& (expr->right->value.funccall->callexpr->type == _ID || expr->right->value.funccall->callexpr->type == _STRING))
			{// ���� ������ �����
				auto method = containsMethodGlobal(expr->right->value.funccall->callexpr->value.id);
				if (method == NULL)
				{
					throw string("Method " + string(expr->right->value.funccall->callexpr->value.id) + " doesn't found ");
				}
			}
			else if (expr->right->type==_ID || expr->right->type==_STRING)
			{
				auto prop = containsPropertyGlobal(expr->right->value.id);
				if (prop == NULL)
				{
					throw string("Property " + string(expr->right->value.id) + " doesn't found");
				}
			}
		}

		if (expr->right->type == _FUNCTION)
		{
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","staticMethodCall","(Ljava/lang/String;[Lphprtl/stdClass;Ljava/lang/String;Ljava/lang/String;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToString","()Ljava/lang/String;");
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","dollar","(Lphprtl/stdClass;)Lphprtl/stdClass;");
		} 
		else
		{
			GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToString","()Ljava/lang/String;");
			expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","getStaticField","(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lphprtl/stdClass;")->getNumber();
		}
		break;

	case _DCOLON_ASSIGN:
		// ��������� �� ������
		if (expr->center->type == _FUNCTION)
		{
			throw string("You cann't assign value to function");
		}

		if ((expr->left->type== _ID || expr->left->type==_STRING))
		{// ���� ����� id
			if (!strcmp(expr->left->value.id,"self"))
			{// ���� ����� self
				if (!strcmp(classname,"0Main"))
					throw string("Using 'self' inside global code section");

				if (expr->center->type==_ID || expr->center->type==_STRING)
				{
					auto prop = containsProperty(classname,expr->center->value.id);
					if (prop == NULL)
					{
						throw string("Property " + string(expr->center->value.id) + " doesn't found in class " + classname);
					}
					else
					{
						if (!prop->isStatic)
							throw string("Try to set nonstatic field " + string(expr->center->value.id));

						if (prop->visibility == _PRIVATE && GlobalDescription::classes[classname]->properties.count(expr->center->value.id)==0)
							throw string("Cann't set private property " + string(expr->center->value.id) + " in class" + classname);
					}
				}
			}
			else if (!strcmp(expr->left->value.id,"parent"))
			{// ���� ����� parent
				if (!strcmp(classname,"0Main"))
					throw string("Using 'parent' inside global code section");

				if (expr->center->type==_ID || expr->center->type==_STRING)
				{
					auto prop = containsProperty(classes[classname],expr->center->value.id);
					if (prop == NULL)
					{
						throw string("Parent property " + string(expr->center->value.id) + " doesn't found in class " + classes[classname]);
					}
					else
					{
						if (!prop->isStatic)
							throw string("Try set nonstatic field " + string(expr->center->value.id));

						if (prop->visibility == _PRIVATE)
							throw string("Cann't set private property " + string(expr->center->value.id) + " in class" + classes[classname]);
					}
				}
			}			
			else			
			{// ���� ����� id
				string clname = expr->left->value.id;

				if (expr->center->type==_ID || expr->center->type==_STRING)
				{
					auto prop = containsProperty(clname,expr->center->value.id);
					if (prop == NULL)
					{
						throw string("Property " + string(expr->center->value.id) + " haven't been found in class " + clname);
					}
					else
					{
						if (!prop->isStatic)
							throw string("Try set nonstatic field " + string(expr->center->value.id));

						if (prop->visibility == _PRIVATE && GlobalDescription::classes[classname]->properties.count(expr->center->value.id)==0
							|| prop->visibility == _PROTECTED && !isChildClass(clname,classname))
							throw string("Cann't set private property " + string(expr->center->value.id) + " in class" + clname);
					}
				}
			}
		}
		else
		{
			if (expr->center->type==_ID || expr->center->type==_STRING)
			{
				auto prop = containsPropertyGlobal(expr->center->value.id);
				if (prop == NULL)
				{
					throw string("Property " + string(expr->center->value.id) + " doesn't found");
				}
			}
		}


		
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","setStaticField","(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToString","()Ljava/lang/String;");
		break;

	case _ARROW:
		// ��������� �� ������_ID || expr->left->type == _STRING
		if ((expr->left->type == _DOLLAR)
			&& (expr->left->right->type== _ID || expr->left->right->type == _STRING)
			&& !strcmp(expr->left->right->value.id,"this")
			)
		{// ���� ����� $this
			if (expr->right->type==_FUNCTION && (expr->right->value.funccall->callexpr->type == _ID || expr->right->value.funccall->callexpr->type == _STRING))
			{// ���� ���� �����
				auto method = containsMethod(classname,expr->right->value.funccall->callexpr->value.id);
				if (method == NULL)
				{
					throw string("Method " + string(expr->right->value.funccall->callexpr->value.id) + " doesn't found in class " + classname);
				}
				else
				{
					if (method->visibility == _PRIVATE && GlobalDescription::classes[classname]->methods.count(expr->right->value.funccall->callexpr->value.id)==0)
						throw string("Cann't call private method " + string(expr->right->value.funccall->callexpr->value.id));
				}
			}
			else if ((expr->right->type==_ID || expr->right->type==_STRING) && !isChildClass(classname,"stdClass"))
			{
				auto prop = containsProperty(classname,expr->right->value.id);
				if (prop == NULL)
				{
					throw string("Property " + string(expr->right->value.id) + " wasn't found in class " + classname);
				}
				else
				{
					if (prop->visibility == _PRIVATE && GlobalDescription::classes[classname]->properties.count(expr->right->value.id)==0)
						throw string("Cann't get private property " + string(expr->right->value.id) + " in class" + classname);
				}
			}
		}
		else 
		{
			if (expr->right->type==_FUNCTION && (expr->right->value.funccall->callexpr->type == _ID || expr->right->value.funccall->callexpr->type == _STRING))
			{// ���� ���� �����
				auto method = containsMethodGlobal(expr->right->value.funccall->callexpr->value.id);
				if (method == NULL)
				{
					throw string("Method " + string(expr->right->value.funccall->callexpr->value.id) + " doesn't found");
				}
			}
		}

		// �������� ����� ������
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToString","()Ljava/lang/String;");
		if (expr->right->type==_FUNCTION)
		{
			expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","methodCall","(Ljava/lang/String;[Lphprtl/stdClass;Ljava/lang/String;)Lphprtl/stdClass;")->getNumber();
		}
		else
		{
			expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","getField","(Ljava/lang/String;Ljava/lang/String;)Lphprtl/stdClass;")->getNumber();
		}
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToString","()Ljava/lang/String;");
		break;
	case _ARROW_ASSIGN:
		
		// ��������� �� ������
		if (expr->right->type==_FUNCTION)
		{
			throw string("You cann't asiggn value to method");
		}

		// ��������� �� ������_ID || expr->left->type == _STRING
		if ((expr->left->type == _DOLLAR)
			&& (expr->left->right->type== _ID || expr->left->right->type == _STRING)
			&& !strcmp(expr->left->right->value.id,"this")
			)
		{// ���� ����� $this
			if ((expr->center->type==_ID || expr->center->type==_STRING) && !isChildClass(classname,"stdClass"))
			{
				auto prop = containsProperty(classname,expr->center->value.id);
				if (prop == NULL)
				{
					throw string("Property " + string(expr->center->value.id) + " doesn't found in class " + classname);
				}
				else
				{
					if (prop->visibility == _PRIVATE && GlobalDescription::classes[classname]->properties.count(expr->center->value.id)==0)
						throw string("Cann't get private property " + string(expr->center->value.id) + " in class" + classname);
				}
			}
		}

		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToString","()Ljava/lang/String;");
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","setField","(Ljava/lang/String;Ljava/lang/String;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		
		break;
	case _SQ_BRACKET:
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToArray","()Lphprtl/Array;");
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Array","get","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		
		break;

	case _SQ_BRACKET_ASSIGN:
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToArray","()Lphprtl/Array;");
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Array","put","(Lphprtl/stdClass;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		break;

	case _FUNCTION:
		///�������� ������� ������������ ������ main
		// ���������, ���� �� ����� �����

		if (checkFCall && (expr->value.funccall->callexpr->type == _ID || expr->value.funccall->callexpr->type == _STRING))
		{
			auto contains = containsMethod("0Main",expr->value.funccall->callexpr->value.id);

			if (contains==NULL)
			{
				throw string("Function " + string(expr->value.funccall->callexpr->value.id) + " wasn't found in global section");
			}
		}

		// �������� ����� callMethod
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","staticMethodCall","(Ljava/lang/String;[Lphprtl/stdClass;Ljava/lang/String;Ljava/lang/String;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToString","()Ljava/lang/String;");
		// ��������� ���������� ���������
		if (expr->value.funccall->paramsC>5)
			GlobalDescription::classes[classname]->addConstantInteger(expr->value.funccall->paramsC);

		exprModification(expr->value.funccall->callexpr,classname);
		elist =expr->value.funccall->params;
		while (elist)
		{
			exprModification(elist->expr,classname);
			elist = elist->next;
		}

		break;
	case _NEWOBJ:
		if (expr->value.funccall->callexpr->type == _ID || expr->value.funccall->callexpr->type == _STRING)
		{// ��������� ������������� ������
			if (strcmp(expr->value.funccall->callexpr->value.id,"stdClass"))
			{

				if (classes.count(expr->value.funccall->callexpr->value.id)==0)
					throw string("Cann't create object. Class " + string(expr->value.funccall->callexpr->value.id) + " wasn't found.");

				// ��������� ��������� ���������� ������������
				if (GlobalDescription::classes[expr->value.funccall->callexpr->value.id]->methods["__construct"]->local_vars.size()!=expr->value.funccall->paramsC)
				{
					throw string("Cann't create object. Constructor of class " + string(expr->value.funccall->callexpr->value.id) + " does not take " + string(itoa(expr->value.funccall->paramsC,number,10)) + " arguments. Expected : " + string(itoa(GlobalDescription::classes[expr->value.funccall->callexpr->value.id]->methods["__construct"]->local_vars.size(),number,10)));
				}
			}
			else if (expr->value.funccall->paramsC>0)
				throw string("stdClass object must take only 0 parameters");

		}

		// �������� ����� �������� ����� ��������
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","newObject","(Ljava/lang/String;)Lphprtl/stdClass;")->getNumber();
		
		exprModification(expr->value.funccall->callexpr,classname);
		
		elist =expr->value.funccall->params;
		while (elist)
		{
			exprModification(elist->expr,classname);
			elist = elist->next;
		}

		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToString","()Ljava/lang/String;");
		GlobalDescription::classes[classname]->addConstantString("__construct");
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","methodCall","(Ljava/lang/String;[Lphprtl/stdClass;Ljava/lang/String;)Lphprtl/stdClass;");
		break;
	case _CLONE:
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","myClone","()Lphprtl/stdClass;")->getNumber();
		break;

	//////////////////////////��������//////////////////////////////////////
	case _UMINUS:
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opUminus","()Lphprtl/stdClass;")->getNumber();
		break;
	case _SUB:
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opSub","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		break;
	case _INC:
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opInc","()Lphprtl/stdClass;")->getNumber();
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","dollar","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		break;
	case _DEC:
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opDec","()Lphprtl/stdClass;")->getNumber();
		break;
	case _INC_DOLLAR:
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","dollar","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","put","(Lphprtl/stdClass;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opInc","()Lphprtl/stdClass;")->getNumber();
		break;
	case _DOLLAR_INC:
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","myClone","()Lphprtl/stdClass;")->getNumber();
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opInc","()Lphprtl/stdClass;")->getNumber();
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","dollar","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","put","(Lphprtl/stdClass;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		break;
	case _DEC_DOLLAR:
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","dollar","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","put","(Lphprtl/stdClass;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opDec","()Lphprtl/stdClass;")->getNumber();
		break;
	case _DOLLAR_DEC:
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","myClone","()Lphprtl/stdClass;")->getNumber();
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opDec","()Lphprtl/stdClass;")->getNumber();
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","dollar","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","put","(Lphprtl/stdClass;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		break;
	case _ADD:
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opAdd","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		break;
	case _MUL:
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opMul","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		break;
	case _DIVISION:
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opDiv","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		break;	
	case _NOT:
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opNot","()Lphprtl/stdClass;")->getNumber();
		break;
	case _AND:
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opAnd","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToBoolean","()Z");
		break;
	case _OR:
		GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToBoolean","()Z");
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opOr","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		break;
	case _ASSIGN:
		//expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","put","(Lphprtl/stdClass;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		break;	
	case _POW:
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opPow","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		break;
	case _EQUAL:
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opEqual","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		break;
	case _NONEEQUAL:
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opNoneEqual","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		break;
	case _EQUAL_BY_TYPE:
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opEqualByType","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		break;
	case _NONEQUAL_BY_TYPE:
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","nonEqualByType","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		break;
	case _LESS:
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opLess","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		break;
	case _LESS_OR_EQUAL:
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opLessOrEqual","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		break;
	case _LARGER:
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opLarger","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		break;
	case _LARGER_OR_EQUAL:
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opLargerOrEqual","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		break;
	case _CONCAT:
		expr->constIndex = GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opConcat","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber();
		break;
	}
}

ClassMethodDescription * containsMethod (string classname,string methodname)
{
	if (classname == "0Main")
	{
		if (GlobalDescription::classes[classname]->methods.count(methodname) > 0)
			return GlobalDescription::classes[classname]->methods[methodname];
		return NULL;
	}


	while (classname != "stdClass" && classname != "0bject")
	{
		if (GlobalDescription::classes[classname]->methods.count(methodname) > 0)
			return GlobalDescription::classes[classname]->methods[methodname];

		classname = classes[classname];
	}

	return NULL;
}

ClassPropertyDescription * containsProperty (string classname,string propname)
{
	while (classname != "stdClass" && classname != "0bject")
	{
		if (GlobalDescription::classes[classname]->properties.count(propname) > 0)
			return GlobalDescription::classes[classname]->properties[propname];

		classname = classes[classname];
	}

	return NULL;
}

ClassMethodDescription * containsMethodGlobal (string methodname)
{
	for (auto i=GlobalDescription::classes.cbegin();i!=GlobalDescription::classes.cend();++i)
	{
		if (i->second->methods.count(methodname) > 0)
		{
			auto result = i->second->methods[methodname];
			return result;
		}

	}
	return NULL;
}

ClassPropertyDescription * containsPropertyGlobal (string propname)
{
	for (auto i=GlobalDescription::classes.cbegin();i!=GlobalDescription::classes.cend();++i)
	{
		if (i->second->properties.count(propname) > 0)
		{
			auto result = i->second->properties[propname];
			return result;
		}

	}
	return NULL;
}

bool isChildClass(string child,string parent)
{
	while (child != "stdClass" && child != "0bject" && child != "")
	{
		if (child == parent)
			return true;

		child = classes[child];
	}

	if (child == parent)
		return true;

	return false;
}

void checkMethods()
{
	for (auto i = GlobalDescription::classes.cbegin();i!=GlobalDescription::classes.cend();++i)
	{	

		if (i->first == "0Main")
			continue;

		string cl;
		
		for (auto j = i->second->methods.cbegin();j!=i->second->methods.cend();++j)
		{
			if (j->first == "<init>")
				continue;

			cl = i->first;

			bool isStatic = j->second->isStatic;

			while (cl != "stdClass" && cl != "0bject")
			{
				if (GlobalDescription::classes[cl]->methods.count(j->first)>0 
					&& (isStatic ^ GlobalDescription::classes[cl]->methods[j->first]->isStatic))
					throw string("Cann't combine static and nonstatic method " + j->first + " in classes: " + i->first + ", " + cl + ".");

				cl = classes[cl];
			}
		}


		for (auto j = i->second->properties.cbegin();j!=i->second->properties.cend();++j)
		{
			cl = i->first;

			bool isStatic = j->second->isStatic;

			while (cl != "stdClass" && cl != "0bject")
			{
				if (GlobalDescription::classes[cl]->properties.count(j->first)>0 
					&& (isStatic ^ GlobalDescription::classes[cl]->properties[j->first]->isStatic))
					throw string("Cann't combine static and nonstatic property " + j->first + " in classes: " + i->first + ", " + cl + ".");

				cl = classes[cl];
			}
		}
	}
}

struct Expression * mergeAssignAndSqBracket(struct Expression * expr)
{
	static struct Expression * left = NULL;

	if (expr && expr->type==_DCOLON &&( expr->right->type!=_DOLLAR && expr->right->type!=_FUNCTION ))
	{
		throw "Incorrect using :: operator";
	}
	
	if (expr && expr->type==_DCOLON && expr->right->type==_DOLLAR )
	{		
		left = expr->right;
		expr->right = left->right;
		free(left);
	}
	
	if (expr && expr->type==_ASSIGN && expr->left->type==_SQ_BRACKET)
	{
		expr->type = _SQ_BRACKET_ASSIGN;
		left = expr->left;
		expr->left = left->left;
		expr->center = left->right;
		free(left);
	}
	
	if (expr && expr->type==_ASSIGN && expr->left->type==_DCOLON)
	{
		expr->type = _DCOLON_ASSIGN;
		left = expr->left;
		expr->left = left->left;
		expr->center = left->right;
		free(left);
	}
	
	if (expr && expr->type==_ASSIGN && expr->left->type==_ARROW)
	{
		expr->type = _ARROW_ASSIGN;
		left = expr->left;
		expr->left = left->left;
		expr->center = left->right;
		free(left);
	}
	
	if (expr && expr->type==_ASSIGN && expr->left->type==_DOLLAR)
	{
		expr->type = _DOLLAR_ASSIGN;
		left = expr->left;
		expr->left = left->right;
		free(left);
	}

	if (expr && expr->type==_INC && (expr->left==NULL || expr->left->type!= _DOLLAR) && (expr->right==NULL || expr->right->type!=_DOLLAR))
	{
		throw string("Cann't increment nonvariable");
	}
	if (expr && expr->type==_INC && expr->left && expr->left->type== _DOLLAR)
	{
		//dol++
		expr->type = _DOLLAR_INC;
		left = expr->left;
		expr->left = left->right;
		free(left);

		
	}
	if (expr && expr->type==_INC && expr->right && expr->right->type== _DOLLAR)
	{
		//++dol
		expr->type = _INC_DOLLAR;
		left = expr->right;
		expr->right = left->right;
		free(left);
	}

	if (expr && expr->type==_DEC  && (expr->left==NULL || expr->left->type!= _DOLLAR) && (expr->right==NULL || expr->right->type!=_DOLLAR))
	{
		throw string("Cann't decrement nonvariable");
	}
	if (expr && expr->type==_DEC && expr->left && expr->left->type== _DOLLAR)
	{
		//dol--
		expr->type = _DOLLAR_DEC;
		left = expr->left;
		expr->left = left->right;
		free(left);
	}
	if (expr && expr->type==_DEC && expr->right && expr->right->type== _DOLLAR)
	{
		//--dol
		expr->type = _DEC_DOLLAR;
		left = expr->right;
		expr->right = left->right;
		free(left);
	}


	return expr;
}

StatementsList * forReplacement(struct StatementsList * stmts)
{
	StatementsList * root = stmts;
	StatementsList * rootWhl = NULL;
	StatementsList * prev = nullptr;
	StatementsList * next = nullptr;
	StatementsList * whl = nullptr;
	Statement * st;
	StatementsList * stList;

	while(stmts)
	{
		switch(stmts->stmt->code)
		{
		case _FOR:
			next = stmts ->next;
			// �� while
			ExpressionList * beforeCycle = stmts->stmt->stmt.forStmt->first;
			while (beforeCycle)
			{
				st = createStatement(_EXPR,beforeCycle->expr);
				stList = createStatementsList(st);

				if (prev)
				{
					prev->next = stList;					
				}
				else
				{
					root = stList;
				}
				prev = stList;
				beforeCycle = beforeCycle->next;
			}
			free(stmts->stmt->stmt.forStmt->first);

			//while
			if (stmts->stmt->stmt.forStmt->second)
				whl = createStatementsList(createStatement(_WHILEDO,createWhileDoStatement(stmts->stmt->stmt.forStmt->second,createStatement(_STMTLIST,stmts->stmt->stmt.forStmt->stmts))));
			else
				whl = createStatementsList(createStatement(_WHILEDO,createWhileDoStatement(createBooleanExpression(1),createStatement(_STMTLIST,stmts->stmt->stmt.forStmt->stmts))));
			prev ->next = whl;

			//afterwhile
			prev=whl->stmt->stmt.whiledoStmt->stmts;
			while (prev && prev->next)
				prev = prev->next;

			ExpressionList * afterCycle = stmts->stmt->stmt.forStmt->third;
			while (afterCycle)
			{
				st = createStatement(_EXPR,afterCycle->expr);
				stList = createStatementsList(st);

				if (prev)
					prev->next = stList;	
				else
					whl->stmt->stmt.whiledoStmt->stmts = stList;
								
				prev = stList;
				afterCycle = afterCycle->next;
			}
			free(stmts->stmt->stmt.forStmt->third);
			whl ->next = next;

			free(stmts->stmt->stmt.forStmt);
			stmts = whl;
			break;
		}
		prev=stmts;
		stmts=stmts->next;
	}
	
	return root;
}
