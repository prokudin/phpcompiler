#pragma once
#include "IConstant.h"
#include <stdio.h>

class ConstantInteger : public IConstant
{
public:
	ConstantInteger(int value) : IConstant(_C_INTEGER) { this->value = value; }
	~ConstantInteger(){}
	int value;
	virtual bool equal(IConstant * other){ return this->getType()==other->getType() && value == ((ConstantInteger*)other)->value; }
	virtual void print(){ printf("\t\t%d\tInt\t%d\n",number,value);}
	virtual string toCSV(){ return std::to_string((long long)number) + ";Int;" + std::to_string((long long)value) + "\n"; }
	virtual void toBytecode(int descr)
	{
		unsigned char u1 = 3;
		write(descr,(void*)&u1,1);

		unsigned int u4 = htonl(value);
		write(descr,(void*)&u4,4);
	}
};