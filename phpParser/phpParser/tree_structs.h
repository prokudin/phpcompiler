#pragma once

#include <stdio.h>
#include <malloc.h>
#include <string.h>



enum Enum{
	_NONE,	
	_DOLLAR,
	_UMINUS,
	_INC,
	_INC_DOLLAR,
	_DOLLAR_INC,
	_DEC,
	_DEC_DOLLAR,
	_DOLLAR_DEC,
	_ADD,
	_SUB,
	_MUL,
	_DIVISION,
	_DIV,
	_MOD,
	_NOT,
	_AND,
	_OR,
	_ASSIGN,
	_SQ_BRACKET_ASSIGN,
	_DCOLON_ASSIGN,
	_ARROW_ASSIGN,
	_DOLLAR_ASSIGN,
	_POW,
	_EQUAL,
	_NONEEQUAL,
	_EQUAL_BY_TYPE,
	_NONEQUAL_BY_TYPE,
	_LESS,
	_LESS_OR_EQUAL,
	_LARGER,
	_LARGER_OR_EQUAL,	
	_CONCAT,
	_SQ_BRACKET,
	_ARRAY_PUSHBACK,
	_OPEN_SQ_BR,
	_CLOSE_SQ_BR,
	_ARROW,
	_DCOLON,
	_CLONE,	
	_PUBLIC,
	_PROTECTED,
	_PRIVATE,	
	_EXPR,
	_IF,
	_FOR,
	_FOREACH,
	_WHILEDO,
	_DOWHILE,
	_ECHO,
	_FUNCTION,
	_CLASS,
	_RETURN,
	_STMTLIST,	
	_INTEGER,
	_BOOLEAN,
	_DOUBLE,
	_ARRAY,
	_STRING,
	_OBJECT,
	_USEROBJECT,
	_NEWOBJ,
	_NIL,
	_ID,
	_HTML,
	_GLOBAL,
	_VOID
};


struct Program
{
	struct HtmlText * stmts;
};

struct HtmlText
{
	char * html;
	int constIndex;
	struct StatementsList *stmts;
	struct HtmlText * next;
};


struct StatementsList
{
	struct Statement *stmt;
	struct StatementsList *next;
};


struct Statement
{
	int code;
	union 
	{
		struct IfStatement * ifStmt;
		struct ForStatement * forStmt;
		struct ForEachStatement * foreachStmt;
		struct WhileDoStatement * whiledoStmt;
		struct WhileDoStatement * dowhileStmt;
		struct Expression * echoStmt;
		struct FunctionStatement * funcStmt;
		struct Expression * exprStmt;
		struct Expression * returnStmt;
		struct ClassStatement * classStmt;
		struct StatementsList * stmtlist;
	} stmt;
};

struct IfStatement
{
	struct Expression * expr;
	struct StatementsList * thenlist;
	struct StatementsList * elselist;
};

struct ForStatement
{
	struct ExpressionList * first;
	struct Expression * second;
	struct ExpressionList * third;
	struct StatementsList * stmts;
};

struct ExpressionList
{
	struct Expression * expr;
	struct ExpressionList * next;
};

struct ForEachStatement
{
	struct Expression * arr;
	struct Expression * key;
	struct Expression * value;
	struct StatementsList * stmts;
};

struct WhileDoStatement
{
	struct Expression * expr;
	struct StatementsList * stmts;
};

struct FunctionList
{
	struct FunctionStatement * func;
	struct FunctionList * next;
};

struct FunctionStatement
{
	char * id;
	int isMethod;
	int isStatic;
	int visibility;

	struct FormalFunctionArglist * arglist;

	int returncode;
	char * returnObjectId;
	
	struct StatementsList * stmts;
};

struct FormalFunctionArglist
{
	struct FunctionArg * arg;
	struct FormalFunctionArglist * next;
};

struct FunctionArg
{
	char * id;
	int vartype;
	char * userobjectname;	
};

struct ClassStatement
{
	char * id;
	char * parentID;
	struct ClassMemberList * members;
};

struct ClassMemberList
{
	struct ClassMemberStatement * stmt;
	struct ClassMemberList * next;
};


struct ClassMemberStatement
{
	struct FunctionStatement * function;
	struct ClassProperty * prt;
};


struct ClassProperty
{
	char * id;
	int isStatic;
	int visibility;
	struct Expression * value;
};

struct ArrayMember
{
	struct Expression * key;
	struct Expression * value;
};

struct Array
{
	struct ArrayMember * value;
	struct Array * next;
};

struct FuncCall
{
	struct Expression * callexpr;
	struct ExpressionList * params;
	int paramsC;
};

struct Expression
{
	int type;
	int constIndex;
	union
	{
		int i;
		double d;
		char * s;
		char * id;
		struct Array * arr;
		struct FuncCall * funccall;
		struct FuncCall * newobj;
	} value;
	struct Expression *left;
	struct Expression *right;
	struct Expression *center;
};