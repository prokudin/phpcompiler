#pragma once
#include "ConstantUtf8.h"
#include "ClassDescription.h"
#include "ClassMethodDescription.h"
#include <fstream>
#include <iostream> 
#include <map>
#include <vector>

using namespace std;

class GlobalDescription
{
public:
	static map<string,ClassDescription*> classes;
	static void print();
	static void toCSV();
};

