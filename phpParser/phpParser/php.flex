
%{
 #include "php_tab.h"
 #include "MyStack.h"
 #include <math.h>
 #include <stdlib.h>
 #include <stdio.h>
 #include <string.h>
 #include <conio.h>
 #include <locale.h> 
 char * fromText(char * str);
 extern void yyerror(char const *s);
%}

%option noyywrap
%option never-interactive

ID [a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*
TRUE [tT][rR][uU][eE]
FALSE [fF][Aa][lL][sS][eE]
NULL [Nn][uU][lL]{2}

INT_DIGIT_10 [1-9][0-9]*|0
INT_DIGIT_16 0[xX][0-9a-fA-F]+
INT_DIGIT_8 [0-7]+
INT_DIGIT_2 0b[01]+

NUMBER          [0-9]+
DNUM          ([0-9]*[\.]{NUMBER})|({NUMBER}[\.][0-9]*)

FLOAT_NUM ({DNUM}|{NUMBER})
FLOAT_EXPONENT (({NUMBER}|{DNUM})[eE][+-]?{NUMBER})



%x OUT_OF_PHP
%x COMMENT
%x STRING_SINGLE
%x STRING_DOUBLE
%x SIMPLE_SYNTAX
%x COMPLEX_SYNTAX
%x HEREDOC
%x NOWDOC
%x HTML_COMMENT
%x HTML_STRING_SINGLE
%x HTML_STRING_DOUBLE
%x MUST_RETURN_TOKEN


%%


%{
	static int STATE=OUT_OF_PHP;
	static struct MyStack STATE_STACK = {{0,0,0,0,0,0,0,0,0,0} , 0};
	static struct MyStack TOKEN_STACK = {{0,0,0,0,0,0,0,0,0,0} , 0};
	static int branch = 0;
	static int isVar=0;
	static int endLex=0;

	int i=0;
	int int_n=0;
	double double_n=0;
	float float_n=0;
	static char buffer[10000]="";
	static char buffer2[100]="";
	static char htmlbuffer[100000]= "";
	static char single_char_str[2]={'x','\0'};	
	BEGIN(STATE);
%}


<OUT_OF_PHP>\<\!\-\- 			{ STATE=HTML_COMMENT; BEGIN(STATE); strcat(htmlbuffer,yytext); }
<HTML_COMMENT>[^\-]+ 			strcat(htmlbuffer,yytext);
<HTML_COMMENT>"-" 				strcat(htmlbuffer,yytext);
<HTML_COMMENT>\-{2,}\> 			{ STATE=OUT_OF_PHP; BEGIN(STATE); strcat(htmlbuffer,yytext); }

<OUT_OF_PHP>\"  				{ STATE=HTML_STRING_DOUBLE; BEGIN(STATE); strcat(htmlbuffer,yytext);}
<HTML_STRING_DOUBLE>[^\\\"]+	strcat(htmlbuffer,yytext);
<HTML_STRING_DOUBLE>\\\\		strcat(htmlbuffer,yytext);
<HTML_STRING_DOUBLE>\\\"		strcat(htmlbuffer,yytext);
<HTML_STRING_DOUBLE>\"			{ STATE=OUT_OF_PHP; BEGIN(STATE); strcat(htmlbuffer,yytext); }

<OUT_OF_PHP>\'  				{ STATE=HTML_STRING_SINGLE; BEGIN(STATE); strcat(htmlbuffer,yytext);}
<HTML_STRING_SINGLE>[^\\\']+	strcat(htmlbuffer,yytext);
<HTML_STRING_SINGLE>\\\\		strcat(htmlbuffer,yytext);
<HTML_STRING_SINGLE>\\\'		strcat(htmlbuffer,yytext);
<HTML_STRING_SINGLE>\'			{ STATE=OUT_OF_PHP; BEGIN(STATE); strcat(htmlbuffer,yytext); }


<OUT_OF_PHP>[^\<\'\"]+				strcat(htmlbuffer,yytext);
<OUT_OF_PHP>\<\?(php)?(\s|\n|" ") 	{ STATE=INITIAL; BEGIN(STATE); buffer[0]='\0'; yylval.statichtml = fromText(htmlbuffer); htmlbuffer[0]='\0';  return HTML;}
<OUT_OF_PHP>\<						strcat(htmlbuffer,yytext);
\?\> 						 		{ STATE = OUT_OF_PHP; BEGIN(STATE); /*printf("\nEnd of php code found ");*/  buffer[0]='\0'; }

<*><<EOF>>							{ if (endLex){return -1;}else{endLex=1;} if (STATE >=STRING_SINGLE && STATE<=NOWDOC ){ yyerror("EOF inside string");} buffer[0]='\0'; yylval.statichtml = fromText(htmlbuffer); htmlbuffer[0]='\0';  return HTML; }


"/*"  						{ BEGIN(COMMENT); }
<COMMENT>[^*]* 				;
<COMMENT>"*"+[^*/]* 		;
<COMMENT>"*"+"/" 			{ BEGIN(INITIAL); }

("//"|"#").*       			{ /*printf("\nSingle line comment was found") ;*/ }


<MUST_RETURN_TOKEN>""/.		{ 
								STATE = pop(&STATE_STACK);  
								BEGIN(STATE); 
								return pop(&TOKEN_STACK);
							}




"\""								{STATE = STRING_DOUBLE;  BEGIN(STATE);  buffer[0]='\0'; branch = 0;}
<STRING_DOUBLE>[^\\\"\$\{]+			{ strcat(buffer,yytext); 		}
<STRING_DOUBLE,HEREDOC>\\\$			{ strcat(buffer,"$"); }
<STRING_DOUBLE,HEREDOC>\\\{			{ strcat(buffer,"{"); }
<STRING_DOUBLE,HEREDOC>\{\$\}		{ strcat(buffer,"{$}"); }
<STRING_DOUBLE,HEREDOC>\$\{\}		{ strcat(buffer,"${}"); }
<STRING_DOUBLE,HEREDOC>\{\}			{ strcat(buffer,"{}"); }
<STRING_DOUBLE,HEREDOC>\$			{ 
										isVar = 1;
										yylval.string_const = fromText(buffer);
										push(&TOKEN_STACK,yytext[0]);   										 
										buffer[0]='\0'; 
										push(&STATE_STACK,STATE); 
										push(&STATE_STACK,SIMPLE_SYNTAX); 
										STATE = MUST_RETURN_TOKEN; BEGIN(STATE);
										return STR;
									}
<STRING_DOUBLE,HEREDOC>(\{\$)|(\$\{) {   
										isVar = 1;	
										push(&TOKEN_STACK,'$');
										push(&TOKEN_STACK,BGN_COMPL);
										yylval.string_const = fromText(buffer); 
										buffer[0]='\0'; 
										push(&STATE_STACK,STATE); 
										push(&STATE_STACK,COMPLEX_SYNTAX); 
										push(&STATE_STACK,MUST_RETURN_TOKEN); 
										STATE = MUST_RETURN_TOKEN; BEGIN(STATE); 
										branch++;
										return STR;
									}
<STRING_DOUBLE,HEREDOC>\{			{ strcat(buffer,yytext);  }
<STRING_DOUBLE,HEREDOC>\\\\			 { strcat(buffer,"\\");  }
<STRING_DOUBLE,HEREDOC>\\n			 { strcat(buffer,"\n");  }
<STRING_DOUBLE,HEREDOC>\\r			 { strcat(buffer,"\r");}
<STRING_DOUBLE,HEREDOC>\\t			 { strcat(buffer,"\t");}
<STRING_DOUBLE,HEREDOC>\\v			 { strcat(buffer,"\v");}
<STRING_DOUBLE,HEREDOC>\\e			 { strcat(buffer,"\e");}
<STRING_DOUBLE,HEREDOC>\\f			 { strcat(buffer,"\f");}
<STRING_DOUBLE,HEREDOC>\\[0-7]{1,3}  { single_char_str[0]=(char)strtol(yytext+1,NULL,8);  strcat(buffer,single_char_str);}
<STRING_DOUBLE,HEREDOC>\\x[0-9A-Fa-f]{1,2}  {single_char_str[0]=(char)strtol(yytext+2,NULL,16); strcat(buffer,single_char_str);}
<STRING_DOUBLE,HEREDOC>\\u[0-9A-Fa-f]+  { single_char_str[0]=(char)strtol(yytext+2,NULL,16); strcat(buffer,single_char_str);}
<STRING_DOUBLE>\\\"			 { strcat(buffer,"\""); }
<STRING_DOUBLE>\"			 { yylval.string_const = fromText(buffer); STATE = INITIAL;  BEGIN(INITIAL); return STR; }

<COMPLEX_SYNTAX>\{			{ isVar = 0; branch++; return '{';}
<SIMPLE_SYNTAX>{ID}			{ isVar = 0; yylval.id = fromText(yytext); return ID;}
<SIMPLE_SYNTAX,COMPLEX_SYNTAX>\[			{ return '[';   }
<SIMPLE_SYNTAX,COMPLEX_SYNTAX>\]			{ return ']';   }
<SIMPLE_SYNTAX,COMPLEX_SYNTAX>{INT_DIGIT_10}				{ int_n =(int)strtol(yytext,NULL,10); yylval.int_const = int_n;   return INT;}
<SIMPLE_SYNTAX,COMPLEX_SYNTAX>{INT_DIGIT_16}				{ int_n = (int)strtol(yytext,NULL,16);   yylval.int_const = int_n;   return INT;}
<SIMPLE_SYNTAX,COMPLEX_SYNTAX>{INT_DIGIT_8}				{ int_n = (int)strtol(yytext,NULL,8);  yylval.int_const = int_n;   return INT;}
<SIMPLE_SYNTAX,COMPLEX_SYNTAX>{INT_DIGIT_2}				{ if (yytext[1]=='b'){ int_n =(int)strtol(yytext+2,NULL,2);} else { int_n =(-1)*(int)strtol(yytext+3,NULL,2); } yylval.int_const = int_n;   return INT;}

<SIMPLE_SYNTAX,COMPLEX_SYNTAX>{FLOAT_NUM} 				{ char * ptr; double_n=strtod(yytext,&ptr); /*printf ("\nNFloat num \"%f\" was found, Str = %s",double_n,yytext);*/ yylval.double_const = double_n; return DBL; }
<SIMPLE_SYNTAX,COMPLEX_SYNTAX>{FLOAT_EXPONENT}			{ double_n=atof(yytext); /*printf ("\nEFloat num \"%f\" was found, Str = %s",double_n,yytext); */yylval.double_const = double_n; return DBL;}
<SIMPLE_SYNTAX,COMPLEX_SYNTAX>"->"			{ return ARROW; }
<SIMPLE_SYNTAX,COMPLEX_SYNTAX>"::"			{ return DCOLON; }
<SIMPLE_SYNTAX,COMPLEX_SYNTAX>\n+			{ STATE = pop(&STATE_STACK);  BEGIN(STATE); }
<SIMPLE_SYNTAX>""/.							{ STATE = pop(&STATE_STACK);  BEGIN(STATE); }

<COMPLEX_SYNTAX>\}  		{  branch--;  if (branch==0) { STATE = pop(&STATE_STACK);  BEGIN(STATE); return END_COMPL; } else {return '}';}}
<COMPLEX_SYNTAX>\'			{ 
								STATE = STRING_SINGLE;
								push(&STATE_STACK,COMPLEX_SYNTAX); 
								BEGIN(STATE); buffer[0]='\0';
							}
<COMPLEX_SYNTAX>\"			{ 
								STATE = STRING_DOUBLE;
								push(&STATE_STACK,COMPLEX_SYNTAX); 
								BEGIN(STATE); buffer[0]='\0';
							}

\'							{ STATE =STRING_SINGLE;  BEGIN(STATE); buffer[0]='\0';}
<STRING_SINGLE>[^\\\']+		{ strcat(buffer,yytext);  }
<STRING_SINGLE>\\\\		    { strcat(buffer,"\\");  }
<STRING_SINGLE>\\n			{ strcat(buffer,"\n");  }
<STRING_SINGLE>\\\'			{ strcat(buffer,"'");  }
<STRING_SINGLE>\'			{ yylval.string_const = fromText(buffer); buffer[0]='\0'; STATE = pop(&STATE_STACK);  BEGIN(STATE); return STR;}


"<<<"\'{ID}\'             { STATE = NOWDOC; BEGIN(STATE); buffer[0]='\0'; strcpy(buffer2,yytext+4); buffer2[strlen(buffer2)-1]='\0'; }
<NOWDOC>[^\s\n;]+			{
								if (strcmp(yytext,buffer2)==0)
								{
									STATE = INITIAL;
									BEGIN(STATE);
									yylval.string_const = fromText(buffer);
									return STR;
								}
								else
								{
									strcat(buffer,yytext);
								}
							}
<NOWDOC>\s.*				{ strcat(buffer,yytext); }
<NOWDOC>\n+					{ strcat(buffer,yytext); }
<NOWDOC>.					{ strcat(buffer,yytext); }

"<<<"{ID}					{ STATE = HEREDOC;  BEGIN(HEREDOC); buffer[0]='\0'; strcpy(buffer2,yytext+3); buffer2[strlen(buffer2)]='\0'; }
<HEREDOC>[^\s\n;\$\{\\]+	{
								if (strcmp(yytext,buffer2)==0)
								{
									STATE = INITIAL;
									BEGIN(STATE);
									yylval.string_const = fromText(buffer);
									buffer[0]='\0';
									return STR;
								}
								else
								{
									strcat(buffer,yytext);
								}
							}
<HEREDOC>\s.*				{ strcat(buffer,yytext); }
<HEREDOC,STRING_DOUBLE>\n+	{ strcat(buffer,yytext); }
<HEREDOC>;					{ strcat(buffer,yytext); }

<COMPLEX_SYNTAX,INITIAL>array	 	 				{ return ARRAY; }
class 	 					{ return CLASS; }
const						{ return CONST; }
clone						{ return CLONE;}
do							{ return DO; }
echo						{ return ECHO; }
else						{ return ELSE; }
elseif						{ push(&TOKEN_STACK,IF); STATE = MUST_RETURN_TOKEN; BEGIN(STATE); return ELSE; }
for							{ return FOR; }
foreach						{ return FOREACH; }
as						    { return AS; }
function					{ return FUNCTION; }
if							{ return IF; }
<COMPLEX_SYNTAX,INITIAL>new							{ return NEW; }
private						{ return PRIVATE; }
protected					{ return PROTECTED; }
public						{ return PUBLIC; }
return						{ return RETURN; }
static						{ return STATIC; }
while						{ return WHILE; }
object 						{ return OBJECT; }
extends 					{ return EXTENDS; }
bool						{ return BOOLEAN; }
float						{ return DOUBLE; }
int							{ return INTEGER; }
string						{ return STRING; }
<COMPLEX_SYNTAX,INITIAL>nil							{ return NIL; }

<COMPLEX_SYNTAX,INITIAL>\${ID}						{ isVar = 0; yylval.id=fromText(yytext+1); if(STATE==COMPLEX_SYNTAX){ push(&STATE_STACK,STATE); }  push(&TOKEN_STACK,ID);   STATE = MUST_RETURN_TOKEN; BEGIN(STATE); return *yytext;}
	
";" 						{ return *yytext;}

<COMPLEX_SYNTAX,INITIAL>"++"						{ return INC;}
<COMPLEX_SYNTAX,INITIAL>"--"						{ return DEC;}
<COMPLEX_SYNTAX,INITIAL>"!" 						{ return *yytext;}
<COMPLEX_SYNTAX,INITIAL>"*" 						{ return *yytext;}
<COMPLEX_SYNTAX,INITIAL>"**" 						{ return POW;}
<COMPLEX_SYNTAX,INITIAL>"/" 						{ return *yytext;}
<COMPLEX_SYNTAX,INITIAL>"%" 						{ return *yytext;}
<COMPLEX_SYNTAX,INITIAL>"+" 						{ return *yytext;}
<COMPLEX_SYNTAX,INITIAL>"." 						{ return *yytext;}
<COMPLEX_SYNTAX,INITIAL>":" 						{ return *yytext;}
<COMPLEX_SYNTAX,INITIAL>"," 						{ return *yytext;}
<COMPLEX_SYNTAX,INITIAL>"-" 						{ return *yytext;}
<COMPLEX_SYNTAX,INITIAL>"<" 						{ return LESS;}
<COMPLEX_SYNTAX,INITIAL>"<=" 						{ return LESS_OR_EQUAL;}
<COMPLEX_SYNTAX,INITIAL>">" 						{ return LARGER;}
<COMPLEX_SYNTAX,INITIAL>">=" 						{ return LARGER_OR_EQUAL;}
<COMPLEX_SYNTAX,INITIAL>"==" 						{ return EQUAL;}
<COMPLEX_SYNTAX,INITIAL>"!=" 						{ return NONEQUAL;}
<COMPLEX_SYNTAX,INITIAL>"===" 						{ return EQUAL_BY_TYPE;}
<COMPLEX_SYNTAX,INITIAL>"!==" 						{ return NONEQUAL_BY_TYPE;}
<COMPLEX_SYNTAX,INITIAL>"&&" 						{ return AND;}
<COMPLEX_SYNTAX,INITIAL>"||" 						{ return OR;}
<COMPLEX_SYNTAX,INITIAL>"=" 						{ return *yytext;}

<COMPLEX_SYNTAX,INITIAL>"[" 						{ return *yytext;}
<COMPLEX_SYNTAX,INITIAL>"]"							{ return *yytext;}
<COMPLEX_SYNTAX,INITIAL>"("							{ if (isVar) return 0; return *yytext;}
<COMPLEX_SYNTAX,INITIAL>")"							{ return *yytext;}
"{"							{ isVar = 0; return *yytext;}
"}"							{ return *yytext;}

<COMPLEX_SYNTAX,INITIAL>"::" 						{ return DCOLON;}
<COMPLEX_SYNTAX,INITIAL>"->"						{ return ARROW;}
<COMPLEX_SYNTAX,INITIAL>"=>" 						{ return DARROW;}

<COMPLEX_SYNTAX,INITIAL>"$"							{ isVar = 1; return *yytext;}

<COMPLEX_SYNTAX,INITIAL>{TRUE}                      {yylval.int_const = 1; return BOOL;}
<COMPLEX_SYNTAX,INITIAL>{FALSE}						{yylval.int_const = 0; return BOOL;}
<COMPLEX_SYNTAX,INITIAL>{NULL}						{ return NIL;}

<COMPLEX_SYNTAX,INITIAL>{ID}   						{isVar =0; yylval.id = fromText(yytext); return ID;}

{INT_DIGIT_10}				{ int_n =(int)strtol(yytext,NULL,10); yylval.int_const = int_n;   return INT;}
{INT_DIGIT_16}				{ int_n = (int)strtol(yytext,NULL,16);   yylval.int_const = int_n;   return INT;}
{INT_DIGIT_8}				{ int_n = (int)strtol(yytext,NULL,8);  yylval.int_const = int_n;   return INT;}
{INT_DIGIT_2}				{ if (yytext[1]=='b'){ int_n =(int)strtol(yytext+2,NULL,2);} else { int_n =(-1)*(int)strtol(yytext+3,NULL,2); } yylval.int_const = int_n;   return INT;}

{FLOAT_NUM} 				{ char * ptr; double_n=strtod(yytext,&ptr);/* printf ("\nNFloat num \"%f\" was found, Str = %s",double_n,yytext);*/ yylval.double_const = double_n; return DBL; }
{FLOAT_EXPONENT}			{ double_n=atof(yytext); /*printf ("\nEFloat num \"%f\" was found, Str = %s",double_n,yytext);*/ yylval.double_const = double_n; return DBL;}

							
" "{1,3}|"\n"|"\n\r"|"\r\n"|"\r"|"\t"|\s	{  /*������� ������ ���� isVar*/}
	
.	printf( "\nUnrecognized character: %s\n", yytext );

%%



char * fromText(char * str)
{
	char * res = (char*)malloc(sizeof(char)*(strlen(str) + 1));
	strcpy(res, str);
	return res;
}

