#pragma once
#include "IConstant.h"
#include "ConstantClass.h"
#include "ConstantFieldref.h"
#include "ClassPropertyDescription.h"
#include "ClassMethodDescription.h"
#include <iostream> 
#include <vector>
#include <map>

using namespace std;

class VarDescription
{
public:
	VarDescription(string name,int type=-1,string objName=""){ this->name=name;this->type=type;this->userObjectName = objName; }
	int type;
	string name;
	string userObjectName;
};
