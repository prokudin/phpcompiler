#pragma once

union u1
{
	unsigned char number;
	char bytes[1];
};

union s1
{
	char number;
	char bytes[1];
};

union s2 
{
	short number;
	char bytes[2];
};

union u2
{
	unsigned short number;
	char bytes[2];
};

union u4
{
	unsigned long int number;
	char bytes[4];
};

union s4
{
	long int number;
	char bytes[4];
};

union s4F
{
	float number;
	char bytes[4];
};

char * swap(char * arr, int size);


int swapS4(int val);
unsigned int swapU4(unsigned int val);
short swapS2(short val);
unsigned short swapU2(unsigned short val);