typedef union {
	int int_const;
	double double_const;
	char * string_const;	
	char * statichtml;	
	char* id;	
	int valuetype;
	int visibilitytype;
	
	struct Program * prgm;
	struct HtmlText * htmltext;
	struct StatementsList * statementsList;
	struct Statement * statement;
	struct IfStatement * ifStatement;
	struct ForStatement * forStatement;
	struct ExpressionList * expressionList;
	struct ForEachStatement * forEachStatement;
	struct WhileDoStatement * whileDoStatement;
	struct WhileDoStatement * doWhileStatement;
	struct FunctionList * functionList;
	struct FunctionStatement * functionStatement;
	struct FormalFunctionArglist * formalFunctionArglist;
	struct ClassStatement * classStatement;
	struct ClassMemberList * classMemberList;
	struct ClassMemberStatement * classMemberStatement;
	struct Array * arr;
	struct Expression * expression;
} YYSTYPE;
#define	INT	258
#define	DBL	259
#define	BOOL	260
#define	STR	261
#define	HTML	262
#define	ARRAY	263
#define	CLASS	264
#define	CONST	265
#define	DO	266
#define	ECHO	267
#define	ELSE	268
#define	ELSEIF	269
#define	FOR	270
#define	FOREACH	271
#define	AS	272
#define	FUNCTION	273
#define	IF	274
#define	NAMESPACE	275
#define	NEW	276
#define	PRIVATE	277
#define	PROTECTED	278
#define	PUBLIC	279
#define	RETURN	280
#define	STATIC	281
#define	WHILE	282
#define	OBJECT	283
#define	COUNT	284
#define	EXTENDS	285
#define	INC	286
#define	DEC	287
#define	POW	288
#define	INTEGER	289
#define	BOOLEAN	290
#define	STRING	291
#define	DOUBLE	292
#define	ID	293
#define	NIL	294
#define	DARROW	295
#define	OR	296
#define	AND	297
#define	EQUAL	298
#define	NONEQUAL	299
#define	EQUAL_BY_TYPE	300
#define	NONEQUAL_BY_TYPE	301
#define	LESS	302
#define	LESS_OR_EQUAL	303
#define	LARGER	304
#define	LARGER_OR_EQUAL	305
#define	UMINUS	306
#define	CLONE	307
#define	DCOLON	308
#define	ARROW	309
#define	BGN_COMPL	310
#define	END_COMPL	311


extern YYSTYPE yylval;
