#pragma once
#include "IConstant.h"
#include <iostream> 
#include <stdio.h>
#include <winsock2.h>
#include <stdint.h>
#include <windows.h>

using namespace std;
#include <string>

class ConstantUtf8 : public IConstant
{
public:
	ConstantUtf8(std::string str) : IConstant(_C_UTF8) { value = str; }
	~ConstantUtf8(){}
	string value;
	virtual bool equal(IConstant * other){ return this->getType() == other->getType() && this->value == ((ConstantUtf8*)other)->value;}
	virtual void print(){ printf("\t\t%d\tUTF-8\t%s\n",number,value.c_str());}
	virtual string toCSV(){ return std::to_string((long long)number) + ";UTF-8;\"" + value + "\"\n";  }
	virtual void toBytecode(int descr)
	{
		unsigned char u1 = 1;
		write(descr,(void*)&u1,1);

		//if (value.size()==0)
		//{
			unsigned short u2 = htons(value.size());
			write(descr,(void*)&u2,2);

			write(descr,(void*)value.c_str(),value.size());
			return;
		/*}

		int size = MultiByteToWideChar(CP_ACP, MB_COMPOSITE, value.c_str(),
                               value.length(), nullptr, 0);
		std::wstring utf16_str(size, '\0');
		MultiByteToWideChar(CP_ACP, MB_COMPOSITE, value.c_str(),
                    value.length(), &utf16_str[0], size);

		int utf8_size = WideCharToMultiByte(CP_UTF8, 0, utf16_str.c_str(),
                                    utf16_str.length(), nullptr, 0,
                                    nullptr, nullptr);
		std::string utf8_str(utf8_size, '\0');
		WideCharToMultiByte(CP_UTF8, 0, utf16_str.c_str(),
                    utf16_str.length(), &utf8_str[0], utf8_size,
                    nullptr, nullptr);


		unsigned short u2 = htons(utf8_size);
		write(descr,(void*)&u2,2);

		write(descr,(void*)utf8_str.c_str(),utf8_size);*/
	}
};