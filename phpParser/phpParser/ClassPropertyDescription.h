#pragma once
#include "ConstantString.h"
#include "tree_structs.h"

class ClassPropertyDescription
{
public:
	ConstantString * name;
	Expression * value;
	int visibility;
	bool isStatic;
};