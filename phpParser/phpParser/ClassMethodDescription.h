#pragma once
#include "ConstantUtf8.h"
#include "VarDescription.h"
#include "tree_structs.h"
#include <iostream> 
#include <map>
#include <vector>

using namespace std;

class ClassMethodDescription
{
public:
	ConstantUtf8 * name;
	ConstantUtf8 * descriptor;
	FunctionStatement * code;
	int visibility;
	int returnType;
	bool isStatic;
	vector<VarDescription> local_vars;
};
