
/*  A Bison parser, made from php.y with Bison version GNU Bison version 1.24
  */

#define YYBISON 1  /* Identify Bison output.  */

#define	INT	258
#define	DBL	259
#define	BOOL	260
#define	STR	261
#define	HTML	262
#define	ARRAY	263
#define	CLASS	264
#define	CONST	265
#define	DO	266
#define	ECHO	267
#define	ELSE	268
#define	ELSEIF	269
#define	FOR	270
#define	FOREACH	271
#define	AS	272
#define	FUNCTION	273
#define	IF	274
#define	NAMESPACE	275
#define	NEW	276
#define	PRIVATE	277
#define	PROTECTED	278
#define	PUBLIC	279
#define	RETURN	280
#define	STATIC	281
#define	WHILE	282
#define	OBJECT	283
#define	COUNT	284
#define	EXTENDS	285
#define	INC	286
#define	DEC	287
#define	POW	288
#define	INTEGER	289
#define	BOOLEAN	290
#define	STRING	291
#define	DOUBLE	292
#define	ID	293
#define	NIL	294
#define	DARROW	295
#define	OR	296
#define	AND	297
#define	EQUAL	298
#define	NONEQUAL	299
#define	EQUAL_BY_TYPE	300
#define	NONEQUAL_BY_TYPE	301
#define	LESS	302
#define	LESS_OR_EQUAL	303
#define	LARGER	304
#define	LARGER_OR_EQUAL	305
#define	UMINUS	306
#define	CLONE	307
#define	DCOLON	308
#define	ARROW	309
#define	BGN_COMPL	310
#define	END_COMPL	311

#line 1 "php.y"

#include "tree_functions.h"
#include "tree_print.h"
#define YYDEBUG 1
#include <stdio.h>
extern int yylex();
int isOk =1;
void yyerror(char const *s);
struct Program * _yyprg = NULL;

#line 12 "php.y"
typedef union {
	int int_const;
	double double_const;
	char * string_const;	
	char * statichtml;	
	char* id;	
	int valuetype;
	int visibilitytype;
	
	struct Program * prgm;
	struct HtmlText * htmltext;
	struct StatementsList * statementsList;
	struct Statement * statement;
	struct IfStatement * ifStatement;
	struct ForStatement * forStatement;
	struct ExpressionList * expressionList;
	struct ForEachStatement * forEachStatement;
	struct WhileDoStatement * whileDoStatement;
	struct WhileDoStatement * doWhileStatement;
	struct FunctionList * functionList;
	struct FunctionStatement * functionStatement;
	struct FormalFunctionArglist * formalFunctionArglist;
	struct ClassStatement * classStatement;
	struct ClassMemberList * classMemberList;
	struct ClassMemberStatement * classMemberStatement;
	struct Array * arr;
	struct Expression * expression;
} YYSTYPE;

#ifndef YYLTYPE
typedef
  struct yyltype
    {
      int timestamp;
      int first_line;
      int first_column;
      int last_line;
      int last_column;
      char *text;
   }
  yyltype;

#define YYLTYPE yyltype
#endif

#include <stdio.h>

#ifndef __cplusplus
#ifndef __STDC__
#define const
#endif
#endif



#define	YYFINAL		275
#define	YYFLAG		-32768
#define	YYNTBASE	75

#define YYTRANSLATE(x) ((unsigned)(x) <= 311 ? yytranslate[x] : 109)

static const char yytranslate[] = {     0,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,    59,     2,     2,    71,    57,     2,     2,    65,
    66,    55,    53,    73,    52,    54,    56,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,    74,    72,     2,
    41,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
    60,     2,    61,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,    67,     2,    68,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     1,     2,     3,     4,     5,
     6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
    16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
    26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
    36,    37,    38,    39,    40,    42,    43,    44,    45,    46,
    47,    48,    49,    50,    51,    58,    62,    63,    64,    69,
    70
};

#if YYDEBUG != 0
static const short yyprhs[] = {     0,
     0,     2,     4,     6,     9,    12,    14,    17,    19,    22,
    24,    26,    28,    30,    32,    34,    36,    40,    42,    44,
    46,    47,    49,    51,    54,    56,    58,    60,    62,    64,
    65,    67,    70,    74,    78,    84,    90,    95,   104,   115,
   126,   132,   140,   141,   143,   145,   148,   150,   153,   156,
   159,   164,   171,   176,   183,   186,   189,   191,   193,   195,
   201,   209,   219,   228,   237,   249,   251,   253,   259,   269,
   273,   277,   281,   285,   289,   293,   296,   300,   304,   308,
   312,   316,   320,   324,   328,   332,   336,   340,   344,   347,
   350,   353,   356,   359,   361,   363,   365,   367,   369,   372,
   377,   382,   388,   393,   397,   403,   407,   413,   416,   419,
   423,   424,   426,   428,   432,   437,   441,   442,   444,   448,
   450,   454,   460,   464,   468,   470,   474,   480,   482,   484,
   486
};

static const short yyrhs[] = {    76,
     0,    77,     0,     7,     0,    76,    77,     0,    76,     7,
     0,    79,     0,    77,    79,     0,    72,     0,    98,    72,
     0,    92,     0,    93,     0,    96,     0,    97,     0,    94,
     0,   105,     0,   106,     0,    67,    80,    68,     0,    78,
     0,    85,     0,    86,     0,     0,    81,     0,    78,     0,
    81,    78,     0,    34,     0,    37,     0,    36,     0,     8,
     0,    35,     0,     0,    84,     0,    71,    38,     0,    38,
    71,    38,     0,    82,    71,    38,     0,    84,    73,    38,
    71,    38,     0,    84,    73,    82,    71,    38,     0,    84,
    73,    71,    38,     0,    18,    38,    65,    83,    66,    67,
    80,    68,     0,    18,    38,    65,    83,    66,    74,    82,
    67,    80,    68,     0,    18,    38,    65,    83,    66,    74,
    38,    67,    80,    68,     0,     9,    38,    67,    87,    68,
     0,     9,    38,    30,    38,    67,    87,    68,     0,     0,
    88,     0,    89,     0,    88,    89,     0,    85,     0,    91,
    85,     0,    26,    85,     0,    90,    85,     0,    91,    71,
    38,    72,     0,    91,    71,    38,    41,    98,    72,     0,
    90,    71,    38,    72,     0,    90,    71,    38,    41,    98,
    72,     0,    26,    91,     0,    91,    26,     0,    24,     0,
    22,     0,    23,     0,    19,    65,    98,    66,    78,     0,
    19,    65,    98,    66,    78,    13,    78,     0,    15,    65,
   100,    72,    98,    72,   100,    66,    78,     0,    15,    65,
   100,    72,    72,   100,    66,    78,     0,    16,    65,    95,
    17,    71,    38,    66,    78,     0,    16,    65,    95,    17,
    71,    38,    40,    71,    38,    66,    78,     0,   102,     0,
    99,     0,    27,    65,    98,    66,    78,     0,    11,    67,
    80,    68,    27,    65,    98,    66,    72,     0,    98,    53,
    98,     0,    98,    52,    98,     0,    98,    55,    98,     0,
    98,    56,    98,     0,    98,    33,    98,     0,    65,    98,
    66,     0,    52,    98,     0,    98,    41,    98,     0,    98,
    42,    98,     0,    98,    43,    98,     0,    98,    44,    98,
     0,    98,    45,    98,     0,    98,    46,    98,     0,    98,
    47,    98,     0,    98,    48,    98,     0,    98,    49,    98,
     0,    98,    50,    98,     0,    98,    51,    98,     0,    98,
    54,    98,     0,    59,    98,     0,    31,    98,     0,    98,
    31,     0,    32,    98,     0,    98,    32,     0,    99,     0,
   108,     0,   102,     0,    39,     0,    38,     0,    71,    99,
     0,    71,    67,    98,    68,     0,    99,    60,    98,    61,
     0,    99,    60,    61,    41,    98,     0,    99,    65,   100,
    66,     0,    99,    64,    99,     0,    99,    64,    67,    98,
    68,     0,    99,    63,    99,     0,    99,    63,    67,    98,
    68,     0,    21,    99,     0,    62,    99,     0,    65,    99,
    66,     0,     0,   101,     0,    98,     0,   101,    73,    98,
     0,     8,    65,   103,    66,     0,    60,   103,    61,     0,
     0,   104,     0,    98,    40,    98,     0,    98,     0,   104,
    73,    98,     0,   104,    73,    98,    40,    98,     0,    12,
    98,    72,     0,    25,    98,    72,     0,     6,     0,   107,
    99,     6,     0,   107,    69,    98,    70,     6,     0,     3,
     0,     4,     0,   107,     0,     5,     0
};

#endif

#if YYDEBUG != 0
static const short yyrline[] = { 0,
   141,   144,   145,   146,   147,   150,   151,   154,   156,   157,
   158,   159,   160,   161,   162,   163,   164,   167,   168,   169,
   172,   173,   176,   177,   180,   181,   182,   183,   184,   187,
   188,   191,   192,   193,   194,   195,   196,   198,   199,   200,
   203,   204,   207,   208,   211,   212,   214,   215,   216,   217,
   218,   219,   220,   221,   224,   225,   228,   229,   230,   233,
   234,   237,   238,   241,   242,   245,   246,   249,   252,   255,
   256,   257,   258,   259,   260,   261,   262,   263,   264,   265,
   266,   267,   268,   269,   270,   271,   272,   273,   274,   275,
   276,   277,   278,   279,   280,   281,   282,   285,   286,   287,
   288,   289,   290,   291,   292,   293,   294,   295,   296,   297,
   302,   303,   306,   307,   310,   311,   314,   315,   318,   319,
   320,   321,   324,   327,   330,   331,   332,   335,   336,   337,
   338
};

static const char * const yytname[] = {   "$","error","$undefined.","INT","DBL",
"BOOL","STR","HTML","ARRAY","CLASS","CONST","DO","ECHO","ELSE","ELSEIF","FOR",
"FOREACH","AS","FUNCTION","IF","NAMESPACE","NEW","PRIVATE","PROTECTED","PUBLIC",
"RETURN","STATIC","WHILE","OBJECT","COUNT","EXTENDS","INC","DEC","POW","INTEGER",
"BOOLEAN","STRING","DOUBLE","ID","NIL","DARROW","'='","OR","AND","EQUAL","NONEQUAL",
"EQUAL_BY_TYPE","NONEQUAL_BY_TYPE","LESS","LESS_OR_EQUAL","LARGER","LARGER_OR_EQUAL",
"'-'","'+'","'.'","'*'","'/'","'%'","UMINUS","'!'","'['","']'","CLONE","DCOLON",
"ARROW","'('","')'","'{'","'}'","BGN_COMPL","END_COMPL","'$'","';'","','","':'",
"program","doc","stmt_list","stmt_incycle","stmt","stmt_incycle_listE","stmt_incycle_list",
"type","func_arglistE","func_arglist","function","class_stmt","class_memberlistE",
"class_memberlist","class_member","classfield_param","visibility","if_stmt",
"for_stmt","foreach_stmt","foreach_arr","whiledo_stmt","dowhile_stmt","expr",
"expr_var","expr_listE","expr_list","array","keyvalue_listE","keyvalue_list",
"echo_stmt","return_stmt","string","value",""
};
#endif

static const short yyr1[] = {     0,
    75,    76,    76,    76,    76,    77,    77,    78,    78,    78,
    78,    78,    78,    78,    78,    78,    78,    79,    79,    79,
    80,    80,    81,    81,    82,    82,    82,    82,    82,    83,
    83,    84,    84,    84,    84,    84,    84,    85,    85,    85,
    86,    86,    87,    87,    88,    88,    89,    89,    89,    89,
    89,    89,    89,    89,    90,    90,    91,    91,    91,    92,
    92,    93,    93,    94,    94,    95,    95,    96,    97,    98,
    98,    98,    98,    98,    98,    98,    98,    98,    98,    98,
    98,    98,    98,    98,    98,    98,    98,    98,    98,    98,
    98,    98,    98,    98,    98,    98,    98,    99,    99,    99,
    99,    99,    99,    99,    99,    99,    99,    99,    99,    99,
   100,   100,   101,   101,   102,   102,   103,   103,   104,   104,
   104,   104,   105,   106,   107,   107,   107,   108,   108,   108,
   108
};

static const short yyr2[] = {     0,
     1,     1,     1,     2,     2,     1,     2,     1,     2,     1,
     1,     1,     1,     1,     1,     1,     3,     1,     1,     1,
     0,     1,     1,     2,     1,     1,     1,     1,     1,     0,
     1,     2,     3,     3,     5,     5,     4,     8,    10,    10,
     5,     7,     0,     1,     1,     2,     1,     2,     2,     2,
     4,     6,     4,     6,     2,     2,     1,     1,     1,     5,
     7,     9,     8,     8,    11,     1,     1,     5,     9,     3,
     3,     3,     3,     3,     3,     2,     3,     3,     3,     3,
     3,     3,     3,     3,     3,     3,     3,     3,     2,     2,
     2,     2,     2,     1,     1,     1,     1,     1,     2,     4,
     4,     5,     4,     3,     5,     3,     5,     2,     2,     3,
     0,     1,     1,     3,     4,     3,     0,     1,     3,     1,
     3,     5,     3,     3,     1,     3,     5,     1,     1,     1,
     1
};

static const short yydefact[] = {     0,
   128,   129,   131,   125,     3,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,    98,    97,
     0,     0,   117,     0,     0,    21,     0,     8,     1,     2,
    18,     6,    19,    20,    10,    11,    14,    12,    13,     0,
    94,    96,    15,    16,   130,    95,   117,     0,    21,     0,
   111,     0,     0,     0,     0,   108,     0,     0,    90,    92,
    76,    89,   120,     0,   118,   109,     0,    94,    23,     0,
    22,     0,    99,     5,     4,     7,    91,    93,     0,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     9,     0,     0,     0,   111,
     0,     0,     0,     0,    43,     0,   123,   113,     0,   112,
     0,    67,    66,    30,     0,     0,   124,     0,     0,   116,
     0,    75,   110,    17,    24,     0,    74,    77,    78,    79,
    80,    81,    82,    83,    84,    85,    86,    87,    71,    70,
    88,    72,    73,     0,     0,     0,   106,     0,   104,     0,
     0,   126,   115,     0,    58,    59,    57,     0,    47,     0,
    44,    45,     0,     0,     0,     0,     0,     0,    28,    25,
    29,    27,    26,     0,     0,     0,     0,    31,     0,     0,
   119,   121,   100,     0,   101,     0,     0,   103,     0,    43,
    49,    55,    41,    46,     0,    50,    56,     0,    48,     0,
   111,     0,   114,     0,     0,    32,     0,     0,     0,    60,
    68,     0,   102,   107,   105,   127,     0,     0,     0,     0,
     0,   111,     0,    33,    34,    21,     0,     0,     0,     0,
     0,   122,    42,     0,    53,     0,    51,     0,     0,     0,
     0,     0,     0,     0,     0,     0,    37,     0,    61,     0,
     0,     0,    63,     0,     0,    64,    38,    21,    21,    35,
    36,    54,    52,    69,    62,     0,     0,     0,     0,    40,
    39,    65,     0,     0,     0
};

static const short yydefgoto[] = {   273,
    29,    30,    69,    32,    70,    71,   176,   177,   178,    33,
    34,   160,   161,   162,   163,   164,    35,    36,    37,   111,
    38,    39,    40,    41,   109,   110,    42,    64,    65,    43,
    44,    45,    46
};

static const short yypact[] = {   124,
-32768,-32768,-32768,-32768,-32768,   -48,   -14,   -42,   491,   -38,
   -37,    -7,   -24,   219,   491,   -21,   491,   491,-32768,-32768,
   491,   491,   491,   219,   491,   380,   136,-32768,   240,   310,
-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,   526,
   287,-32768,-32768,-32768,   140,-32768,   491,   -25,   380,   558,
   491,    88,   -18,   491,   219,    47,   590,   491,   191,   191,
   191,   191,   962,   -11,   -19,    47,   832,   280,-32768,   -10,
   380,   491,-32768,-32768,   310,-32768,-32768,-32768,   491,   491,
   491,   491,   491,   491,   491,   491,   491,   491,   491,   491,
   491,   491,   491,   491,   491,-32768,   454,   154,   215,   491,
   491,    40,    -5,    26,    98,     4,-32768,  1014,    -6,     1,
    59,   287,-32768,     2,   858,   280,-32768,   884,   491,-32768,
   491,-32768,-32768,-32768,-32768,   748,    64,  1014,  1040,  1066,
  1075,  1075,  1075,  1075,   242,   242,   242,   242,    82,    82,
    82,   191,   191,    60,   936,   491,    43,   491,    43,    52,
   718,-32768,-32768,    58,-32768,-32768,-32768,   265,-32768,    66,
    98,-32768,   -16,   -12,   120,   417,   491,    81,-32768,-32768,
-32768,-32768,-32768,    83,   122,    87,   100,    99,   380,   380,
  1014,   988,-32768,   491,-32768,   776,   804,-32768,   167,    98,
-32768,-32768,-32768,-32768,   139,-32768,-32768,   147,-32768,   123,
   491,   622,  1014,   149,   152,-32768,   155,   -56,   133,   181,
-32768,   491,  1014,-32768,-32768,-32768,   129,   -29,   -15,   491,
   142,   491,   -32,-32768,-32768,   380,   192,   135,   174,   144,
   380,  1014,-32768,   491,-32768,   491,-32768,   910,   380,   151,
   162,   380,   169,   168,   171,   203,-32768,   212,-32768,   654,
   686,   188,-32768,   380,   224,-32768,-32768,   380,   380,-32768,
-32768,-32768,-32768,-32768,-32768,   197,   196,   198,   380,-32768,
-32768,-32768,   268,   270,-32768
};

static const short yypgoto[] = {-32768,
-32768,   247,     0,   -27,   -45,-32768,  -158,-32768,-32768,   -96,
-32768,    95,-32768,   130,-32768,   143,-32768,-32768,-32768,-32768,
-32768,-32768,    -2,     8,   -99,-32768,   241,   256,-32768,-32768,
-32768,-32768,-32768
};


#define	YYLAST		1131


static const short yytable[] = {    31,
   150,    12,    76,   106,   104,    12,    50,   241,   159,   169,
   226,   234,    57,   197,    59,    60,    47,   227,    61,    62,
    63,    56,    67,    48,    49,   236,    51,    52,    31,    31,
    53,    66,    68,   242,    73,   170,   171,   172,   173,   174,
    54,   105,   235,    58,    63,   152,   114,    76,   108,   120,
   230,   115,   102,   121,   195,   118,   237,   124,   198,   112,
   153,   191,   116,   154,   159,   166,   196,   199,   245,   126,
   125,   165,   175,   167,    31,   168,   127,   128,   129,   130,
   131,   132,   133,   134,   135,   136,   137,   138,   139,   140,
   141,   142,   143,   159,   145,     6,    79,   108,   151,    97,
   184,   221,    98,    99,   100,   147,   149,   100,    14,    98,
    99,   100,    77,    78,    79,    12,   181,   188,   182,   155,
   156,   157,   240,   158,   190,    19,     1,     2,     3,     4,
     5,     6,     7,   193,     8,     9,    94,    95,    10,    11,
   169,    12,    13,   186,    14,   187,   200,    23,    15,    24,
    16,   204,    55,   205,    17,    18,    14,   207,    27,   206,
    14,    19,    20,   202,   203,   208,   170,   171,   172,   173,
   228,   209,   216,    19,    14,    21,   218,    19,   210,   211,
   243,   213,    22,    23,   219,    24,   223,   220,    25,   224,
    26,    19,   225,   231,    27,    28,   233,    24,   108,   169,
    55,    24,    72,   229,    55,   246,    27,   239,   101,   232,
    27,   247,   267,   268,   248,    24,   254,   238,    55,   108,
   146,    77,    78,    79,    27,   170,   171,   172,   173,   244,
   249,   250,   255,   251,   258,    14,   257,   259,   253,    14,
   260,   256,     1,     2,     3,     4,    74,     6,     7,   261,
     8,     9,    19,   265,    10,    11,    19,    12,    13,   264,
    14,   266,   269,   270,    15,   271,    16,   274,   272,   275,
    17,    18,    77,    78,    79,    75,    24,    19,    20,    55,
    24,   148,    12,    55,   217,    27,   155,   156,   157,    27,
   194,    21,   113,    91,    92,    93,    94,    95,    22,    23,
   192,    24,   103,     0,    25,     0,    26,     0,     0,     0,
    27,    28,     1,     2,     3,     4,     0,     6,     7,     0,
     8,     9,     0,     0,    10,    11,     0,    12,    13,     0,
    14,     0,     0,     0,    15,     0,    16,     0,     0,    97,
    17,    18,    98,    99,   100,   123,    97,    19,    20,    98,
    99,   100,     0,     0,     0,     0,     0,     0,     0,     0,
     0,    21,     0,     0,     0,     0,     0,     0,    22,    23,
     0,    24,     0,     0,    25,     0,    26,     0,     0,     0,
    27,    28,     1,     2,     3,     4,     0,     6,     0,     0,
     8,     9,     0,     0,    10,    11,     0,     0,    13,     0,
    14,     0,     0,     0,    15,     0,    16,     0,     0,     0,
    17,    18,     0,     0,     0,     0,     0,    19,    20,     1,
     2,     3,     4,     0,     6,     0,     0,     0,     0,     0,
     0,    21,     0,     0,     0,     0,     0,    14,    22,    23,
     0,    24,     0,     0,    25,     0,    26,    17,    18,     0,
    27,    28,     0,     0,    19,    20,     1,     2,     3,     4,
     0,     6,     0,     0,     0,     0,     0,     0,    21,     0,
     0,     0,     0,     0,    14,    22,    23,     0,    24,     0,
     0,    25,     0,     0,    17,    18,     0,    27,   201,     0,
     0,    19,    20,     1,     2,     3,     4,     0,     6,     0,
     0,     0,     0,     0,     0,    21,     0,     0,     0,     0,
     0,    14,    22,    23,   144,    24,     0,     0,    25,     0,
     0,    17,    18,     0,    27,     0,     0,     0,    19,    20,
     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     0,     0,    21,     0,     0,     0,     0,     0,     0,    22,
    23,     0,    24,     0,     0,    25,    77,    78,    79,     0,
     0,    27,     0,     0,     0,     0,    80,    81,    82,    83,
    84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
    94,    95,     0,     0,     0,     0,     0,     0,    77,    78,
    79,     0,     0,     0,     0,     0,     0,    96,    80,    81,
    82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
    92,    93,    94,    95,     0,     0,     0,     0,     0,     0,
    77,    78,    79,     0,     0,     0,     0,     0,     0,   107,
    80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
    90,    91,    92,    93,    94,    95,     0,     0,     0,     0,
     0,     0,    77,    78,    79,     0,     0,     0,     0,     0,
     0,   117,    80,    81,    82,    83,    84,    85,    86,    87,
    88,    89,    90,    91,    92,    93,    94,    95,     0,     0,
     0,     0,     0,     0,    77,    78,    79,     0,     0,     0,
     0,     0,     0,   222,    80,    81,    82,    83,    84,    85,
    86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
     0,     0,     0,     0,     0,     0,    77,    78,    79,     0,
     0,     0,     0,     0,     0,   262,    80,    81,    82,    83,
    84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
    94,    95,     0,     0,     0,     0,     0,     0,    77,    78,
    79,     0,     0,     0,     0,     0,     0,   263,    80,    81,
    82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
    92,    93,    94,    95,     0,     0,     0,     0,    77,    78,
    79,     0,     0,     0,     0,     0,     0,   189,    80,    81,
    82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
    92,    93,    94,    95,     0,     0,    77,    78,    79,     0,
     0,     0,     0,     0,     0,   183,    80,    81,    82,    83,
    84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
    94,    95,     0,     0,    77,    78,    79,     0,     0,     0,
     0,     0,     0,   214,    80,    81,    82,    83,    84,    85,
    86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
     0,     0,    77,    78,    79,     0,     0,     0,     0,     0,
     0,   215,    80,    81,    82,    83,    84,    85,    86,    87,
    88,    89,    90,    91,    92,    93,    94,    95,    77,    78,
    79,     0,     0,     0,     0,     0,     0,   122,    80,    81,
    82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
    92,    93,    94,    95,    77,    78,    79,     0,     0,     0,
     0,     0,     0,   179,    80,    81,    82,    83,    84,    85,
    86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
    77,    78,    79,     0,     0,     0,     0,     0,     0,   180,
    80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
    90,    91,    92,    93,    94,    95,    77,    78,    79,     0,
     0,     0,     0,     0,     0,   252,    80,    81,    82,    83,
    84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
    94,    95,    77,    78,    79,     0,   185,     0,     0,     0,
     0,   119,    80,    81,    82,    83,    84,    85,    86,    87,
    88,    89,    90,    91,    92,    93,    94,    95,    77,    78,
    79,     0,     0,     0,     0,     0,     0,   212,    80,    81,
    82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
    92,    93,    94,    95,    77,    78,    79,     0,     0,     0,
     0,     0,     0,     0,    80,    81,    82,    83,    84,    85,
    86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
    77,    78,    79,     0,     0,     0,     0,     0,     0,     0,
     0,     0,    82,    83,    84,    85,    86,    87,    88,    89,
    90,    91,    92,    93,    94,    95,    77,    78,    79,     0,
     0,     0,     0,     0,     0,    77,    78,    79,     0,    83,
    84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
    94,    95,    87,    88,    89,    90,    91,    92,    93,    94,
    95
};

static const short yycheck[] = {     0,
   100,    18,    30,    49,    30,    18,     9,    40,   105,     8,
    67,    41,    15,    26,    17,    18,    65,    74,    21,    22,
    23,    14,    25,    38,    67,    41,    65,    65,    29,    30,
    38,    24,    25,    66,    27,    34,    35,    36,    37,    38,
    65,    67,    72,    65,    47,     6,    65,    75,    51,    61,
   209,    54,    45,    73,    71,    58,    72,    68,    71,    52,
    66,   158,    55,    38,   161,    72,   163,   164,   227,    72,
    71,    68,    71,    73,    75,    17,    79,    80,    81,    82,
    83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
    93,    94,    95,   190,    97,     8,    33,   100,   101,    60,
    41,   201,    63,    64,    65,    98,    99,    65,    21,    63,
    64,    65,    31,    32,    33,    18,   119,    66,   121,    22,
    23,    24,   222,    26,    67,    38,     3,     4,     5,     6,
     7,     8,     9,    68,    11,    12,    55,    56,    15,    16,
     8,    18,    19,   146,    21,   148,    27,    60,    25,    62,
    27,    71,    65,    71,    31,    32,    21,    71,    71,    38,
    21,    38,    39,   166,   167,    66,    34,    35,    36,    37,
    38,    73,     6,    38,    21,    52,    38,    38,   179,   180,
   226,   184,    59,    60,    38,    62,    38,    65,    65,    38,
    67,    38,    38,    13,    71,    72,    68,    62,   201,     8,
    65,    62,    67,    71,    65,    71,    71,    66,    69,   212,
    71,    38,   258,   259,    71,    62,    66,   220,    65,   222,
    67,    31,    32,    33,    71,    34,    35,    36,    37,    38,
   231,   234,    71,   236,    67,    21,    68,    67,   239,    21,
    38,   242,     3,     4,     5,     6,     7,     8,     9,    38,
    11,    12,    38,   254,    15,    16,    38,    18,    19,    72,
    21,    38,    66,    68,    25,    68,    27,     0,   269,     0,
    31,    32,    31,    32,    33,    29,    62,    38,    39,    65,
    62,    67,    18,    65,   190,    71,    22,    23,    24,    71,
   161,    52,    52,    52,    53,    54,    55,    56,    59,    60,
   158,    62,    47,    -1,    65,    -1,    67,    -1,    -1,    -1,
    71,    72,     3,     4,     5,     6,    -1,     8,     9,    -1,
    11,    12,    -1,    -1,    15,    16,    -1,    18,    19,    -1,
    21,    -1,    -1,    -1,    25,    -1,    27,    -1,    -1,    60,
    31,    32,    63,    64,    65,    66,    60,    38,    39,    63,
    64,    65,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    52,    -1,    -1,    -1,    -1,    -1,    -1,    59,    60,
    -1,    62,    -1,    -1,    65,    -1,    67,    -1,    -1,    -1,
    71,    72,     3,     4,     5,     6,    -1,     8,    -1,    -1,
    11,    12,    -1,    -1,    15,    16,    -1,    -1,    19,    -1,
    21,    -1,    -1,    -1,    25,    -1,    27,    -1,    -1,    -1,
    31,    32,    -1,    -1,    -1,    -1,    -1,    38,    39,     3,
     4,     5,     6,    -1,     8,    -1,    -1,    -1,    -1,    -1,
    -1,    52,    -1,    -1,    -1,    -1,    -1,    21,    59,    60,
    -1,    62,    -1,    -1,    65,    -1,    67,    31,    32,    -1,
    71,    72,    -1,    -1,    38,    39,     3,     4,     5,     6,
    -1,     8,    -1,    -1,    -1,    -1,    -1,    -1,    52,    -1,
    -1,    -1,    -1,    -1,    21,    59,    60,    -1,    62,    -1,
    -1,    65,    -1,    -1,    31,    32,    -1,    71,    72,    -1,
    -1,    38,    39,     3,     4,     5,     6,    -1,     8,    -1,
    -1,    -1,    -1,    -1,    -1,    52,    -1,    -1,    -1,    -1,
    -1,    21,    59,    60,    61,    62,    -1,    -1,    65,    -1,
    -1,    31,    32,    -1,    71,    -1,    -1,    -1,    38,    39,
    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    52,    -1,    -1,    -1,    -1,    -1,    -1,    59,
    60,    -1,    62,    -1,    -1,    65,    31,    32,    33,    -1,
    -1,    71,    -1,    -1,    -1,    -1,    41,    42,    43,    44,
    45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
    55,    56,    -1,    -1,    -1,    -1,    -1,    -1,    31,    32,
    33,    -1,    -1,    -1,    -1,    -1,    -1,    72,    41,    42,
    43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
    53,    54,    55,    56,    -1,    -1,    -1,    -1,    -1,    -1,
    31,    32,    33,    -1,    -1,    -1,    -1,    -1,    -1,    72,
    41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
    51,    52,    53,    54,    55,    56,    -1,    -1,    -1,    -1,
    -1,    -1,    31,    32,    33,    -1,    -1,    -1,    -1,    -1,
    -1,    72,    41,    42,    43,    44,    45,    46,    47,    48,
    49,    50,    51,    52,    53,    54,    55,    56,    -1,    -1,
    -1,    -1,    -1,    -1,    31,    32,    33,    -1,    -1,    -1,
    -1,    -1,    -1,    72,    41,    42,    43,    44,    45,    46,
    47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
    -1,    -1,    -1,    -1,    -1,    -1,    31,    32,    33,    -1,
    -1,    -1,    -1,    -1,    -1,    72,    41,    42,    43,    44,
    45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
    55,    56,    -1,    -1,    -1,    -1,    -1,    -1,    31,    32,
    33,    -1,    -1,    -1,    -1,    -1,    -1,    72,    41,    42,
    43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
    53,    54,    55,    56,    -1,    -1,    -1,    -1,    31,    32,
    33,    -1,    -1,    -1,    -1,    -1,    -1,    70,    41,    42,
    43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
    53,    54,    55,    56,    -1,    -1,    31,    32,    33,    -1,
    -1,    -1,    -1,    -1,    -1,    68,    41,    42,    43,    44,
    45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
    55,    56,    -1,    -1,    31,    32,    33,    -1,    -1,    -1,
    -1,    -1,    -1,    68,    41,    42,    43,    44,    45,    46,
    47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
    -1,    -1,    31,    32,    33,    -1,    -1,    -1,    -1,    -1,
    -1,    68,    41,    42,    43,    44,    45,    46,    47,    48,
    49,    50,    51,    52,    53,    54,    55,    56,    31,    32,
    33,    -1,    -1,    -1,    -1,    -1,    -1,    66,    41,    42,
    43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
    53,    54,    55,    56,    31,    32,    33,    -1,    -1,    -1,
    -1,    -1,    -1,    66,    41,    42,    43,    44,    45,    46,
    47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
    31,    32,    33,    -1,    -1,    -1,    -1,    -1,    -1,    66,
    41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
    51,    52,    53,    54,    55,    56,    31,    32,    33,    -1,
    -1,    -1,    -1,    -1,    -1,    66,    41,    42,    43,    44,
    45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
    55,    56,    31,    32,    33,    -1,    61,    -1,    -1,    -1,
    -1,    40,    41,    42,    43,    44,    45,    46,    47,    48,
    49,    50,    51,    52,    53,    54,    55,    56,    31,    32,
    33,    -1,    -1,    -1,    -1,    -1,    -1,    40,    41,    42,
    43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
    53,    54,    55,    56,    31,    32,    33,    -1,    -1,    -1,
    -1,    -1,    -1,    -1,    41,    42,    43,    44,    45,    46,
    47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
    31,    32,    33,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    -1,    -1,    43,    44,    45,    46,    47,    48,    49,    50,
    51,    52,    53,    54,    55,    56,    31,    32,    33,    -1,
    -1,    -1,    -1,    -1,    -1,    31,    32,    33,    -1,    44,
    45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
    55,    56,    48,    49,    50,    51,    52,    53,    54,    55,
    56
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "bison.simple"

/* Skeleton output parser for bison,
   Copyright (C) 1984, 1989, 1990 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

#ifndef alloca
#ifdef __GNUC__
#define alloca __builtin_alloca
#else /* not GNU C.  */
#if (!defined (__STDC__) && defined (sparc)) || defined (__sparc__) || defined (__sparc) || defined (__sgi)
#include <alloca.h>
#else /* not sparc */
#if defined (MSDOS) && !defined (__TURBOC__)
#include <malloc.h>
#else /* not MSDOS, or __TURBOC__ */
#if defined(_AIX)
#include <malloc.h>
 #pragma alloca
#else /* not MSDOS, __TURBOC__, or _AIX */
#ifdef __hpux
#ifdef __cplusplus
extern "C" {
void *alloca (unsigned int);
};
#else /* not __cplusplus */
void *alloca ();
#endif /* not __cplusplus */
#endif /* __hpux */
#endif /* not _AIX */
#endif /* not MSDOS, or __TURBOC__ */
#endif /* not sparc.  */
#endif /* not GNU C.  */
#endif /* alloca not defined.  */

/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

/* Note: there must be only one dollar sign in this file.
   It is replaced by the list of actions, each action
   as one case of the switch.  */

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	return(0)
#define YYABORT 	return(1)
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.
   This remains here temporarily to ease the
   transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(token, value) \
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    { yychar = (token), yylval = (value);			\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { yyerror ("syntax error: cannot back up"); YYERROR; }	\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

#ifndef YYPURE
#define YYLEX		yylex()
#endif

#ifdef YYPURE
#ifdef YYLSP_NEEDED
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, &yylloc, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval, &yylloc)
#endif
#else /* not YYLSP_NEEDED */
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval)
#endif
#endif /* not YYLSP_NEEDED */
#endif

/* If nonreentrant, generate the variables here */

#ifndef YYPURE

int	yychar;			/*  the lookahead symbol		*/
YYSTYPE	yylval;			/*  the semantic value of the		*/
				/*  lookahead symbol			*/

#ifdef YYLSP_NEEDED
YYLTYPE yylloc;			/*  location data for the lookahead	*/
				/*  symbol				*/
#endif

int yynerrs;			/*  number of parse errors so far       */
#endif  /* not YYPURE */

#if YYDEBUG != 0
int yydebug;			/*  nonzero means print parse trace	*/
/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif

/*  YYINITDEPTH indicates the initial size of the parser's stacks	*/

#ifndef	YYINITDEPTH
#define YYINITDEPTH 200
#endif

/*  YYMAXDEPTH is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#if YYMAXDEPTH == 0
#undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
int yyparse (void);
#endif

#if __GNUC__ > 1		/* GNU C and GNU C++ define this.  */
#define __yy_memcpy(FROM,TO,COUNT)	__builtin_memcpy(TO,FROM,COUNT)
#else				/* not GNU C or C++ */
#ifndef __cplusplus

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (from, to, count)
     char *from;
     char *to;
     int count;
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#else /* __cplusplus */

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (char *from, char *to, int count)
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#endif
#endif

#line 192 "bison.simple"

/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
#define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
#else
#define YYPARSE_PARAM
#define YYPARSE_PARAM_DECL
#endif

int
yyparse(YYPARSE_PARAM)
     YYPARSE_PARAM_DECL
{
  register int yystate;
  register int yyn;
  register short *yyssp;
  register YYSTYPE *yyvsp;
  int yyerrstatus;	/*  number of tokens to shift before error messages enabled */
  int yychar1 = 0;		/*  lookahead token as an internal (translated) token number */

  short	yyssa[YYINITDEPTH];	/*  the state stack			*/
  YYSTYPE yyvsa[YYINITDEPTH];	/*  the semantic value stack		*/

  short *yyss = yyssa;		/*  refer to the stacks thru separate pointers */
  YYSTYPE *yyvs = yyvsa;	/*  to allow yyoverflow to reallocate them elsewhere */

#ifdef YYLSP_NEEDED
  YYLTYPE yylsa[YYINITDEPTH];	/*  the location stack			*/
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
#define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  int yystacksize = YYINITDEPTH;

#ifdef YYPURE
  int yychar;
  YYSTYPE yylval;
  int yynerrs;
#ifdef YYLSP_NEEDED
  YYLTYPE yylloc;
#endif
#endif

  YYSTYPE yyval;		/*  the variable used to return		*/
				/*  semantic values from the action	*/
				/*  routines				*/

  int yylen;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Starting parse\n");
#endif

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss - 1;
  yyvsp = yyvs;
#ifdef YYLSP_NEEDED
  yylsp = yyls;
#endif

/* Push a new state, which is found in  yystate  .  */
/* In all cases, when you get here, the value and location stacks
   have just been pushed. so pushing a state here evens the stacks.  */
yynewstate:

  *++yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Give user a chance to reallocate the stack */
      /* Use copies of these so that the &'s don't force the real ones into memory. */
      YYSTYPE *yyvs1 = yyvs;
      short *yyss1 = yyss;
#ifdef YYLSP_NEEDED
      YYLTYPE *yyls1 = yyls;
#endif

      /* Get the current used size of the three stacks, in elements.  */
      int size = yyssp - yyss + 1;

#ifdef yyoverflow
      /* Each stack pointer address is followed by the size of
	 the data in use in that stack, in bytes.  */
#ifdef YYLSP_NEEDED
      /* This used to be a conditional around just the two extra args,
	 but that might be undefined if yyoverflow is a macro.  */
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yyls1, size * sizeof (*yylsp),
		 &yystacksize);
#else
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yystacksize);
#endif

      yyss = yyss1; yyvs = yyvs1;
#ifdef YYLSP_NEEDED
      yyls = yyls1;
#endif
#else /* no yyoverflow */
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	{
	  yyerror("parser stack overflow");
	  return 2;
	}
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;
      yyss = (short *) alloca (yystacksize * sizeof (*yyssp));
      __yy_memcpy ((char *)yyss1, (char *)yyss, size * sizeof (*yyssp));
      yyvs = (YYSTYPE *) alloca (yystacksize * sizeof (*yyvsp));
      __yy_memcpy ((char *)yyvs1, (char *)yyvs, size * sizeof (*yyvsp));
#ifdef YYLSP_NEEDED
      yyls = (YYLTYPE *) alloca (yystacksize * sizeof (*yylsp));
      __yy_memcpy ((char *)yyls1, (char *)yyls, size * sizeof (*yylsp));
#endif
#endif /* no yyoverflow */

      yyssp = yyss + size - 1;
      yyvsp = yyvs + size - 1;
#ifdef YYLSP_NEEDED
      yylsp = yyls + size - 1;
#endif

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Stack size increased to %d\n", yystacksize);
#endif

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Entering state %d\n", yystate);
#endif

  goto yybackup;
 yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Reading a token: ");
#endif
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Now at end of input.\n");
#endif
    }
  else
    {
      yychar1 = YYTRANSLATE(yychar);

#if YYDEBUG != 0
      if (yydebug)
	{
	  fprintf (stderr, "Next token is %d (%s", yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise meaning
	     of a token, for further debugging info.  */
#ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
#endif
	  fprintf (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting token %d (%s), ", yychar, yytname[yychar1]);
#endif

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* count tokens shifted since error; after three, turn off error status.  */
  if (yyerrstatus) yyerrstatus--;

  yystate = yyn;
  goto yynewstate;

/* Do the default action for the current state.  */
yydefault:

  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;

/* Do a reduction.  yyn is the number of a rule to reduce with.  */
yyreduce:
  yylen = yyr2[yyn];
  if (yylen > 0)
    yyval = yyvsp[1-yylen]; /* implement default value of the action */

#if YYDEBUG != 0
  if (yydebug)
    {
      int i;

      fprintf (stderr, "Reducing via rule %d (line %d), ",
	       yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (i = yyprhs[yyn]; yyrhs[i] > 0; i++)
	fprintf (stderr, "%s ", yytname[yyrhs[i]]);
      fprintf (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif


  switch (yyn) {

case 1:
#line 141 "php.y"
{yyval.prgm = _yyprg = createProgram(yyvsp[0].htmltext); ;
    break;}
case 2:
#line 144 "php.y"
{yyval.htmltext = createHtmlText(NULL,yyvsp[0].statementsList);;
    break;}
case 3:
#line 145 "php.y"
{yyval.htmltext = createHtmlText(yyvsp[0].statichtml,NULL);;
    break;}
case 4:
#line 146 "php.y"
{yyval.htmltext = addToHtmlText(yyvsp[-1].htmltext,NULL,yyvsp[0].statementsList);;
    break;}
case 5:
#line 147 "php.y"
{yyval.htmltext = addToHtmlText(yyvsp[-1].htmltext,yyvsp[0].statichtml,NULL);;
    break;}
case 6:
#line 150 "php.y"
{yyval.statementsList = createStatementsList(yyvsp[0].statement);;
    break;}
case 7:
#line 151 "php.y"
{yyval.statementsList = addStatementToList(yyvsp[-1].statementsList,yyvsp[0].statement);;
    break;}
case 8:
#line 155 "php.y"
{yyval.statement = NULL;;
    break;}
case 9:
#line 156 "php.y"
{yyval.statement = createStatement(_EXPR,(void*)yyvsp[-1].expression);   ;
    break;}
case 10:
#line 157 "php.y"
{yyval.statement = createStatement(_IF,(void*)yyvsp[0].ifStatement);   ;
    break;}
case 11:
#line 158 "php.y"
{yyval.statement = createStatement(_FOR,(void*)yyvsp[0].forStatement);   ;
    break;}
case 12:
#line 159 "php.y"
{yyval.statement = createStatement(_WHILEDO,(void*)yyvsp[0].whileDoStatement);   ;
    break;}
case 13:
#line 160 "php.y"
{yyval.statement = createStatement(_DOWHILE,(void*)yyvsp[0].doWhileStatement);   ;
    break;}
case 14:
#line 161 "php.y"
{yyval.statement = createStatement(_FOREACH,(void*)yyvsp[0].forEachStatement);   ;
    break;}
case 15:
#line 162 "php.y"
{yyval.statement = createStatement(_ECHO,(void*)yyvsp[0].expression);   ;
    break;}
case 16:
#line 163 "php.y"
{yyval.statement = createStatement(_RETURN,(void*)yyvsp[0].expression);   ;
    break;}
case 17:
#line 164 "php.y"
{yyval.statement = createStatement(_STMTLIST,(void*)yyvsp[-1].statementsList);   ;
    break;}
case 18:
#line 167 "php.y"
{yyval.statement = yyvsp[0].statement;
    break;}
case 19:
#line 168 "php.y"
{yyval.statement = createStatement(_FUNCTION,(void*)yyvsp[0].functionStatement);   ;
    break;}
case 20:
#line 169 "php.y"
{yyval.statement = createStatement(_CLASS,(void*)yyvsp[0].classStatement);   ;
    break;}
case 21:
#line 172 "php.y"
{yyval.statementsList = NULL;;
    break;}
case 22:
#line 173 "php.y"
{yyval.statementsList = yyvsp[0].statementsList;;
    break;}
case 23:
#line 176 "php.y"
{yyval.statementsList = createStatementsList(yyvsp[0].statement);;
    break;}
case 24:
#line 177 "php.y"
{yyval.statementsList = addStatementToList(yyvsp[-1].statementsList,yyvsp[0].statement);;
    break;}
case 25:
#line 180 "php.y"
{yyval.valuetype = _INTEGER;;
    break;}
case 26:
#line 181 "php.y"
{yyval.valuetype = _DOUBLE;;
    break;}
case 27:
#line 182 "php.y"
{yyval.valuetype = _STRING;;
    break;}
case 28:
#line 183 "php.y"
{yyval.valuetype = _ARRAY;;
    break;}
case 29:
#line 184 "php.y"
{yyval.valuetype = _BOOLEAN;;
    break;}
case 30:
#line 187 "php.y"
{yyval.formalFunctionArglist = NULL;;
    break;}
case 31:
#line 188 "php.y"
{yyval.formalFunctionArglist = yyvsp[0].formalFunctionArglist;;
    break;}
case 32:
#line 191 "php.y"
{yyval.formalFunctionArglist = createFormalFunctionArglist(createFunctionArg(yyvsp[0].id,_NONE,NULL));;
    break;}
case 33:
#line 192 "php.y"
{yyval.formalFunctionArglist = createFormalFunctionArglist(createFunctionArg(yyvsp[0].id,_USEROBJECT,yyvsp[-2].id));;
    break;}
case 34:
#line 193 "php.y"
{yyval.formalFunctionArglist = createFormalFunctionArglist(createFunctionArg(yyvsp[0].id,yyvsp[-2].valuetype,NULL));;
    break;}
case 35:
#line 194 "php.y"
{yyval.formalFunctionArglist = addToFormalFunctionArglist(yyvsp[-4].formalFunctionArglist,createFunctionArg(yyvsp[0].id,_USEROBJECT,yyvsp[-2].id));;
    break;}
case 36:
#line 195 "php.y"
{yyval.formalFunctionArglist = addToFormalFunctionArglist(yyvsp[-4].formalFunctionArglist,createFunctionArg(yyvsp[0].id,yyvsp[-2].valuetype,NULL));;
    break;}
case 37:
#line 196 "php.y"
{yyval.formalFunctionArglist = addToFormalFunctionArglist(yyvsp[-3].formalFunctionArglist,createFunctionArg(yyvsp[0].id,_NONE,NULL));;
    break;}
case 38:
#line 198 "php.y"
{yyval.functionStatement = createFunctionStatement(yyvsp[-6].id,0,1,_PUBLIC,yyvsp[-4].formalFunctionArglist,_NONE,NULL,yyvsp[-1].statementsList);;
    break;}
case 39:
#line 199 "php.y"
{yyval.functionStatement = createFunctionStatement(yyvsp[-8].id,0,1,_PUBLIC,yyvsp[-6].formalFunctionArglist,yyvsp[-3].valuetype,NULL,yyvsp[-1].statementsList);;
    break;}
case 40:
#line 200 "php.y"
{yyval.functionStatement = createFunctionStatement(yyvsp[-8].id,0,1,_PUBLIC,yyvsp[-6].formalFunctionArglist,_USEROBJECT,yyvsp[-3].id,yyvsp[-1].statementsList);;
    break;}
case 41:
#line 203 "php.y"
{ yyval.classStatement = createClassStatement(yyvsp[-3].id,NULL,yyvsp[-1].classMemberList); ;
    break;}
case 42:
#line 204 "php.y"
{ yyval.classStatement = createClassStatement(yyvsp[-5].id,yyvsp[-3].id,yyvsp[-1].classMemberList); ;
    break;}
case 43:
#line 207 "php.y"
{yyval.classMemberList = NULL;;
    break;}
case 44:
#line 208 "php.y"
{yyval.classMemberList = yyvsp[0].classMemberList;;
    break;}
case 45:
#line 211 "php.y"
{yyval.classMemberList = createClassMemberList(yyvsp[0].classMemberStatement);;
    break;}
case 46:
#line 212 "php.y"
{yyval.classMemberList = addToClassMemberList(yyvsp[-1].classMemberList,yyvsp[0].classMemberStatement);;
    break;}
case 47:
#line 214 "php.y"
{ yyval.classMemberStatement = createClassFunctionStatement(yyvsp[0].functionStatement,0,_PUBLIC);;
    break;}
case 48:
#line 215 "php.y"
{ yyval.classMemberStatement = createClassFunctionStatement(yyvsp[0].functionStatement,0,yyvsp[-1].visibilitytype);;
    break;}
case 49:
#line 216 "php.y"
{ yyval.classMemberStatement = createClassFunctionStatement(yyvsp[0].functionStatement,1,_PUBLIC);;
    break;}
case 50:
#line 217 "php.y"
{ yyval.classMemberStatement = createClassFunctionStatement(yyvsp[0].functionStatement,1,yyvsp[-1].visibilitytype);;
    break;}
case 51:
#line 218 "php.y"
{ yyval.classMemberStatement = createClassPropertyStatement(yyvsp[-1].id,0,yyvsp[-3].visibilitytype,createExpression(_NIL,NULL,NULL));;
    break;}
case 52:
#line 219 "php.y"
{ yyval.classMemberStatement = createClassPropertyStatement(yyvsp[-3].id,0,yyvsp[-5].visibilitytype,yyvsp[-1].expression);;
    break;}
case 53:
#line 220 "php.y"
{ yyval.classMemberStatement = createClassPropertyStatement(yyvsp[-1].id,1,yyvsp[-3].visibilitytype,createExpression(_NIL,NULL,NULL));;
    break;}
case 54:
#line 221 "php.y"
{ yyval.classMemberStatement = createClassPropertyStatement(yyvsp[-3].id,1,yyvsp[-5].visibilitytype,yyvsp[-1].expression);;
    break;}
case 55:
#line 224 "php.y"
{yyval.visibilitytype = yyvsp[0].visibilitytype;;
    break;}
case 56:
#line 225 "php.y"
{yyval.visibilitytype = yyvsp[-1].visibilitytype;;
    break;}
case 57:
#line 228 "php.y"
{yyval.visibilitytype = _PUBLIC;;
    break;}
case 58:
#line 229 "php.y"
{yyval.visibilitytype = _PRIVATE;;
    break;}
case 59:
#line 230 "php.y"
{yyval.visibilitytype = _PROTECTED;;
    break;}
case 60:
#line 233 "php.y"
{yyval.ifStatement = createIfStatement(yyvsp[-2].expression,yyvsp[0].statement,NULL);;
    break;}
case 61:
#line 234 "php.y"
{yyval.ifStatement = createIfStatement(yyvsp[-4].expression,yyvsp[-2].statement,yyvsp[0].statement);;
    break;}
case 62:
#line 237 "php.y"
{yyval.forStatement = createForStatement(yyvsp[-6].expressionList,yyvsp[-4].expression,yyvsp[-2].expressionList,yyvsp[0].statement);;
    break;}
case 63:
#line 238 "php.y"
{yyval.forStatement = createForStatement(yyvsp[-5].expressionList,NULL,yyvsp[-2].expressionList,yyvsp[0].statement);;
    break;}
case 64:
#line 241 "php.y"
{yyval.forEachStatement = createForEachStatement(yyvsp[-5].expression,NULL,createExpression(_DOLLAR,NULL,createIdExpression(yyvsp[-2].id)),yyvsp[0].statement);;
    break;}
case 65:
#line 242 "php.y"
{yyval.forEachStatement = createForEachStatement(yyvsp[-8].expression,createExpression(_DOLLAR,NULL,createIdExpression(yyvsp[-5].id)),createExpression(_DOLLAR,NULL,createIdExpression(yyvsp[-2].id)),yyvsp[0].statement);;
    break;}
case 66:
#line 245 "php.y"
{yyval.expression = createArrayExpression(yyvsp[0].arr); ;
    break;}
case 67:
#line 246 "php.y"
{ yyval.expression = yyvsp[0].expression; ;
    break;}
case 68:
#line 249 "php.y"
{yyval.whileDoStatement = createWhileDoStatement(yyvsp[-2].expression,yyvsp[0].statement);;
    break;}
case 69:
#line 252 "php.y"
{yyval.doWhileStatement = createDoWhileStatement(yyvsp[-2].expression,yyvsp[-6].statementsList);;
    break;}
case 70:
#line 255 "php.y"
{yyval.expression = createExpression(_ADD,yyvsp[-2].expression,yyvsp[0].expression);;
    break;}
case 71:
#line 256 "php.y"
{yyval.expression = createExpression(_SUB,yyvsp[-2].expression,yyvsp[0].expression);;
    break;}
case 72:
#line 257 "php.y"
{yyval.expression = createExpression(_MUL,yyvsp[-2].expression,yyvsp[0].expression);;
    break;}
case 73:
#line 258 "php.y"
{yyval.expression = createExpression(_DIVISION,yyvsp[-2].expression,yyvsp[0].expression);;
    break;}
case 74:
#line 259 "php.y"
{yyval.expression = createExpression(_POW,yyvsp[-2].expression,yyvsp[0].expression);;
    break;}
case 75:
#line 260 "php.y"
{yyval.expression = yyvsp[-1].expression;;
    break;}
case 76:
#line 261 "php.y"
{yyval.expression = createExpression(_UMINUS,NULL,yyvsp[0].expression);;
    break;}
case 77:
#line 262 "php.y"
{yyval.expression = createExpression(_ASSIGN,yyvsp[-2].expression,yyvsp[0].expression);;
    break;}
case 78:
#line 263 "php.y"
{yyval.expression = createExpression(_OR,yyvsp[-2].expression,yyvsp[0].expression);;
    break;}
case 79:
#line 264 "php.y"
{yyval.expression = createExpression(_AND,yyvsp[-2].expression,yyvsp[0].expression);;
    break;}
case 80:
#line 265 "php.y"
{yyval.expression = createExpression(_EQUAL,yyvsp[-2].expression,yyvsp[0].expression);;
    break;}
case 81:
#line 266 "php.y"
{yyval.expression = createExpression(_NONEEQUAL,yyvsp[-2].expression,yyvsp[0].expression);;
    break;}
case 82:
#line 267 "php.y"
{yyval.expression = createExpression(_EQUAL_BY_TYPE,yyvsp[-2].expression,yyvsp[0].expression);;
    break;}
case 83:
#line 268 "php.y"
{yyval.expression = createExpression(_NONEQUAL_BY_TYPE,yyvsp[-2].expression,yyvsp[0].expression);;
    break;}
case 84:
#line 269 "php.y"
{yyval.expression = createExpression(_LESS,yyvsp[-2].expression,yyvsp[0].expression);;
    break;}
case 85:
#line 270 "php.y"
{yyval.expression = createExpression(_LESS_OR_EQUAL,yyvsp[-2].expression,yyvsp[0].expression);;
    break;}
case 86:
#line 271 "php.y"
{yyval.expression = createExpression(_LARGER,yyvsp[-2].expression,yyvsp[0].expression);;
    break;}
case 87:
#line 272 "php.y"
{yyval.expression = createExpression(_LARGER_OR_EQUAL,yyvsp[-2].expression,yyvsp[0].expression);;
    break;}
case 88:
#line 273 "php.y"
{yyval.expression = createExpression(_CONCAT,yyvsp[-2].expression,yyvsp[0].expression);;
    break;}
case 89:
#line 274 "php.y"
{yyval.expression = createExpression(_NOT,NULL,yyvsp[0].expression);;
    break;}
case 90:
#line 275 "php.y"
{yyval.expression = createExpression(_INC,NULL,yyvsp[0].expression);;
    break;}
case 91:
#line 276 "php.y"
{yyval.expression = createExpression(_INC,yyvsp[-1].expression,NULL);;
    break;}
case 92:
#line 277 "php.y"
{yyval.expression = createExpression(_DEC,NULL,yyvsp[0].expression);;
    break;}
case 93:
#line 278 "php.y"
{yyval.expression = createExpression(_DEC,yyvsp[-1].expression,NULL);;
    break;}
case 94:
#line 279 "php.y"
{ if ((yyvsp[0].expression)->type==_ID)  { yyerror("parce error"); YYABORT;}  yyval.expression = yyvsp[0].expression;;
    break;}
case 95:
#line 280 "php.y"
{ yyval.expression = yyvsp[0].expression;;
    break;}
case 96:
#line 281 "php.y"
{ yyval.expression = createArrayExpression(yyvsp[0].arr);;
    break;}
case 97:
#line 282 "php.y"
{ yyval.expression = createExpression(_NIL,NULL,NULL);;
    break;}
case 98:
#line 285 "php.y"
{yyval.expression = createIdExpression(yyvsp[0].id);;
    break;}
case 99:
#line 286 "php.y"
{yyval.expression = createExpression(_DOLLAR,NULL,yyvsp[0].expression);;
    break;}
case 100:
#line 287 "php.y"
{yyval.expression = createExpression(_DOLLAR,NULL,yyvsp[-1].expression);;
    break;}
case 101:
#line 288 "php.y"
{if ((yyvsp[-3].expression)->type==_ID) { yyerror("parce error"); YYABORT;} yyval.expression = createExpression(_SQ_BRACKET,yyvsp[-3].expression,yyvsp[-1].expression);;
    break;}
case 102:
#line 289 "php.y"
{yyval.expression = createExpression(_ARRAY_PUSHBACK,yyvsp[-4].expression,yyvsp[0].expression);;
    break;}
case 103:
#line 290 "php.y"
{yyval.expression = createFuncCallExpression(yyvsp[-3].expression,yyvsp[-1].expressionList);;
    break;}
case 104:
#line 291 "php.y"
{if ((yyvsp[-2].expression)->type==_ID)  { yyerror("parce error"); YYABORT;} yyval.expression = createExpression(_ARROW,yyvsp[-2].expression,yyvsp[0].expression);;
    break;}
case 105:
#line 292 "php.y"
{if ((yyvsp[-4].expression)->type==_ID)  { yyerror("parce error"); YYABORT;} yyval.expression = createExpression(_ARROW,yyvsp[-4].expression,yyvsp[-1].expression);;
    break;}
case 106:
#line 293 "php.y"
{ if ((yyvsp[-2].expression)->type == _NEWOBJ)  { yyerror("parce error"); YYABORT;}  yyval.expression = createExpression(_DCOLON,yyvsp[-2].expression,yyvsp[0].expression); ;
    break;}
case 107:
#line 294 "php.y"
{ if ((yyvsp[-4].expression)->type == _NEWOBJ)  { yyerror("parce error"); YYABORT;}  yyval.expression = createExpression(_DCOLON,yyvsp[-4].expression,yyvsp[-1].expression);;
    break;}
case 108:
#line 295 "php.y"
{ yyval.expression = createNewObjExpressionFromFuncCall(yyvsp[0].expression); ;
    break;}
case 109:
#line 296 "php.y"
{ if ((yyvsp[0].expression)->type == _SQ_BRACKET || (yyvsp[0].expression)->type == _FUNCTION || (yyvsp[0].expression)->type == _ID)  { yyerror("parce error"); YYABORT;}  yyval.expression = createExpression(_CLONE,NULL,yyvsp[0].expression); ;
    break;}
case 110:
#line 297 "php.y"
{yyval.expression = yyvsp[-1].expression;;
    break;}
case 111:
#line 302 "php.y"
{yyval.expressionList = NULL;;
    break;}
case 112:
#line 303 "php.y"
{yyval.expressionList = yyvsp[0].expressionList;;
    break;}
case 113:
#line 306 "php.y"
{yyval.expressionList = createExpressionList(yyvsp[0].expression);;
    break;}
case 114:
#line 307 "php.y"
{yyval.expressionList = addToExpressionList(yyvsp[-2].expressionList,yyvsp[0].expression);;
    break;}
case 115:
#line 310 "php.y"
{yyval.arr = yyvsp[-1].arr;;
    break;}
case 116:
#line 311 "php.y"
{yyval.arr = yyvsp[-1].arr;;
    break;}
case 117:
#line 314 "php.y"
{yyval.arr = NULL;;
    break;}
case 118:
#line 315 "php.y"
{yyval.arr = yyvsp[0].arr;;
    break;}
case 119:
#line 318 "php.y"
{yyval.arr = createArray(yyvsp[-2].expression,yyvsp[0].expression);;
    break;}
case 120:
#line 319 "php.y"
{yyval.arr = createArray(NULL,yyvsp[0].expression);;
    break;}
case 121:
#line 320 "php.y"
{yyval.arr = addToArray(yyvsp[-2].arr,NULL,yyvsp[0].expression);;
    break;}
case 122:
#line 321 "php.y"
{yyval.arr = addToArray(yyvsp[-4].arr,yyvsp[-2].expression,yyvsp[0].expression);;
    break;}
case 123:
#line 324 "php.y"
{yyval.expression = yyvsp[-1].expression;;
    break;}
case 124:
#line 327 "php.y"
{yyval.expression = yyvsp[-1].expression; ;
    break;}
case 125:
#line 330 "php.y"
{yyval.expression = createStringExpression(yyvsp[0].string_const);;
    break;}
case 126:
#line 331 "php.y"
{yyval.expression = createExpression(_CONCAT,createExpression(_CONCAT,yyvsp[-2].expression,yyvsp[-1].expression),createStringExpression(yyvsp[0].string_const));;
    break;}
case 127:
#line 332 "php.y"
{yyval.expression = createExpression(_CONCAT,createExpression(_CONCAT,yyvsp[-4].expression,yyvsp[-2].expression),createStringExpression(yyvsp[0].string_const));;
    break;}
case 128:
#line 335 "php.y"
{yyval.expression = createIntExpression(yyvsp[0].int_const);;
    break;}
case 129:
#line 336 "php.y"
{yyval.expression = createDoubleExpression(yyvsp[0].double_const);;
    break;}
case 130:
#line 337 "php.y"
{yyval.expression = yyvsp[0].expression;;
    break;}
case 131:
#line 338 "php.y"
{yyval.expression = createBooleanExpression(yyvsp[0].int_const);;
    break;}
}
   /* the action file gets copied in in place of this dollarsign */
#line 487 "bison.simple"

  yyvsp -= yylen;
  yyssp -= yylen;
#ifdef YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;

#ifdef YYLSP_NEEDED
  yylsp++;
  if (yylen == 0)
    {
      yylsp->first_line = yylloc.first_line;
      yylsp->first_column = yylloc.first_column;
      yylsp->last_line = (yylsp-1)->last_line;
      yylsp->last_column = (yylsp-1)->last_column;
      yylsp->text = 0;
    }
  else
    {
      yylsp->last_line = (yylsp+yylen-1)->last_line;
      yylsp->last_column = (yylsp+yylen-1)->last_column;
    }
#endif

  /* Now "shift" the result of the reduction.
     Determine what state that goes to,
     based on the state we popped back to
     and the rule number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;

yyerrlab:   /* here on detecting error */

  if (! yyerrstatus)
    /* If not already recovering from an error, report this error.  */
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  int size = 0;
	  char *msg;
	  int x, count;

	  count = 0;
	  /* Start X at -yyn if nec to avoid negative indexes in yycheck.  */
	  for (x = (yyn < 0 ? -yyn : 0);
	       x < (sizeof(yytname) / sizeof(char *)); x++)
	    if (yycheck[x + yyn] == x)
	      size += strlen(yytname[x]) + 15, count++;
	  msg = (char *) malloc(size + 15);
	  if (msg != 0)
	    {
	      strcpy(msg, "parse error");

	      if (count < 5)
		{
		  count = 0;
		  for (x = (yyn < 0 ? -yyn : 0);
		       x < (sizeof(yytname) / sizeof(char *)); x++)
		    if (yycheck[x + yyn] == x)
		      {
			strcat(msg, count == 0 ? ", expecting `" : " or `");
			strcat(msg, yytname[x]);
			strcat(msg, "'");
			count++;
		      }
		}
	      yyerror(msg);
	      free(msg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exceeded");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror("parse error");
    }

  goto yyerrlab1;
yyerrlab1:   /* here on error raised explicitly by an action */

  if (yyerrstatus == 3)
    {
      /* if just tried and failed to reuse lookahead token after an error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Discarding token %d (%s).\n", yychar, yytname[yychar1]);
#endif

      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token
     after shifting the error token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;

yyerrdefault:  /* current state does not do anything special for the error token. */

#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */
  yyn = yydefact[yystate];  /* If its default is to accept any token, ok.  Otherwise pop it.*/
  if (yyn) goto yydefault;
#endif

yyerrpop:   /* pop the current state because it cannot handle the error token */

  if (yyssp == yyss) YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#ifdef YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "Error: state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

yyerrhandle:

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting error token, ");
#endif

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;
}
#line 341 "php.y"

void yyerror(char const *s)
{
	printf("%s",s);
}

/*
yywrap()
{
    return(1);
}
*/
