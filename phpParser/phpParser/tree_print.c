#include "tree_print.h"

void printNode(int id, char * label)
{
	nodename[0]='\0';
	itoa(id,nodename,10);
	strcat(nodename," [label=\"");
	strcat(nodename,label);
	strcat(nodename,"\"];\n");
	fputs(nodename,dotfile);
}

void printTransition(int prev, int next)
{
	nodename[0]='\0';
	itoa(prev,nodename,10);
	strcat(nodename," -> ");
	itoa(next,nodename + strlen(nodename),10);
	strcat(nodename,";\n");
	fputs(nodename,dotfile);
}

int print(int prev, char * label)
{
	printNode(++nodeid,label);
	printTransition(prev,nodeid);
	return nodeid;
}

void printFromTo(int prev,int next, char * label)
{
	printNode(next,label);
	printTransition(prev,next);
}

void printProgram(struct Program * prg)
{
	int programNode = 0;

	nodeid = -1;
	dotfile = fopen("result.dot","w");
	
	fputs("digraph g{\n",dotfile);	
	if (prg!=NULL)
	{
		printNode(++nodeid,"Program");
		printHtmlText(prg->stmts,programNode);
	}
	fputs("}",dotfile);
	fclose(dotfile);
}


void printHtmlText(struct HtmlText * html, int previd)
{
	int node = ++nodeid;	
	printFromTo(previd,node,"HtmlText");
	
	while (html!=NULL)
	{
		if (html->html)
		{
			print(node,"Html");
			print(nodeid,html->html);
		}
		else
		{
			printStatementsList(html->stmts,node);
		}

		html=html->next;
	}
}

void printStatementsList(struct StatementsList *stmts,int previd)
{
	int node = ++nodeid;
	
	printFromTo(previd,node,"StmtList");
		
	while (stmts!=NULL)
	{
		printStatement(stmts->stmt,node);
		stmts= stmts->next;
	}
}

void printStatement(struct Statement *stmt,int previd)
{
	if (stmt == NULL)
		return;

	switch(stmt->code)
	{
	case _EXPR:
		printExpression(stmt->stmt.exprStmt,previd);
		break;
	case _IF:
		printIfStatement(stmt->stmt.ifStmt,previd);
		break;
	case _FOR:
		printForStatement(stmt->stmt.forStmt,previd);
		break;
	case _FOREACH:
		printForEachStatement(stmt->stmt.foreachStmt,previd);
		break;
	case _WHILEDO:
		printWhileDoStatement(stmt->stmt.whiledoStmt,previd,"WhileDoStmt");
		break;
	case _DOWHILE:
		printWhileDoStatement(stmt->stmt.dowhileStmt,previd,"DoWhileStmt");
		break;
	case _ECHO:
		print(previd,"EchoStmt");
		printExpression(stmt->stmt.echoStmt,nodeid);
		break;
	case _FUNCTION:
		printFunctionStatement(stmt->stmt.funcStmt,previd);
		break;
	case _CLASS:
		printClassStatement(stmt->stmt.classStmt,previd);
		break;
	case _RETURN:
		print(previd,"ReturnStmt");
		printExpression(stmt->stmt.echoStmt,nodeid);
		break;
	case _STMTLIST:
		printStatementsList(stmt->stmt.stmtlist,previd);
		break;
	}
}

void printIfStatement(struct IfStatement * stmt, int previd)
{
	int node = ++nodeid;
	
	printFromTo(previd,node,"IfStmt");

	printExpression(stmt->expr,node);
	printStatementsList(stmt->thenlist,node);
	printStatementsList(stmt->elselist,node);
}

void printForStatement(struct ForStatement * stmt, int previd)
{
	int node = ++nodeid;
	
	printFromTo(previd,node,"ForStmt");

	printExpressionList(stmt->first,node);	
	if (stmt->second)
	{
		printExpression(stmt->second,node);
	}	
	printExpressionList(stmt->third,node);

	printStatementsList(stmt->stmts,node);
}

void printForEachStatement(struct ForEachStatement * stmt, int previd)
{
	int node = ++nodeid;
	
	printFromTo(previd,node,"ForEachStmt");

	print(node,"arr");
	printExpression(stmt->arr,nodeid);

	if (stmt->key != NULL)
	{
		print(node,"key");
		printExpression(stmt->key,nodeid);
	}

	print(node,"value");
	printExpression(stmt->value,nodeid);

	printStatementsList(stmt->stmts,node);
}

void printWhileDoStatement(struct WhileDoStatement* stmt, int previd, char * cycle)
{
	int node = ++nodeid;
	
	printFromTo(previd,node,cycle);

	printExpression(stmt->expr,node);
	printStatementsList(stmt->stmts,node);
}

void printFunctionStatement(struct FunctionStatement* stmt, int previd)
{
	int node = ++nodeid;
	
	printFromTo(previd,node,"FuncStmt");

	if (stmt->isMethod)
	{
		printVisibility(stmt->visibility,node);
		if (stmt->isStatic)
		{
			print(node,"static");
		}

	}
	printType(stmt->returncode,node,stmt->returnObjectId);
	print(node,stmt->id);
	printFormalFunctionArglist(stmt->arglist,node);
	printStatementsList(stmt->stmts,node);
}

void printFormalFunctionArglist(struct FormalFunctionArglist * arglist,int previd)
{
	int node = ++nodeid;
	
	printFromTo(previd,node,"FormalFuncArglist");

	while(arglist != NULL)
	{
		printFunctionArg(arglist->arg,node);
		arglist = arglist->next;
	}
}

void printFunctionArg(struct FunctionArg * arg, int previd)
{
	int node = ++nodeid;
	
	printFromTo(previd,node,"arg");

	printType(arg->vartype,node,arg->userobjectname);
	print(node,arg->id);
}

void printClassStatement(struct ClassStatement * stmt,int previd)
{
	int node = ++nodeid;
	printFromTo(previd,node,"ClassStmt");

	print(node,stmt->id);

	if (stmt->parentID!=NULL)
	{
		print(node,"extends");
		print(node,stmt->parentID);
	}

	printClassMemberList(stmt->members,node);
}

void printClassMemberList(struct ClassMemberList * list, int previd)
{
	int node = ++nodeid;
	printFromTo(previd,node,"ClassMembers");

	while (list!= NULL)
	{
		printClassMemberStatement(list->stmt,node);
		list = list->next;
	}
}

void printClassMemberStatement(struct ClassMemberStatement * stmt, int previd)
{
	int node = ++nodeid;

	if (stmt->prt == NULL)
	{
		printFromTo(previd,node,"Method");
		printFunctionStatement(stmt->function,node);
	}
	else
	{
		printFromTo(previd,node,"Property");

		printVisibility(stmt->prt->visibility,node);
		if (stmt->prt->isStatic)
		{
			print(node,"static");
		}
		print(node,stmt->prt->id);
		if (stmt->prt->value != NULL)
		{
			print(node,"=");
			printExpression(stmt->prt->value,node);
		}
	}
}

void printVisibility(int visibility,int previd)
{
	char str[10];
	switch(visibility)
	{
	case _PUBLIC:
		strcpy(str,"public");
		break;
	case _PRIVATE:
		strcpy(str,"private");
		break;
	case _PROTECTED:
		strcpy(str,"protected");
		break;
	default:
		return;
	}

	print(previd,str);
}

void printType(int type,int previd,char * objtype)
{
	char str[50];

	switch(type)
	{
	case _INTEGER:
		strcpy(str,"integer");
		break;
	case _DOUBLE:
		strcpy(str,"double");
		break;
	case _STRING:
		strcpy(str,"string");
		break;
	case _ARRAY:
		strcpy(str,"array");
		break;
	case _BOOLEAN:
		strcpy(str,"bool");
		break;
	case _USEROBJECT:
		strcpy(str,objtype);
		break;
	default:
		return;
	}

	print(previd,str);
}

void printExpression(struct Expression * expr, int previd)
{
	int node = ++nodeid;
	char buf[30] = "Expr,ind = ";
	char in[10];
	strcat(buf,itoa(expr->constIndex,in,10));
	printFromTo(previd,node,buf);
	
	if (expr->left != NULL)
	{
		printExpression(expr->left,node);
	}

	if (expr->center != NULL)
	{
		printExpression(expr->center,node);
	}

	printExprOperation(expr,node);
	if (expr->right != NULL)
	{
		printExpression(expr->right,node);
	}
}

void printExprOperation(struct Expression * expr, int previd)
{
	static char buffer[30]="";

	switch (expr->type)
	{
	case _DOLLAR:
		print(previd,"$");
		break;
	case _UMINUS:
	case _SUB:
		print(previd,"-");
		break;
	case _INC:
		print(previd,"++");
		break;
	case _INC_DOLLAR:
		print(previd,"++$");
		break;
	case _DOLLAR_INC:
		print(previd,"$++");
		break;
	case _DEC:
		print(previd,"--");
		break;
	case _DEC_DOLLAR:
		print(previd,"--$");
		break;
	case _DOLLAR_DEC:
		print(previd,"$--");
		break;
	case _ADD:
		print(previd,"+");
		break;
	case _MUL:
		print(previd,"*");
		break;
	case _DIVISION:
		print(previd,"/");
		break;
	case _MOD:
		print(previd,"%");
		break;
	case _NOT:
		print(previd,"!");
		break;
	case _AND:
		print(previd,"&&");
		break;
	case _OR:
		print(previd,"||");
		break;
	case _ASSIGN:
		print(previd,"=");
		break;
	case _SQ_BRACKET_ASSIGN:
		print(previd,"[]=");
		break;
	case _DCOLON_ASSIGN:
		print(previd,"::=");
		break;
	case _ARROW_ASSIGN:
		print(previd,"->=");
		break;
	case _DOLLAR_ASSIGN:
		print(previd,"$=");
		break;
	case _POW:
		print(previd,"**");
		break;
	case _EQUAL:
		print(previd,"==");
		break;
	case _NONEEQUAL:
		print(previd,"!=");
		break;
	case _EQUAL_BY_TYPE:
		print(previd,"===");
		break;
	case _NONEQUAL_BY_TYPE:
		print(previd,"!==");
		break;
	case _LESS:
		print(previd,"<");
		break;
	case _LESS_OR_EQUAL:
		print(previd,"<=");
		break;
	case _LARGER:
		print(previd,">");
		break;
	case _LARGER_OR_EQUAL:
		print(previd,">=");
		break;
	case _CONCAT:
		print(previd,".");
		break;
	case _SQ_BRACKET:
		print(previd,"[]");
		break;
	case _ARROW:
		print(previd,"->");
		break;
	case _DCOLON:
		print(previd,"::");
		break;
	case _ARRAY:
		printArray(expr->value.arr,previd);
		break;
	case _FUNCTION:
		printFuncCall(expr->value.funccall,previd);
		break;
	case _NEWOBJ:
		printNewObj(expr->value.newobj,previd);
		break;
	case _CLONE:
		print(previd,"clone");
		break;
	case _NIL:
		print(previd,"null");
		break;
	case _ID:
		print(previd,"ID");
		print(nodeid,expr->value.id);
		break;
	case _INTEGER:
		print(previd,itoa(expr->value.i,buffer,10));
		break;
	case _DOUBLE:
		sprintf(buffer,"%f",expr->value.d);
		print(previd,buffer);
		break;
	case _BOOLEAN:		
		print(previd,(expr->value.i)?"true":"false");
		break;
	case _STRING:	
		strcpy(buffer,"\\\"");
		strcat(buffer,expr->value.s);
		strcat(buffer,"\\\"");
		print(previd,buffer);
		break;
	}
}

void printArray(struct Array * arr, int previd)
{
	int node = ++nodeid;
	int memberid;
	printFromTo(previd,node,"Array");

	while (arr!= NULL)
	{
		memberid=print(node,"member");
		if (arr->value->key!=NULL)
		{
			printExpression(arr->value->key,memberid);
		}
		printExpression(arr->value->value,memberid);

		arr= arr->next;
	}
}

void printFuncCall(struct FuncCall * call, int previd)
{
	int node = ++nodeid;
	printFromTo(previd,node,"FuncCall");
	printExpression(call->callexpr,node);
	printExpressionList(call->params,node);
}

void printNewObj(struct FuncCall * call, int previd)
{
	int node = ++nodeid;
	printFromTo(previd,node,"NewObj");
	printExpression(call->callexpr,node);
	printExpressionList(call->params,node);
}

void printExpressionList(struct ExpressionList *stmts,int previd)
{
	int node = ++nodeid;
	
	printFromTo(previd,node,"ExprList");
		
	while (stmts!=NULL)
	{
		printExpression(stmts->expr,node);
		stmts= stmts->next;
	}
}
