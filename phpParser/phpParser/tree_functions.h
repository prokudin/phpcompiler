#pragma once

#include "tree_structs.h"
extern void yyerror(char const *s);

struct Program * createProgram(struct HtmlText * htmltext);
struct HtmlText * createHtmlText(char * html, struct StatementsList * list);
struct HtmlText * addToHtmlText(struct HtmlText * htmltext,char * html, struct StatementsList * list);
struct StatementsList *  createStatementsList(struct Statement * stmt);
struct StatementsList * addStatementToList(struct StatementsList * list,struct Statement * stmt);
struct Statement * createStatement(enum EStatementType type, void * statement);
struct IfStatement * createIfStatement(struct Expression * expr, struct Statement * thenStmts, struct Statement * elseStmts);
struct ForStatement * createForStatement(struct ExpressionList * expr1,struct Expression * expr2,struct ExpressionList * expr3,struct Statement * statement);
struct ForEachStatement * createForEachStatement(struct Expression * arr,struct Expression * key,struct Expression * value,struct Statement * statement);
struct WhileDoStatement * createWhileDoStatement(struct Expression * expr,struct Statement * statement);
struct WhileDoStatement * createDoWhileStatement(struct Expression * expr,struct StatementsList * statement);
struct Expression * createNewObjExpressionFromFuncCall(struct Expression * funccall);
struct ExpressionList * createExpressionList(struct Expression * expr);
struct ExpressionList * addToExpressionList(struct ExpressionList * list,struct Expression * expr);
struct FunctionStatement * createFunctionStatement(char * id, int isMethod, int isStatic, int visibility, struct FormalFunctionArglist * arglist, int returncode, char * returnObjectId,struct StatementsList * stmts);
struct ClassStatement * createClassStatement(char * id, char * parentID,struct ClassMemberList * members);
struct ClassMemberList * createClassMemberList(struct ClassMemberStatement * member);
struct ClassMemberList * addToClassMemberList(struct ClassMemberList * list,struct ClassMemberStatement * member);
struct ClassMemberStatement * createClassFunctionStatement(struct FunctionStatement * function,int isStatic, int visibility);
struct ClassMemberStatement * createClassPropertyStatement(char * id,int isStatic, int visibility, struct Expression * value);
struct Expression * createExpression(int type,struct Expression * left, struct Expression * right);
struct Array * createArray(struct Expression * key, struct Expression * value);
struct Array * createArrayL(struct ExpressionList * list);
struct Array * addToArray(struct Array *arr,struct Expression * key, struct Expression * value);
struct Array * addToArrayL(struct Array *arr,struct ExpressionList * list);
struct FunctionArg * createFunctionArg(char * id,int vartype,char * userobjectname);
struct FormalFunctionArglist * createFormalFunctionArglist(struct FunctionArg * arg);
struct FormalFunctionArglist * addToFormalFunctionArglist(struct FormalFunctionArglist *list,struct FunctionArg * arg);
struct Expression * createIntExpression(int value);
struct Expression * createDoubleExpression(double value);
struct Expression * createStringExpression(char * value);
struct Expression * createFuncCallExpression(struct Expression * callexpr, struct ExpressionList * params);
struct Expression * createNewObjExpression(struct Expression * callexpr, struct ExpressionList * params);
struct Expression * createArrayExpression(struct Array * value);
struct Expression * createBooleanExpression(int value);
struct Expression * createIdExpression(char * id);
