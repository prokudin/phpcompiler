#include "ClassDescription.h"


int ClassDescription::indexOfUtf8Const(string value)
{
	auto constant = new ConstantUtf8(value);

	int index = indexOfConst(constant);

	delete constant;

	return index;
}

ConstantUtf8 * ClassDescription::addConstantUtf8(string value)
{
	auto iconst = new ConstantUtf8(value);
	int index;
	if ((index=indexOfConst(iconst)) == -1)
	{
		this->addConstant(iconst);
		return iconst;
	}

	return (ConstantUtf8*)this->constants[index];
}
	
ConstantClass * ClassDescription::addConstantClass(string value)
{
	auto iconst = new ConstantClass(addConstantUtf8(value));
	int index;
	
	if ((index=indexOfConst(iconst)) == -1)
	{
		this->addConstant(iconst);
		return iconst;
	}

	return (ConstantClass*)this->constants[index];
}
	
ConstantString * ClassDescription::addConstantString(string value)
{
	auto iconst = new ConstantString(addConstantUtf8(value));
	int index;
	
	if ((index=indexOfConst(iconst))==-1)
	{
		this->addConstant(iconst);
		return iconst;
	}

	return (ConstantString*)this->constants[index];
}
	
ConstantFloat * ClassDescription::addConstantFloat(float value)
{
	auto iconst = new ConstantFloat(value);
	int index;
	
	if ((index=indexOfConst(iconst)) == -1)
	{
		this->addConstant(iconst);
		return iconst;
	}

	return (ConstantFloat*)this->constants[index];

}
	
ConstantInteger * ClassDescription::addConstantInteger(int value)
{
	//if 

	auto iconst = new ConstantInteger(value);
	int index;
	
	if ((index=indexOfConst(iconst)) == -1)
	{
		this->addConstant(iconst);
		return iconst;
	}

	return (ConstantInteger*)this->constants[index];
}
	
ConstantNameAndType * ClassDescription::addConstantNameAndType(string name,string type)
{
	auto iconst = new ConstantNameAndType(addConstantUtf8(name),addConstantUtf8(type));
	int index;
	
	if ((index=indexOfConst(iconst)) == -1)
	{
		this->addConstant(iconst);
		return iconst;
	}

	return (ConstantNameAndType*)this->constants[index];
}
	
ConstantFieldref * ClassDescription::addConstantFieldref(string classname,string name,string type)
{
	auto iconst = new ConstantFieldref(addConstantClass(classname),addConstantNameAndType(name,type));
	int index;
	
	if ((index=indexOfConst(iconst)) == -1)
	{
		this->addConstant(iconst);
		return iconst;
	}

	return (ConstantFieldref*)this->constants[index];
}
	
ConstantMethodref * ClassDescription::addConstantMethodref(string classname,string name,string type)
{
	auto iconst = new ConstantMethodref(addConstantClass(classname),addConstantNameAndType(name,type));
	int index;
	
	if ((index=indexOfConst(iconst)) == -1)
	{
		this->addConstant(iconst);
		return iconst;
	}

	return (ConstantMethodref*)this->constants[index];
}

void ClassDescription::print()
{	
	printf("Class %s:\n",name->value->value.c_str());

	printf("\tConstants:\n");
	for(auto i=constants.cbegin();i!=constants.cend();++i)
	{
		(*i)->print();
	}
	printf("\n");

	printf("\tMethods:\n");
	int index =1;
	for(auto i=methods.cbegin();i!=methods.cend();++i,++index)
	{
		printf("\t\t%d\t%s\n",index,i->first.c_str());
	}

	printf("\tProperties:\n");
	index =1;
	for(auto i=properties.cbegin();i!=properties.cend();++i,++index)
	{
		printf("\t\t%d\t%s\n",index,i->first.c_str());
	}
}

int ClassDescription::indexOfConst(IConstant * constant)
{
	for (int i=0;i<constants.size();i++)
	{
		if (constants[i]->equal(constant))
		{
			return i;
		}
	}

	return -1;
}

void ClassDescription::generateConstTable()
{
	
}