#include "generate_code.h"

BytecodeInfo bytecodeInfo;

unsigned char u1;
unsigned short u2;
unsigned int u4;

char s1;
short s2;
int s4;

void fillBytecodeInfo()
{
	bytecodeInfo.comand.insert(pair<string,unsigned char>("aconst_null", 0x01));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("aconst_null",1));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("iconst_m1", 0x2));
	bytecodeInfo.comand.insert(pair<string,unsigned char>("iconst_0", 0x3));
	bytecodeInfo.comand.insert(pair<string,unsigned char>("iconst_1", 0x4));
	bytecodeInfo.comand.insert(pair<string,unsigned char>("iconst_2", 0x5));
	bytecodeInfo.comand.insert(pair<string,unsigned char>("iconst_3", 0x6));
	bytecodeInfo.comand.insert(pair<string,unsigned char>("iconst_4", 0x7));
	bytecodeInfo.comand.insert(pair<string,unsigned char>("iconst_5", 0x8));
	
	bytecodeInfo.const_i.insert(pair<int,string>(-1, "iconst_m1"));
	bytecodeInfo.const_i.insert(pair<int,string>(0, "iconst_0"));
	bytecodeInfo.const_i.insert(pair<int,string>(1, "iconst_1"));
	bytecodeInfo.const_i.insert(pair<int,string>(2, "iconst_2"));
	bytecodeInfo.const_i.insert(pair<int,string>(3, "iconst_3"));
	bytecodeInfo.const_i.insert(pair<int,string>(4, "iconst_4"));
	bytecodeInfo.const_i.insert(pair<int,string>(5, "iconst_5"));

	
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("iconst_m1",1));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("iconst_0",1));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("iconst_1",1));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("iconst_2",1));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("iconst_3",1));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("iconst_4",1));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("iconst_5",1));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("bipush", 0x10));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("bipush",2));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("sipush", 0x11));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("sipush",3));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("ldc", 0x12));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("ldc",2));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("ldc_w", 0x13));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("ldc_w",3));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("iload", 0x15));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("iload",2));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("aload", 0x19));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("aload",2));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("istore", 0x36));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("istore",2));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("aastore", 0x53));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("aastore",1));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("astore", 0x3A));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("astore",2));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("getstatic", 0xB2));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("getstatic",3));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("getfield", 0xB4));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("getstatic",3));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("putfield", 0xB5));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("putfield",3));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("pop", 0x57));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("pop",1));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("dup", 0x59));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("dup",1));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("swap", 0x5f));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("swap",1));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("ifeq", 0x99));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("ifeq",3));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("ifne", 0x9A));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("ifne",3));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("goto", 0xA7));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("goto",3));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("anewarray", 0xBD));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("anewarray",3));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("new", 0xBB));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("new",3));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("invokevirtual", 0xB6));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("invokevirtual",3));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("invokespecial", 0xB7));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("invokespecial",3));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("invokestatic", 0xB8));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("invokestatic",3));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("areturn", 0xB0));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("areturn",1));

	bytecodeInfo.comand.insert(pair<string,unsigned char>("return", 0xB1));
	bytecodeInfo.comandSize.insert(pair<string,unsigned char>("return",1));
}

void generateBytecode()
{
	fillBytecodeInfo();

	generateMainClassBytecode();

	for (auto i=GlobalDescription::classes.cbegin();i!=GlobalDescription::classes.cend();++i)
	{
		if (i->first!="0Main")
			generateClassBytecode(i->first);
	}
}

void generateMainClassBytecode()
{
	int descr = open("0Main.class",O_WRONLY | O_CREAT | O_TRUNC | O_BINARY,644);
	if (descr<0)
		_exception();
	
	// �������� ��������� ��������� �������
	u4 = htonl(0xCAFEBABE);	
	write(descr,(void*)&u4,4);
	u2 = htons(0);
	write(descr,(void*)&u2,2);
	u2 = htons(47);
	write(descr,(void*)&u2,2);

	// �������� ��������� �������
	vector<IConstant*> constants = GlobalDescription::classes["0Main"]->constants;

	// ���������� �������� + 1 
	u2 = htons(constants.size()+1);
	write(descr,(void*)&u2,2);
	
	for (int i=0; i<constants.size();++i)
	{		
		constants[i]->toBytecode(descr);
	}

	//����� �������
	u2 = htons(0x02 + 0x01);
	write(descr,(void*)&u2,2);

	//// ������� �����
	u2 = htons(GlobalDescription::classes["0Main"]->name->getNumber());
	write(descr,(void*)&u2,2);

	//// ��������
	u2 = htons(GlobalDescription::classes["0Main"]->parentname->getNumber());
	write(descr,(void*)&u2,2);

	//// ���������� �����������
	u2 =htons(0);
	write(descr,(void*)&u2,2);

	//// ���������� �����
	u2 = htons(0);
	write(descr,(void*)&u2,2);

	//// ���������� �������
	u2 = htons(GlobalDescription::classes["0Main"]->methods.size());
	write(descr,(void*)&u2,2);
	createMainConstruct(descr);
	createMainMethodReadline(descr);
	createMainMethod(descr);

	for (auto i = GlobalDescription::classes["0Main"]->methods.cbegin();i!=GlobalDescription::classes["0Main"]->methods.cend();++i)
	{
		if (i->first == "main" && i->second->descriptor->value == "([Ljava/lang/String;)V" 
			|| i->first == "<init>"
			|| i->first == "readLine")
			continue;
		createMethod(descr,string("0Main"),(string)i->first);
	}


	//// ���������� ���������
	u2 = htons(0);
	write(descr,(void*)&u2,2);


	close(descr);
}

void createMainMethodReadline(int descr)
{
	list<pair<string,list<pair<int,int>>>> bytecode=createMainMethodReadlineCode();
	int bsize = bytecodeLength(bytecode);

	// �������� ����� �������
	u2=htons(0x0001 | 0x0008);
	write(descr,(void*)&u2,2);
	
	// ��� ������ 
	u2=htons(GlobalDescription::classes["0Main"]->addConstantUtf8("readLine")->getNumber());
	write(descr,(void*)&u2,2);

	// ����������
	u2=htons(GlobalDescription::classes["0Main"]->addConstantUtf8("()Lphprtl/stdClass;")->getNumber());
	write(descr,(void*)&u2,2);

	// ���������� ���������
	u2=htons(1);
	write(descr,(void*)&u2,2);

	// ��� �������� 
	u2=htons(GlobalDescription::classes["0Main"]->addConstantUtf8("Code")->getNumber());
	write(descr,(void*)&u2,2);

	// ����� ��������
	u4 = htonl(2 +2 +4 + bsize +2 +2);
	write(descr,(void*)&u4,4);
	
	// ������ ����� ��������� ��� ������
	u2=htons(1000);
	write(descr,(void*)&u2,2);

	// ���������� ��������� ����������
	u2=htons(0);
	write(descr,(void*)&u2,2);

	// ����� ��������
	u4 = htonl(bsize);
	write(descr,(void*)&u4,4);

	// �������
	writeBytecode(descr,bytecode);

	// ����������
	u2=htons(0);
	write(descr,(void*)&u2,2);

	// ��������
	u2=htons(0);
	write(descr,(void*)&u2,2);
}

list<pair<string,list<pair<int,int>>>> createMainMethodReadlineCode()
{
	list<pair<string,list<pair<int,int>>>> result;
	list<pair<int,int>> args;

	//������� ������ ��������
	args.clear();
	args.push_back(pair<int,int>(GlobalDescription::classes["0Main"]->addConstantClass("java/util/Scanner")->getNumber(),_U2));
	result.push_back(pair<string,list<pair<int,int>>>("new",args));

	args.clear();
	result.push_back(pair<string,list<pair<int,int>>>("dup",args));

	args.clear();
	args.push_back(pair<int,int>(GlobalDescription::classes["0Main"]->addConstantFieldref("java/lang/System","in","Ljava/io/InputStream;")->getNumber(),_U2));
	result.push_back(pair<string,list<pair<int,int>>>("getstatic",args));

	args.clear();
	args.push_back(pair<int,int>(GlobalDescription::classes["0Main"]->addConstantMethodref("java/util/Scanner","<init>","(Ljava/io/InputStream;)V")->getNumber(),_U2));
	result.push_back(pair<string,list<pair<int,int>>>("invokespecial",args));

	args.clear();
	args.push_back(pair<int,int>(GlobalDescription::classes["0Main"]->addConstantMethodref("java/util/Scanner","nextLine","()Ljava/lang/String;")->getNumber(),_U2));
	result.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

	//�������������� � ������ stdClass � �������
	args.clear();
	args.push_back(pair<int,int>(GlobalDescription::classes["0Main"]->addConstantMethodref("phprtl/stdClass","fromString","(Ljava/lang/String;)Lphprtl/stdClass;")->getNumber(),_U2));
	result.push_back(pair<string,list<pair<int,int>>>("invokestatic",args));

	args.clear();
	result.push_back(pair<string,list<pair<int,int>>>("areturn",args));

	return result;
}

void generateClassBytecode(string classname)
{
	string filename = classname + ".class";
	int descr = open(filename.c_str(),O_WRONLY | O_CREAT | O_TRUNC | O_BINARY,644);
	if (descr<0)
		_exception();
	
	// �������� ��������� ��������� �������
	u4 = htonl(0xCAFEBABE);	
	write(descr,(void*)&u4,4);
	u2 = htons(0);
	write(descr,(void*)&u2,2);
	u2 = htons(47);
	write(descr,(void*)&u2,2);

	// �������� ��������� �������
	vector<IConstant*> constants = GlobalDescription::classes[classname]->constants;

	// ���������� �������� + 1 
	u2 = htons(constants.size()+1);
	write(descr,(void*)&u2,2);
	
	for (int i=0; i<constants.size();++i)
	{		
		constants[i]->toBytecode(descr);
	}

	//����� �������
	u2 = htons(0x02 + 0x01);
	write(descr,(void*)&u2,2);

	//// ������� �����
	u2 = htons(GlobalDescription::classes[classname]->name->getNumber());
	write(descr,(void*)&u2,2);

	//// ��������
	u2 = htons(GlobalDescription::classes[classname]->parentname->getNumber());
	write(descr,(void*)&u2,2);

	//// ���������� �����������
	u2 =htons(0);
	write(descr,(void*)&u2,2);

	//// ���������� �����
	u2 = htons(0);
	write(descr,(void*)&u2,2);

	//// ���������� �������
	u2 = htons(GlobalDescription::classes[classname]->methods.size());
	write(descr,(void*)&u2,2);
	
	createConstruct(descr,classname);
	for (auto i = GlobalDescription::classes[classname]->methods.cbegin();i!=GlobalDescription::classes[classname]->methods.cend();++i)
	{
		if (i->first == "<init>")
			continue;

		createMethod(descr,classname,(string)i->first);
	}

	//// ���������� ���������
	u2 = htons(0);
	write(descr,(void*)&u2,2);

	close(descr);
}

void createMainMethod(int descr)
{
	list<pair<string,list<pair<int,int>>>> bytecode=createMainMethodCode();
	int bsize = bytecodeLength(bytecode);

	// �������� ����� �������
	u2=htons(0x0001 | 0x0008);
	write(descr,(void*)&u2,2);
	
	// ��� ������ 
	u2=htons(GlobalDescription::classes["0Main"]->addConstantUtf8("main")->getNumber());
	write(descr,(void*)&u2,2);

	// ����������
	u2=htons(GlobalDescription::classes["0Main"]->addConstantUtf8("([Ljava/lang/String;)V")->getNumber());
	write(descr,(void*)&u2,2);

	// ���������� ���������
	u2=htons(1);
	write(descr,(void*)&u2,2);

	// ��� �������� 
	u2=htons(GlobalDescription::classes["0Main"]->addConstantUtf8("Code")->getNumber());
	write(descr,(void*)&u2,2);

	// ����� ��������
	u4 = htonl(2 +2 +4 + bsize +2 +2);
	write(descr,(void*)&u4,4);
	
	// ������ ����� ��������� ��� ������
	u2=htons(1000);
	write(descr,(void*)&u2,2);

	// ���������� ��������� ����������
	u2=htons(2);
	write(descr,(void*)&u2,2);

	// ����� ��������
	u4 = htonl(bsize);
	write(descr,(void*)&u4,4);

	// �������
	writeBytecode(descr,bytecode);

	// ����������
	u2=htons(0);
	write(descr,(void*)&u2,2);

	// ��������
	u2=htons(0);
	write(descr,(void*)&u2,2);
}

void createMainConstruct(int descr)
{	
	list<pair<string,list<pair<int,int>>>> bytecode=createMainConstructCode();
	int bsize = bytecodeLength(bytecode);

	// �������� ����� �������
	u2=htons(0x0001);
	write(descr,(void*)&u2,2);
	
	// ��� ������ 
	u2=htons(GlobalDescription::classes["0Main"]->addConstantUtf8("<init>")->getNumber());
	write(descr,(void*)&u2,2);

	// ����������
	u2=htons(GlobalDescription::classes["0Main"]->addConstantUtf8("()V")->getNumber());
	write(descr,(void*)&u2,2);

	// ���������� ���������
	u2=htons(1);
	write(descr,(void*)&u2,2);

	// ��� �������� 
	u2=htons(GlobalDescription::classes["0Main"]->addConstantUtf8("Code")->getNumber());
	write(descr,(void*)&u2,2);

	// ����� ��������
	u4 = htonl(2 +2 +4 + bsize +2 +2);
	write(descr,(void*)&u4,4);
	
	// ������ ����� ��������� ��� ������
	u2=htons(1000);
	write(descr,(void*)&u2,2);

	// ���������� ��������� ����������
	u2=htons(1);
	write(descr,(void*)&u2,2);

	// ����� ��������
	u4 = htonl(bsize);
	write(descr,(void*)&u4,4);

	// ,fqnrjl
	writeBytecode(descr,bytecode);

	// ����������
	u2=htons(0);
	write(descr,(void*)&u2,2);

	// ��������
	u2=htons(0);
	write(descr,(void*)&u2,2);
}

void createMethod(int descr, string & classname,string & methodname)
{
	// ������������� �������
	ClassMethodDescription * method = GlobalDescription::classes[classname]->methods[methodname];
	list<pair<string,list<pair<int,int>>>> bytecode=generateMethodBytecode(method,classname);
	int bsize = bytecodeLength(bytecode);

	// �������� ����� �������
	u2 = 0x0001;
	int ofset = 1;
	if (method->isStatic)
	{
		ofset=0;
		u2|=0x0008;
	}
	u2=htons(u2);
	write(descr,(void*)&u2,2);
	
	// ��� ������ 
	u2=htons(method->name->getNumber());
	write(descr,(void*)&u2,2);

	// ����������
	u2=htons(method->descriptor->getNumber());
	write(descr,(void*)&u2,2);

	// ���������� ���������
	u2=htons(1);
	write(descr,(void*)&u2,2);

	// ��� �������� 
	u2=htons(GlobalDescription::classes[classname]->addConstantUtf8("Code")->getNumber());
	write(descr,(void*)&u2,2);

	// ����� ��������
	u4 = htonl(2 +2 +4 + bsize +2 +2);
	write(descr,(void*)&u4,4);
	
	// ������ ����� ��������� ��� ������
	u2=htons(1000);
	write(descr,(void*)&u2,2);

	// ���������� ��������� ����������+ Variables
	u2=htons(ofset + method->local_vars.size()+ 1);
	write(descr,(void*)&u2,2);

	// ����� ��������
	u4 = htonl(bsize);
	write(descr,(void*)&u4,4);

	// �������
	writeBytecode(descr,bytecode);

	// ����������
	u2=htons(0);
	write(descr,(void*)&u2,2);

	// ��������
	u2=htons(0);
	write(descr,(void*)&u2,2);
}

void createConstruct(int descr, string & classname)
{
	list<pair<string,list<pair<int,int>>>> bytecode=createConstructCode(classname);
	int bsize = bytecodeLength(bytecode);

	// �������� ����� �������
	u2=htons(0x0001);
	write(descr,(void*)&u2,2);
	
	// ��� ������ 
	u2=htons(GlobalDescription::classes[classname]->addConstantUtf8("<init>")->getNumber());
	write(descr,(void*)&u2,2);

	// ����������
	u2=htons(GlobalDescription::classes[classname]->addConstantUtf8("()V")->getNumber());
	write(descr,(void*)&u2,2);

	// ���������� ���������
	u2=htons(1);
	write(descr,(void*)&u2,2);

	// ��� �������� 
	u2=htons(GlobalDescription::classes[classname]->addConstantUtf8("Code")->getNumber());
	write(descr,(void*)&u2,2);

	// ����� ��������
	u4 = htonl(2 +2 +4 + bsize +2 +2);
	write(descr,(void*)&u4,4);
	
	// ������ ����� ��������� ��� ������
	u2=htons(1000);
	write(descr,(void*)&u2,2);

	// ���������� ��������� ����������
	u2=htons(1);
	write(descr,(void*)&u2,2);

	// ����� ��������
	u4 = htonl(bsize);
	write(descr,(void*)&u4,4);

	// ,fqnrjl
	writeBytecode(descr,bytecode);

	// ����������
	u2=htons(0);
	write(descr,(void*)&u2,2);

	// ��������
	u2=htons(0);
	write(descr,(void*)&u2,2);
}

list<pair<string,list<pair<int,int>>>> createConstructCode(string & classname)
{
	list<pair<string,list<pair<int,int>>>> result;
	list<pair<int,int>> args;

	// �������� ����������� ������������� ������
	// �������� this
	args.clear();
	args.push_back(pair<int,int>(0,_U1));
	result.push_back(pair<string,list<pair<int,int>>>("aload",args));

	args.clear();
	result.push_back(pair<string,list<pair<int,int>>>("dup",args));

	// ����� ������������ ��������
	args.clear();
	args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref(GlobalDescription::classes[classname]->parentname->value->value,"<init>","()V")->getNumber(),_U2));
	result.push_back(pair<string,list<pair<int,int>>>("invokespecial",args));
	
	args.clear();
	result.push_back(pair<string,list<pair<int,int>>>("dup",args));

	// ��������� ��� ������
	args.clear();
	args.push_back(pair<int,int>(GlobalDescription::classes[classname]->names->getNumber(),_U2));
	result.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));

	args.clear();
	args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantFieldref(GlobalDescription::classes[classname]->name->value->value,"className","Ljava/lang/String;")->getNumber(),_U2));
	result.push_back(pair<string,list<pair<int,int>>>("putfield",args));


	// ��� ������� �������������� ����
	for (auto j = GlobalDescription::classes[classname]->properties.cbegin(); j!=GlobalDescription::classes[classname]->properties.cend();j++)
	{
		if (j->second->isStatic)
			continue;

		args.clear();
		result.push_back(pair<string,list<pair<int,int>>>("dup",args));

		// �������� ���
		args.clear();
		args.push_back(pair<int,int>(j->second->name->getNumber(),_U2));
		result.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));

		// �������� ���������
		args.clear();
		if (j->second->visibility == _PUBLIC)
			result.push_back(pair<string,list<pair<int,int>>>("iconst_0",args));				
		else if (j->second->visibility == _PROTECTED)
			result.push_back(pair<string,list<pair<int,int>>>("iconst_1",args));				
		else 
			result.push_back(pair<string,list<pair<int,int>>>("iconst_2",args));

		// �������� ��������
		result.splice(result.end(),generateExpressionBytecode(j->second->value,classname,1));

		//������� ���������� ���������
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","addField","(Ljava/lang/String;ILphprtl/stdClass;)V")->getNumber(),_U2));
		result.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
	}

	// ������ this
	args.clear();
	result.push_back(pair<string,list<pair<int,int>>>("pop",args));

	// ������� ��������
	args.clear();
	result.push_back(pair<string,list<pair<int,int>>>("return",args));

	return result;
}

list<pair<string,list<pair<int,int>>>> createMainConstructCode()
{
	list<pair<string,list<pair<int,int>>>> result;
	list<pair<int,int>> args;

	// �������� ����������� ������������� ������
	// �������� this
	args.clear();
	args.push_back(pair<int,int>(0,_U1));
	result.push_back(pair<string,list<pair<int,int>>>("aload",args));

	// ����� ������������ ��������
	args.clear();
	args.push_back(pair<int,int>(GlobalDescription::classes["0Main"]->addConstantMethodref("java/lang/Object","<init>","()V")->getNumber(),_U2));
	result.push_back(pair<string,list<pair<int,int>>>("invokespecial",args));
	
	// ������� ��������
	args.clear();
	result.push_back(pair<string,list<pair<int,int>>>("return",args));

	return result;
}

list<pair<string,list<pair<int,int>>>> createMainMethodCode()
{
	list<pair<string,list<pair<int,int>>>> result;
	list<pair<int,int>> args;

	ClassDescription * main = GlobalDescription::classes["0Main"];
		
	// ��� ������� ������ �������� ��������� ����������� ����� � ����������� �������
	for (auto i=GlobalDescription::classes.cbegin();i!=GlobalDescription::classes.cend();++i)
	{
		for (auto j = i->second->methods.cbegin(); j!=i->second->methods.cend();j++ )
		{// ��� ������� ������ ����� ������������
			if (j->first=="<init>")
				continue;

			// �������� ���
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes["0Main"]->addConstantString(j->first)->getNumber(),_U2));
			result.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));

			// �������� ���������
			args.clear();
			if (j->second->visibility == _PUBLIC)
				result.push_back(pair<string,list<pair<int,int>>>("iconst_0",args));				
			else if (j->second->visibility == _PROTECTED)
				result.push_back(pair<string,list<pair<int,int>>>("iconst_1",args));				
			else 
				result.push_back(pair<string,list<pair<int,int>>>("iconst_2",args));

			// �������� ��� ������� ������
			args.clear();				
			args.push_back(pair<int,int>(GlobalDescription::classes["0Main"]->addConstantString(i->second->name->value->value)->getNumber(),_U2));
			result.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));
			

			//������� ���������� ���������
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes["0Main"]->addConstantMethodref("phprtl/stdClass","setStaticMethodVisibility","(Ljava/lang/String;ILjava/lang/String;)V")->getNumber(),_U2));
			result.push_back(pair<string,list<pair<int,int>>>("invokestatic",args));
		}

		for (auto j = i->second->properties.cbegin(); j!=i->second->properties.cend();j++ )
		{// ��� ������� ��������
			if (!j->second->isStatic)
				continue;

			// �������� ���
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes["0Main"]->addConstantString(j->first)->getNumber(),_U2));
			result.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));

			// �������� ���������
			args.clear();
			if (j->second->visibility == _PUBLIC)
				result.push_back(pair<string,list<pair<int,int>>>("iconst_0",args));				
			else if (j->second->visibility == _PROTECTED)
				result.push_back(pair<string,list<pair<int,int>>>("iconst_1",args));				
			else
				result.push_back(pair<string,list<pair<int,int>>>("iconst_2",args));

			// �������� ��� ������� ������
			args.clear();				
			args.push_back(pair<int,int>(GlobalDescription::classes["0Main"]->addConstantString(i->second->name->value->value)->getNumber(),_U2));
			result.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));

			if (j->second->value == NULL)
			{// ���� �������� ���
				args.clear();
				args.push_back(pair<int,int>(GlobalDescription::classes["0Main"]->addConstantMethodref("phprtl/stdClass","fromNull","()Lphprtl/stdClass;")->getNumber(),_U2));
				result.push_back(pair<string,list<pair<int,int>>>("invokestatic",args));
			}
			else
			{
				result.splice(result.end(),generateExpressionBytecode(j->second->value,string("0Main"),1));
			}

			//������� ���������� ���������
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes["0Main"]->addConstantMethodref("phprtl/stdClass","addStaticField","(Ljava/lang/String;ILjava/lang/String;Lphprtl/stdClass;)V")->getNumber(),_U2));
			result.push_back(pair<string,list<pair<int,int>>>("invokestatic",args));
		}

	}

	//��� ������������ �� Main
			// �������� ���
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes["0Main"]->addConstantString("__construct")->getNumber(),_U2));
			result.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));

			// �������� ���������
			args.clear();
			result.push_back(pair<string,list<pair<int,int>>>("iconst_0",args));
			
			// �������� ��� ������� ������
			args.clear();				
			args.push_back(pair<int,int>(GlobalDescription::classes["0Main"]->addConstantString("phprtl/stdClass")->getNumber(),_U2));
			result.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));
			

			//������� ���������� ���������
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes["0Main"]->addConstantMethodref("phprtl/stdClass","setStaticMethodVisibility","(Ljava/lang/String;ILjava/lang/String;)V")->getNumber(),_U2));
			result.push_back(pair<string,list<pair<int,int>>>("invokestatic",args));


	// ��� �� ���������� ������

		//�������� ���������� Variables
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes["0Main"]->addConstantClass("phprtl/Variables")->getNumber(),_U2));
		result.push_back(pair<string,list<pair<int,int>>>("new",args));

		args.clear();
		result.push_back(pair<string,list<pair<int,int>>>("dup",args));

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes["0Main"]->addConstantMethodref("phprtl/Variables","<init>","()V")->getNumber(),_U2));
		result.push_back(pair<string,list<pair<int,int>>>("invokespecial",args));
		
		// ��������� Variables � ������� ����������
		args.clear();
		args.push_back(pair<int,int>(1,_U1));
		result.push_back(pair<string,list<pair<int,int>>>("astore",args));

		//��� �� stmtlist
		result.splice(result.end(),generateStatementsListBytecode(_yyprg->stmts->stmts,string("0Main"),1));

	//return
	args.clear();
	result.push_back(pair<string,list<pair<int,int>>>("return",args));

	return result;
}

list<pair<string,list<pair<int,int>>>> generateMethodBytecode(ClassMethodDescription * method, string & classname)
{
	list<pair<string,list<pair<int,int>>>> result;
	list<pair<int,int>> args;

	int ofset = (method->isStatic) ? 0 : 1 ;

	// �������� ���������� Variables
	args.clear();
	args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantClass("phprtl/Variables")->getNumber(),_U2));
	result.push_back(pair<string,list<pair<int,int>>>("new",args));

	args.clear();
	result.push_back(pair<string,list<pair<int,int>>>("dup",args));

	args.clear();
	args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","<init>","()V")->getNumber(),_U2));
	result.push_back(pair<string,list<pair<int,int>>>("invokespecial",args));

	// ����������� ���������
	for (int i=0; i<method->local_vars.size();i++)
	{
		args.clear();
		result.push_back(pair<string,list<pair<int,int>>>("dup",args));

		// �������� ��� ����������
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantString(method->local_vars[i].name)->getNumber(),_U2));
		result.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));

		//�������������� ��� ����������
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","fromString","(Ljava/lang/String;)Lphprtl/stdClass;")->getNumber(),_U2));
		result.push_back(pair<string,list<pair<int,int>>>("invokestatic",args));

		// ��������� �������� �� ����
		args.clear();
		args.push_back(pair<int,int>(i+ofset,_U1));
		result.push_back(pair<string,list<pair<int,int>>>("aload",args));

		// ������� ����� ������������
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","myClone","()Lphprtl/stdClass;")->getNumber(),_U2));
		result.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		// �������� �������� � ������ �����
		switch (method->local_vars[i].type)
		{
		case _NONE:
			break;
		case _USEROBJECT:		
			// �������� ��� ������
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantString(method->local_vars[i].userObjectName)->getNumber(),_U2));
			result.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));

			// ������� ����������
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertTo","(Ljava/lang/String;)Lphprtl/stdClass;")->getNumber(),_U2));
			result.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
			break;
		case _INTEGER:
			// ������� ����������
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","toInt","()Lphprtl/stdClass;")->getNumber(),_U2));
			result.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
			break;
		case _DOUBLE:
			// ������� ����������
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","toFloat","()Lphprtl/stdClass;")->getNumber(),_U2));
			result.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
			break;

		case _STRING:
			// ������� ����������
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","toStr","()Lphprtl/stdClass;")->getNumber(),_U2));
			result.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
			break;

		case _ARRAY:
			// ������� ����������
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","toArray","()Lphprtl/stdClass;")->getNumber(),_U2));
			result.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
			break;
		case _BOOLEAN:
			// ������� ����������
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","toBool","()Lphprtl/stdClass;")->getNumber(),_U2));
			result.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
			break;
		}

		//������� ����� put ��� Variables
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","put","(Lphprtl/stdClass;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		result.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		// ������ ������ �������� �� �����
		args.clear();
		result.push_back(pair<string,list<pair<int,int>>>("pop",args));
	}

	if (!method->isStatic)
	{// �������� this � variables
		args.clear();
		result.push_back(pair<string,list<pair<int,int>>>("dup",args));

		// �������� ��� ����������
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantString("this")->getNumber(),_U2));
		result.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));

		//�������������� ��� ����������
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","fromString","(Ljava/lang/String;)Lphprtl/stdClass;")->getNumber(),_U2));
		result.push_back(pair<string,list<pair<int,int>>>("invokestatic",args));

		// �������� ��������
		args.clear();
		args.push_back(pair<int,int>(0,_U1));
		result.push_back(pair<string,list<pair<int,int>>>("aload",args));

		//������� ����� put ��� Variables
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","put","(Lphprtl/stdClass;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		result.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		// ������ ������ �������� �� �����
		args.clear();
		result.push_back(pair<string,list<pair<int,int>>>("pop",args));
	}

	// ��������� Variables � ������� ����������
	args.clear();
	args.push_back(pair<int,int>(method->local_vars.size() + ofset,_U1));
	result.push_back(pair<string,list<pair<int,int>>>("astore",args));
	

	//��������� ��� ��� stmtsList
	result.splice(result.end(),generateStatementsListBytecode(method->code->stmts,classname,ofset + method->local_vars.size()));
	
	return result;
}

list<pair<string,list<pair<int,int>>>> generateStatementsListBytecode(StatementsList * stmts,string & classname,int varIndex)
{
	list<pair<string,list<pair<int,int>>>> bytecode;
	static list<pair<int,int>> args;

	StatementsList * iter = stmts;
	while (iter)
	{
		switch(iter->stmt->code)
		{
		case _EXPR:
			bytecode.splice(bytecode.end(),generateExpressionBytecode(iter->stmt->stmt.exprStmt,classname,varIndex));

			args.clear();
			bytecode.push_back(pair<string,list<pair<int,int>>>("pop",args));
			break;
		case _IF:
			generateIfStmtBytecode(iter->stmt->stmt.ifStmt,bytecode,classname,varIndex);
			break;
		case _FOREACH:
			generateForeachStmtBytecode(iter->stmt->stmt.foreachStmt,bytecode,classname,varIndex);
			break;
		case _WHILEDO:
			generateWhileDoStmtBytecode(iter->stmt->stmt.whiledoStmt,bytecode,classname,varIndex);
			break;
		case _DOWHILE:
			generateDoWhileStmtBytecode(iter->stmt->stmt.whiledoStmt,bytecode,classname,varIndex);
			break;
		case _ECHO:
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantFieldref("java/lang/System","out","Ljava/io/PrintStream;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("getstatic",args));

			//����� �������� ���-�
			bytecode.splice(bytecode.end(),generateExpressionBytecode(iter->stmt->stmt.exprStmt,classname,varIndex));

			//�������������� � ������
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToString","()Ljava/lang/String;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
			
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("java/io/PrintStream","print","(Ljava/lang/String;)V")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

			break;
		case _RETURN:
			bytecode.splice(bytecode.end(),generateExpressionBytecode(iter->stmt->stmt.exprStmt,classname,varIndex));

			args.clear();
			bytecode.push_back(pair<string,list<pair<int,int>>>("areturn",args));
			break;
		case _STMTLIST:
			return generateStatementsListBytecode(iter->stmt->stmt.stmtlist,classname,varIndex);
		}

		iter = iter->next;
	}

	return bytecode;
}

void generateIfStmtBytecode(IfStatement * stmt,list<pair<string,list<pair<int,int>>>> & bytecode, string & classname,int varIndex)
{
	static list<pair<int,int>> args;

	if (stmt->elselist == NULL)
	{
		auto thenB = generateStatementsListBytecode(stmt->thenlist,classname,varIndex);

		// ��������� ���-�
		bytecode.splice(bytecode.end(),generateExpressionBytecode(stmt->expr,classname,varIndex));

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToBoolean","()Z")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		// �������� �������
		args.clear();
		args.push_back(pair<int,int>(3 + bytecodeLength(thenB),_S2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("ifeq",args));

		bytecode.splice(bytecode.end(),thenB);
	}
	else
	{
		auto thenB = generateStatementsListBytecode(stmt->thenlist,classname,varIndex);
		auto elseB = generateStatementsListBytecode(stmt->elselist,classname,varIndex);


		// ��������� ���-�
		bytecode.splice(bytecode.end(),generateExpressionBytecode(stmt->expr,classname,varIndex));

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToBoolean","()Z")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		// �������� �������� � ���� then
		int thOF = 3 + bytecodeLength(elseB);
		args.clear();
		args.push_back(pair<int,int>(thOF,_S2));
		thenB.push_back(pair<string,list<pair<int,int>>>("goto",args));

		// �������� �������
		args.clear();
		args.push_back(pair<int,int>(3 + bytecodeLength(thenB),_S2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("ifeq",args));

		bytecode.splice(bytecode.end(),thenB);
		bytecode.splice(bytecode.end(),elseB);
	}
}

void generateWhileDoStmtBytecode(WhileDoStatement * cycle, list<pair<string,list<pair<int,int>>>> & bytecode, string & classname,int varIndex)
{
	list<pair<string,list<pair<int,int>>>> expr,stmts;
	static list<pair<int,int>> args;

	// ���������
		expr.splice(expr.end(),generateExpressionBytecode(cycle->expr,classname,varIndex));

		// �������������� � ���
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToBoolean","()Z")->getNumber(),_U2));
		expr.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

	// ���� �����
	stmts.splice(stmts.end(),generateStatementsListBytecode(cycle->stmts,classname,varIndex));

	int withoutGotoLen = bytecodeLength(stmts);

	// �������� �������� � ���� stmts		
		args.clear();
		args.push_back(pair<int,int>(-1*(withoutGotoLen+3 + bytecodeLength(expr)),_S2));
		stmts.push_back(pair<string,list<pair<int,int>>>("goto",args));


	//// �������� �������� ������� � ���������
		args.clear();
		args.push_back(pair<int,int>(3 + bytecodeLength(stmts),_S2));
		expr.push_back(pair<string,list<pair<int,int>>>("ifeq",args));

	bytecode.splice(bytecode.end(),expr);
	bytecode.splice(bytecode.end(),stmts);
}

void generateForeachStmtBytecode(ForEachStatement * cycle,list<pair<string,list<pair<int,int>>>> & bytecode, string & classname,int varIndex)
{
	list<pair<string,list<pair<int,int>>>> expr,stmts;
	static list<pair<int,int>> args;

		// ������
		bytecode.splice(bytecode.end(),generateExpressionBytecode(cycle->arr,classname,varIndex));

		// �������������� � Array
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToArray","()Lphprtl/Array;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		// �������� �������� �������
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Array","iterator","()Lphprtl/ArrayIterator;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));


	//����������� ��������
	args.clear();	
	expr.push_back(pair<string,list<pair<int,int>>>("dup",args));

	// ������ ���� �� ���������
	args.clear();
	args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/ArrayIterator","hasNext","()Z")->getNumber(),_U2));
	expr.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

	//����������� ��������
	args.clear();	
	stmts.push_back(pair<string,list<pair<int,int>>>("dup",args));

	//������� � ����������
	args.clear();
	args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/ArrayIterator","next","()V")->getNumber(),_U2));
	stmts.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

	if (cycle->key!=NULL)
	{
		//����������� ��������
		args.clear();	
		stmts.push_back(pair<string,list<pair<int,int>>>("dup",args));

		//��������
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/ArrayIterator","key","()Lphprtl/stdClass;")->getNumber(),_U2));
		stmts.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		// ��������� ���������� � �����������
		args.clear();
		args.push_back(pair<int,int>(varIndex,_U1));
		stmts.push_back(pair<string,list<pair<int,int>>>("aload",args));

		args.clear();
		stmts.push_back(pair<string,list<pair<int,int>>>("swap",args));

		//���
		stmts.splice(stmts.end(),generateExpressionBytecode(cycle->key->right,classname,varIndex));

		args.clear();
		stmts.push_back(pair<string,list<pair<int,int>>>("swap",args));

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","put","(Lphprtl/stdClass;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		stmts.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		
		args.clear();
		stmts.push_back(pair<string,list<pair<int,int>>>("pop",args));
	}


	//����������� ��������
		args.clear();	
		stmts.push_back(pair<string,list<pair<int,int>>>("dup",args));

		//��������
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/ArrayIterator","value","()Lphprtl/stdClass;")->getNumber(),_U2));
		stmts.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		// ��������� ���������� � �����������
		args.clear();
		args.push_back(pair<int,int>(varIndex,_U1));
		stmts.push_back(pair<string,list<pair<int,int>>>("aload",args));

		args.clear();
		stmts.push_back(pair<string,list<pair<int,int>>>("swap",args));

		//���
		stmts.splice(stmts.end(),generateExpressionBytecode(cycle->value->right,classname,varIndex));

		args.clear();
		stmts.push_back(pair<string,list<pair<int,int>>>("swap",args));

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","put","(Lphprtl/stdClass;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		stmts.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		
		args.clear();
		stmts.push_back(pair<string,list<pair<int,int>>>("pop",args));

	// �������� ��������� ��� stmtList
	stmts.splice(stmts.end(),generateStatementsListBytecode(cycle->stmts,classname,varIndex));

	// �������� �������� � ���� stmts	
	args.clear();
	args.push_back(pair<int,int>(-1*(bytecodeLength(stmts) + 3 + bytecodeLength(expr)),_S2));
	stmts.push_back(pair<string,list<pair<int,int>>>("goto",args));

	//// �������� �������� ������� � ���������
	args.clear();
	args.push_back(pair<int,int>(3 + bytecodeLength(stmts),_S2));
	expr.push_back(pair<string,list<pair<int,int>>>("ifeq",args));

	bytecode.splice(bytecode.end(),expr);
	bytecode.splice(bytecode.end(),stmts);

	// ������� ��������
	args.clear();	
	bytecode.push_back(pair<string,list<pair<int,int>>>("pop",args));
}

void generateDoWhileStmtBytecode(WhileDoStatement * cycle,list<pair<string,list<pair<int,int>>>> & bytecode, string & classname,int varIndex)
{
	list<pair<string,list<pair<int,int>>>> expr,stmts;
	static list<pair<int,int>> args;

	// ���� �����
	stmts.splice(stmts.end(),generateStatementsListBytecode(cycle->stmts,classname,varIndex));

	// ���������
	stmts.splice(stmts.end(),generateExpressionBytecode(cycle->expr,classname,varIndex));
	// �������������� � ���
	args.clear();
	args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToBoolean","()Z")->getNumber(),_U2));
	stmts.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

	// �������� �������� ������� � ���������
	args.clear();
	args.push_back(pair<int,int>(-1*bytecodeLength(stmts),_S2));
	stmts.push_back(pair<string,list<pair<int,int>>>("ifne",args));
	
	bytecode.splice(bytecode.end(),stmts);
}

list<pair<string,list<pair<int,int>>>> generateExpressionBytecode(Expression * expr, string & classname,int varIndex)
{
	list<pair<string,list<pair<int,int>>>> bytecode;

	if (!expr)
		return bytecode;

	static list<pair<int,int>> args;
	int i;
	Array * arr;
	ExpressionList * elist;
	pair<int,int> ofs ;
	list<pair<string,list<pair<int,int>>>> rExprB ;
	switch(expr->type)
	{
	case _BOOLEAN:

		if (expr->value.i)
		{
			args.clear();
			bytecode.push_back(pair<string,list<pair<int,int>>>("iconst_1",args));
		}
		else
		{
			args.clear();
			bytecode.push_back(pair<string,list<pair<int,int>>>("iconst_0",args));
		}

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","fromBool","(Z)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokestatic",args));

		break;
	case _NIL:
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","fromNull","()Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokestatic",args));
		break;
	case _INTEGER:
		args.clear();
		if (expr->value.i>=-1 && expr->value.i<=5)
		{
			bytecode.push_back(pair<string,list<pair<int,int>>>(bytecodeInfo.const_i[expr->value.i],args));
		}
		else
		{
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantInteger(expr->value.i)->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));
		}

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","fromInt","(I)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokestatic",args));
		break;
	case _DOUBLE:
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantFloat((float)(expr->value.d))->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","fromFloat","(F)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokestatic",args));

		break;
	case _ID:
	case _STRING:
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantString(expr->value.id)->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","fromString","(Ljava/lang/String;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokestatic",args));
		break;
	case _ARRAY_PUSHBACK:
		// �������� ����� ��������
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));

		// ��������������� �������� � ������
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToArray","()Lphprtl/Array;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		// �������� ����
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Array","put","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		break;
	case _ARRAY:
		// ������� ������
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantClass("phprtl/Array")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("new",args));

		args.clear();
		bytecode.push_back(pair<string,list<pair<int,int>>>("dup",args));

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Array","<init>","()V")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokespecial",args));

		//�������� ���������
		arr = expr->value.arr;
		while (arr)
		{
			args.clear();
			bytecode.push_back(pair<string,list<pair<int,int>>>("dup",args));

			if (arr->value->key!=NULL)
			{
				bytecode.splice(bytecode.end(),generateExpressionBytecode(arr->value->key,classname,varIndex));
				bytecode.splice(bytecode.end(),generateExpressionBytecode(arr->value->value,classname,varIndex));

				args.clear();
				args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Array","put","(Lphprtl/stdClass;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
				bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
			}
			else
			{
				bytecode.splice(bytecode.end(),generateExpressionBytecode(arr->value->value,classname,varIndex));

				args.clear();
				args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Array","put","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
				bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
			}

			args.clear();
			bytecode.push_back(pair<string,list<pair<int,int>>>("pop",args));

			arr = arr->next;
		}

		//�������������� � stdClass
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","fromArray","(Lphprtl/Array;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokestatic",args));
		break;

	case _ADD:
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));		
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));
		
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opAdd","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		break;
	case _SUB:
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));
		
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opSub","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		break;
	case _MUL:
		
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));
		
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opMul","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		break;
	case _DIVISION:
		
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));
		
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opDiv","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		break;

	case _POW:
		
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));
		
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opPow","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		break;

	case _UMINUS:
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));
		
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opUminus","()Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		break;

	case _OR:
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));
		
		args.clear();
		bytecode.push_back(pair<string,list<pair<int,int>>>("dup",args));

		// �������������� � bool
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToBoolean","()Z")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		// ��������� ����� then
		rExprB = generateExpressionBytecode(expr->right,classname,varIndex);

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opOr","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		rExprB.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		// �������� ��������
		args.clear();
		args.push_back(pair<int,int>(bytecodeLength(rExprB)+3,_S2));
		ofs = args.back();		
		bytecode.push_back(pair<string,list<pair<int,int>>>("ifne",args));


		bytecode.splice(bytecode.end(),rExprB);

		break;

	case _AND:
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));
		
		args.clear();
		bytecode.push_back(pair<string,list<pair<int,int>>>("dup",args));

		// �������������� � bool
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToBoolean","()Z")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		// ��������� ����� then
		rExprB = generateExpressionBytecode(expr->right,classname,varIndex);
		
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opAnd","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		rExprB.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		
		// �������� ��������
		args.clear();
		args.push_back(pair<int,int>(bytecodeLength(rExprB)+3,_S2));
		ofs = args.back();		
		bytecode.push_back(pair<string,list<pair<int,int>>>("ifeq",args));

		bytecode.splice(bytecode.end(),rExprB);
		break;
	case _EQUAL:
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));
		
		
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opEqual","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		break;

	case _NONEEQUAL:
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));
		
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opNoneEqual","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		break;

	case _EQUAL_BY_TYPE:
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));
		
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opEqualByType","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		break;

	case _NONEQUAL_BY_TYPE:
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));
		
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","nonEqualByType","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		break;

	case _LESS:
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));
		
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opLess","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		break;

	case _LESS_OR_EQUAL:
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));
		
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opLessOrEqual","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		break;

	case _LARGER:
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opLarger","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		break;

	case _LARGER_OR_EQUAL:
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));
		
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opLargerOrEqual","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		break;

	case _CONCAT:
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));
		
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opConcat","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		break;

	case _NOT:
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));
		
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opNot","()Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		break;
	///******************************************************************************************************************************/
	case _DOLLAR:
		// ��������� ���������� � �����������
		args.clear();
		args.push_back(pair<int,int>(varIndex,_U1));
		bytecode.push_back(pair<string,list<pair<int,int>>>("aload",args));

		//���
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));		

		// ������� ����� get
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","dollar","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		break;

	case _DOLLAR_ASSIGN:
		
		// ��������� ���������� � �����������
		args.clear();
		args.push_back(pair<int,int>(varIndex,_U1));
		bytecode.push_back(pair<string,list<pair<int,int>>>("aload",args));

		//���
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));

		//��������
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","put","(Lphprtl/stdClass;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		break;

	case _SQ_BRACKET:
		// ����� ��������
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));
		
		// ��������������� �������� � ������
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToArray","()Lphprtl/Array;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		// �������� ����
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));

		// �������� �������� �� �������
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Array","get","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		break;

	case _SQ_BRACKET_ASSIGN:
		// ����� ��������
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));

		// ��������������� �������� � ������
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToArray","()Lphprtl/Array;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		// ����� ����
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->center,classname,varIndex));

		// ����� ��������
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));

		// �������� �������� �� �������
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Array","put","(Lphprtl/stdClass;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		break;
	case _INC_DOLLAR:
		// ��������� ���������� � �����������
		args.clear();
		args.push_back(pair<int,int>(varIndex,_U1));
		bytecode.push_back(pair<string,list<pair<int,int>>>("aload",args));

		args.clear();
		bytecode.push_back(pair<string,list<pair<int,int>>>("dup",args));

		// ��������� ���
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","dollar","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opInc","()Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		// ��������� ���
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));

		args.clear();
		bytecode.push_back(pair<string,list<pair<int,int>>>("swap",args));

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","put","(Lphprtl/stdClass;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		break;
	case _DOLLAR_INC:
		// ��������� ���������� � �����������
		args.clear();
		args.push_back(pair<int,int>(varIndex,_U1));
		bytecode.push_back(pair<string,list<pair<int,int>>>("aload",args));

		// ��������� ���
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","dollar","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		args.clear();
		bytecode.push_back(pair<string,list<pair<int,int>>>("dup",args));
		
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opInc","()Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		// ��������� ���������� � �����������
		args.clear();
		args.push_back(pair<int,int>(varIndex,_U1));
		bytecode.push_back(pair<string,list<pair<int,int>>>("aload",args));

		args.clear();
		bytecode.push_back(pair<string,list<pair<int,int>>>("swap",args));

		// ��������� ��� 
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));

		args.clear();
		bytecode.push_back(pair<string,list<pair<int,int>>>("swap",args));

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","put","(Lphprtl/stdClass;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		args.clear();
		bytecode.push_back(pair<string,list<pair<int,int>>>("pop",args));

		break;
	case _DEC_DOLLAR:
		// ��������� ���������� � �����������
		args.clear();
		args.push_back(pair<int,int>(varIndex,_U1));
		bytecode.push_back(pair<string,list<pair<int,int>>>("aload",args));

		args.clear();
		bytecode.push_back(pair<string,list<pair<int,int>>>("dup",args));

		// ��������� ���
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","dollar","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opDec","()Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		// ��������� ���
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));

		args.clear();
		bytecode.push_back(pair<string,list<pair<int,int>>>("swap",args));

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","put","(Lphprtl/stdClass;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		
		break;
	case _DOLLAR_DEC:
		// ��������� ���������� � �����������
		args.clear();
		args.push_back(pair<int,int>(varIndex,_U1));
		bytecode.push_back(pair<string,list<pair<int,int>>>("aload",args));

		// ��������� ���
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","dollar","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		args.clear();
		bytecode.push_back(pair<string,list<pair<int,int>>>("dup",args));
		
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","opDec","()Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		// ��������� ���������� � �����������
		args.clear();
		args.push_back(pair<int,int>(varIndex,_U1));
		bytecode.push_back(pair<string,list<pair<int,int>>>("aload",args));

		args.clear();
		bytecode.push_back(pair<string,list<pair<int,int>>>("swap",args));

		// ��������� ��� 
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));

		args.clear();
		bytecode.push_back(pair<string,list<pair<int,int>>>("swap",args));

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","put","(Lphprtl/stdClass;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		args.clear();
		bytecode.push_back(pair<string,list<pair<int,int>>>("pop",args));

		break;

	case _CLONE:

		// ������ ���������
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));

		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","myClone","()Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		break;

	case _NEWOBJ:

		// ����� ��� �������
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->value.newobj->callexpr,classname,varIndex));

		// �������������� � String
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToString","()Ljava/lang/String;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		// ������� ����� ������
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","newObject","(Ljava/lang/String;)Lphprtl/stdClass;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokestatic",args));
		
		// �����������
		args.clear();
		bytecode.push_back(pair<string,list<pair<int,int>>>("dup",args));

		//������� �����������(methodcall)
			// ���
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantString("__construct")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));

			// ���������
				// ���������� ��������� �������
				args.clear();
				if (expr->value.newobj->paramsC>=0 && expr->value.newobj->paramsC<=5)
				{
					bytecode.push_back(pair<string,list<pair<int,int>>>(bytecodeInfo.const_i[expr->value.newobj->paramsC],args));
				}
				else
				{
					args.push_back(pair<int,int>(expr->value.newobj->paramsC,_S2));
					bytecode.push_back(pair<string,list<pair<int,int>>>("sipush",args));
				}


				// ������� ������
				args.clear();
				args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantClass("phprtl/stdClass")->getNumber(),_U2));
				bytecode.push_back(pair<string,list<pair<int,int>>>("anewarray",args));

				elist = expr->value.newobj->params;
				i= 0;
				while (elist)
				{
					// ����������� ������
					args.clear();
					bytecode.push_back(pair<string,list<pair<int,int>>>("dup",args));

					// �������� ������
					args.clear();
					if (i>=0 && i<=5)
					{
						bytecode.push_back(pair<string,list<pair<int,int>>>(bytecodeInfo.const_i[i],args));
					}
					else
					{
						args.push_back(pair<int,int>(i,_S2));
						bytecode.push_back(pair<string,list<pair<int,int>>>("sipush",args));
					}	

					// �������� ��������
					bytecode.splice(bytecode.end(),generateExpressionBytecode(elist->expr,classname,varIndex));

					// ����������� � ������
					args.clear();
					bytecode.push_back(pair<string,list<pair<int,int>>>("aastore",args));

					elist = elist->next;
					i++;
				}
			
			// ��� �������� ������
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->names->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));


			//������� methodCall
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","methodCall","(Ljava/lang/String;[Lphprtl/stdClass;Ljava/lang/String;)Lphprtl/stdClass;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

			args.clear();
			bytecode.push_back(pair<string,list<pair<int,int>>>("pop",args));

		break;

	case _FUNCTION:
		// ����� ��� ������
		bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->value.newobj->callexpr,classname,varIndex));

		// �������������� � String
		args.clear();
		args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToString","()Ljava/lang/String;")->getNumber(),_U2));
		bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		//������������ ������ ����������			
				// ���������� ��������� �������
				args.clear();
				if (expr->value.newobj->paramsC>=0 && expr->value.newobj->paramsC<=5)
				{
					bytecode.push_back(pair<string,list<pair<int,int>>>(bytecodeInfo.const_i[expr->value.newobj->paramsC],args));
				}
				else
				{
					args.push_back(pair<int,int>(expr->value.newobj->paramsC,_S2));
					bytecode.push_back(pair<string,list<pair<int,int>>>("sipush",args));
				}


				// ������� ������
				args.clear();
				args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantClass("phprtl/stdClass")->getNumber(),_U2));
				bytecode.push_back(pair<string,list<pair<int,int>>>("anewarray",args));

				elist = expr->value.newobj->params;
				i= 0;
				while (elist)
				{
					// ����������� ������
					args.clear();
					bytecode.push_back(pair<string,list<pair<int,int>>>("dup",args));

					// �������� ������
					args.clear();
					if (i>=0 && i<=5)
					{
						bytecode.push_back(pair<string,list<pair<int,int>>>(bytecodeInfo.const_i[i],args));
					}
					else
					{
						args.push_back(pair<int,int>(i,_S2));
						bytecode.push_back(pair<string,list<pair<int,int>>>("sipush",args));
					}	

					// �������� ��������
					bytecode.splice(bytecode.end(),generateExpressionBytecode(elist->expr,classname,varIndex));

					// ����������� � ������
					args.clear();
					bytecode.push_back(pair<string,list<pair<int,int>>>("aastore",args));

					elist = elist->next;
					i++;
				}
			
			// ��� �������� ������
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantString("phpprogram/0Main")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));

			// ��� ���������� ����� ������
			args.clear();			
			bytecode.push_back(pair<string,list<pair<int,int>>>("dup",args));

			args.clear();
			args.push_back(pair<int,int>(varIndex,_U1));
			bytecode.push_back(pair<string,list<pair<int,int>>>("aload",args));
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantString("this")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","fromString","(Ljava/lang/String;)Lphprtl/stdClass;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokestatic",args));
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","dollar","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

			//������� staticMethodCall
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","staticMethodCall","(Ljava/lang/String;[Lphprtl/stdClass;Ljava/lang/String;Ljava/lang/String;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokestatic",args));

		break;
	case _DCOLON:		

		if (expr->right->type == _FUNCTION)
		{
			// �������� ��� ������������ ������
			bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right->value.newobj->callexpr,classname,varIndex));

			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToString","()Ljava/lang/String;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

			//������������ ������ ����������			
				// ���������� ��������� �������
				args.clear();
				if (expr->right->value.newobj->paramsC>=0 && expr->right->value.newobj->paramsC<=5)
				{
					bytecode.push_back(pair<string,list<pair<int,int>>>(bytecodeInfo.const_i[expr->right->value.newobj->paramsC],args));
				}
				else
				{
					args.push_back(pair<int,int>(expr->right->value.newobj->paramsC,_S2));
					bytecode.push_back(pair<string,list<pair<int,int>>>("sipush",args));
				}


				// ������� ������
				args.clear();
				args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantClass("phprtl/stdClass")->getNumber(),_U2));
				bytecode.push_back(pair<string,list<pair<int,int>>>("anewarray",args));

				elist = expr->right->value.newobj->params;
				i= 0;
				while (elist)
				{
					// ����������� ������
					args.clear();
					bytecode.push_back(pair<string,list<pair<int,int>>>("dup",args));

					// �������� ������
					args.clear();
					if (i>=0 && i<=5)
					{
						bytecode.push_back(pair<string,list<pair<int,int>>>(bytecodeInfo.const_i[i],args));
					}
					else
					{
						args.push_back(pair<int,int>(i,_S2));
						bytecode.push_back(pair<string,list<pair<int,int>>>("sipush",args));
					}	

					// �������� ��������
					bytecode.splice(bytecode.end(),generateExpressionBytecode(elist->expr,classname,varIndex));

					// ����������� � ������
					args.clear();
					bytecode.push_back(pair<string,list<pair<int,int>>>("aastore",args));

					elist = elist->next;
					i++;
				}

			// �������� ��� ������
			bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));

			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToString","()Ljava/lang/String;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));


			//�������� ��� ����������� ������
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->names->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));

			args.clear();
			args.push_back(pair<int,int>(varIndex,_U1));
			bytecode.push_back(pair<string,list<pair<int,int>>>("aload",args));
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantString("this")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","fromString","(Ljava/lang/String;)Lphprtl/stdClass;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokestatic",args));
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/Variables","dollar","(Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","staticMethodCall","(Ljava/lang/String;[Lphprtl/stdClass;Ljava/lang/String;Ljava/lang/String;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokestatic",args));
		}
		else
		{
			// �������� ��� ������������ ��������
			bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));

			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToString","()Ljava/lang/String;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));


			// �������� ��� ������
			bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));

			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToString","()Ljava/lang/String;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));


			//�������� ��� ����������� ������
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->names->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));

			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","getStaticField","(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lphprtl/stdClass;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokestatic",args));
		}

		break;

		case _DCOLON_ASSIGN:

			// �������� ��� ������������ ��������
			bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->center,classname,varIndex));

			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToString","()Ljava/lang/String;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));


			// �������� ��� ������
			bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));

			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToString","()Ljava/lang/String;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));


			//�������� ��� ����������� ������
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->names->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));

			// �������� ��������
			bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));

			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","setStaticField","(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokestatic",args));
			
			break;

	case _ARROW:
		if (expr->right->type == _FUNCTION)
		{
			// �������� ������
			bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));

			// �������� ��� ������
			bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right->value.newobj->callexpr,classname,varIndex));

			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToString","()Ljava/lang/String;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

			//������������ ������ ����������			
				// ���������� ��������� �������
				args.clear();
				if (expr->right->value.newobj->paramsC>=0 && expr->right->value.newobj->paramsC<=5)
				{
					bytecode.push_back(pair<string,list<pair<int,int>>>(bytecodeInfo.const_i[expr->right->value.newobj->paramsC],args));
				}
				else
				{
					args.push_back(pair<int,int>(expr->right->value.newobj->paramsC,_S2));
					bytecode.push_back(pair<string,list<pair<int,int>>>("sipush",args));
				}


				// ������� ������
				args.clear();
				args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantClass("phprtl/stdClass")->getNumber(),_U2));
				bytecode.push_back(pair<string,list<pair<int,int>>>("anewarray",args));

				elist = expr->right->value.newobj->params;
				i= 0;
				while (elist)
				{
					// ����������� ������
					args.clear();
					bytecode.push_back(pair<string,list<pair<int,int>>>("dup",args));

					// �������� ������
					args.clear();
					if (i>=0 && i<=5)
					{
						bytecode.push_back(pair<string,list<pair<int,int>>>(bytecodeInfo.const_i[i],args));
					}
					else
					{
						args.push_back(pair<int,int>(i,_S2));
						bytecode.push_back(pair<string,list<pair<int,int>>>("sipush",args));
					}	

					// �������� ��������
					bytecode.splice(bytecode.end(),generateExpressionBytecode(elist->expr,classname,varIndex));

					// ����������� � ������
					args.clear();
					bytecode.push_back(pair<string,list<pair<int,int>>>("aastore",args));

					elist = elist->next;
					i++;
				}

			//�������� ��� ����������� ������
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->names->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));

			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","methodCall","(Ljava/lang/String;[Lphprtl/stdClass;Ljava/lang/String;)Lphprtl/stdClass;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		}
		else
		{
			// �������� ������
			bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));

			// �������� ��� ��������
			bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));

			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToString","()Ljava/lang/String;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
			
			//�������� ��� ����������� ������
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->names->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));

			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","getField","(Ljava/lang/String;Ljava/lang/String;)Lphprtl/stdClass;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
		}
		break;

	case _ARROW_ASSIGN:
			// �������� ������
			bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->left,classname,varIndex));

			// �������� ��� ��������
			bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->center,classname,varIndex));

			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","convertToString","()Ljava/lang/String;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));
			
			//�������� ��� ����������� ������
			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->names->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("ldc_w",args));
			
			// �������� ��������			
			bytecode.splice(bytecode.end(),generateExpressionBytecode(expr->right,classname,varIndex));

			args.clear();
			args.push_back(pair<int,int>(GlobalDescription::classes[classname]->addConstantMethodref("phprtl/stdClass","setField","(Ljava/lang/String;Ljava/lang/String;Lphprtl/stdClass;)Lphprtl/stdClass;")->getNumber(),_U2));
			bytecode.push_back(pair<string,list<pair<int,int>>>("invokevirtual",args));

		break;
	}

	return bytecode;
}



int bytecodeLength(list<pair<string,list<pair<int,int>>>> & bytecode)
{
	int size = 0;

	for (auto i =bytecode.cbegin(); i!= bytecode.cend();++i )
	{
		size+=bytecodeInfo.comandSize[i->first];
	}

	return size;
}

void writeBytecode(int descr,list<pair<string,list<pair<int,int>>>> & bytecode)
{	
	for (auto i =bytecode.cbegin(); i!= bytecode.cend();++i )
	{// ��� ������ �������

		u1 = bytecodeInfo.comand[i->first];
		write(descr,(void*)&u1,1);

		for (auto j = i->second.cbegin();j!= i->second.cend();++j)
		{// ��� ������� ��������� �������
			switch (j->second)
			{
			case _U1:
				u1 = j->first;
				write(descr,(void*)&u1,1);
				break;
			case _U2:
				u2 = htons(j->first);
				write(descr,(void*)&u2,2);
				break;
			case _U4:
				u4 = htonl(j->first);
				write(descr,(void*)&u4,4);
				break;
			case _S1:
				s1 = j->first;
				write(descr,(void*)&s1,1);
				break;
			case _S2:
				s2 = htons(j->first);
				write(descr,(void*)&s2,2);
				break;
			case _S4:
				s4 = htonl(j->first);
				write(descr,(void*)&s4,4);
				break;
			}
		}
	}
}